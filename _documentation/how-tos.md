---
layout: page
title: How-Tos
---

*practical steps · problem-oriented · useful when working*

<div class='row'>
<div class='col' markdown='1'>


### Character set

- {% internallink 'how-tos/adding-and-removing-glyphs' %}
- {% internallink 'how-tos/building-accented-glyphs-with-glyph-construction' %}
- {% internallink 'how-tos/renaming-glyphs' %}
- {% internallink 'how-tos/custom-gnful-mapping' %}

### Variable fonts

- {% internallink 'how-tos/checking-interpolation-compatibility' %}
- {% internallink 'how-tos/creating-glyph-substitution-rules' %}

### Scripting

- {% internallink 'how-tos/adding-localized-name-table' %}
- {% internallink 'how-tos/building-accented-glyphs-with-a-script' %}

### Font generation

- {% internallink 'how-tos/hinting-ttfautohint' %}
- {% internallink 'how-tos/generating-woffs' %}
- {% internallink 'how-tos/generating-trial-fonts' %}

### Application & workspace

- {% internallink 'how-tos/installation' %}
- {% internallink 'how-tos/adding-custom-items-to-application-menu' %}

### Scripting environment

- {% internallink 'how-tos/overriding-embedded-libraries' %}

### UFO Data and Format Conversion

- {% internallink 'how-tos/fixing-glif-errors' %}
- {% internallink 'how-tos/converting-between-ufo-versions' %}
- {% internallink 'how-tos/converting-from-cubic-to-quadratic' %}
- {% internallink 'how-tos/converting-from-glyphs-to-ufo' %}
- {% internallink 'how-tos/converting-from-opentype-to-ufo' %}
- {% internallink 'how-tos/converting-from-vfb-to-ufo' %}

### DrawBot

- {% internallink 'how-tos/drawbot/drawbot-module' %}
- {% internallink 'how-tos/drawbot/drawbot-view' %}
- {% internallink 'how-tos/drawbot/proof-character-set' %}
- {% internallink 'how-tos/drawbot/proof-glyphs' %}
- {% internallink 'how-tos/drawbot/proof-words' %}
- {% internallink 'how-tos/drawbot/proof-features' %}
- {% internallink 'how-tos/drawbot/proof-spacing' %}

### Merz

- {% internallink 'how-tos/merz/merzview-accepting-mouse-events' %}

### vanilla

- {% internallink 'how-tos/vanilla/bulk-list' %}
- {% internallink 'how-tos/vanilla/window-with-move-sliders' %}

### Documentation

- {% internallink 'how-tos/giving-feedback-on-docs' %}
- {% internallink 'how-tos/previewing-documentation-locally' %}

</div>
<div class='col' markdown='1'>

### mojo

- {% internallink 'how-tos/mojo/accordion-window' %}
- {% internallink 'how-tos/mojo/decompose-point-pen' %}
- {% internallink 'how-tos/mojo/glyph-preview' %}
- {% internallink 'how-tos/mojo/help-window' %}
- {% internallink 'how-tos/mojo/multiline-view' %}
- {% internallink 'how-tos/mojo/open-glyph-in-new-window' %}
- {% internallink 'how-tos/mojo/read-write-defaults' %}
- {% internallink 'how-tos/mojo/sort-fonts-list' %}
- {% internallink 'how-tos/mojo/smart-sets' %}
- {% internallink 'how-tos/mojo/space-center' %}
- {% internallink 'how-tos/updating-scripts-from-RF3-to-RF4' %}

### Subscriber

- {% internallink 'how-tos/subscriber/add-toolbar-item' %}
- {% internallink 'how-tos/subscriber/custom-font-overview-contextual-menu' %}
- {% internallink 'how-tos/subscriber/custom-glyph-preview' %}
- {% internallink 'how-tos/subscriber/custom-glyph-window-display-menu' %}
- {% internallink 'how-tos/subscriber/custom-inspector-panel' %}
- {% internallink 'how-tos/subscriber/interactive-selection-distance' %}
- {% internallink 'how-tos/subscriber/draw-info-text-in-glyph-view' %}
- {% internallink 'how-tos/subscriber/draw-reference-glyph' %}
- {% internallink 'how-tos/subscriber/glyph-editor-subview' %}
- {% internallink 'how-tos/subscriber/list-layers' %}
- {% internallink 'how-tos/subscriber/multifont-glyph-preview' %}
- {% internallink 'how-tos/subscriber/several-subscribers-listening-to-one-window' %}
- {% internallink 'how-tos/subscriber/open-component-in-new-window' %}
- {% internallink 'how-tos/subscriber/stencil-preview' %}
- {% internallink 'how-tos/subscriber/subscribing-to-current-font' %}
- {% internallink 'how-tos/subscriber/subscribing-to-current-glyph' %}
- {% internallink 'how-tos/subscriber/subscribing-to-glyph-editor' %}
- {% internallink 'how-tos/subscriber/subscribing-to-robofont-application' %}
- {% internallink 'how-tos/subscriber/synchronize-glypheditors-across-fonts' %}

### Defcon Representations

- {% internallink 'how-tos/representations/svgPen' %}
- {% internallink 'how-tos/representations/nsimage' %}
- {% internallink 'how-tos/representations/grid' %}
- {% internallink 'how-tos/representations/flatKerning' %}
- {% internallink 'how-tos/representations/area' %}
- {% internallink 'how-tos/representations/drawMatchingPoints' %}

### Interactive Tools

- {% internallink 'how-tos/interactive/simple-tool-example' %}
- {% internallink 'how-tos/interactive/polygonal-selection-tool' %}
- {% internallink 'how-tos/interactive/constrain-angle-tool' %}

### Observers
- {% internallink 'how-tos/observers/custom-label-in-glyph-cells' %} <span class='yellow'>✗</span>
- {% internallink 'how-tos/observers/custom-space-center-preview' %} <span class='yellow'>✗</span>

### Extensions

- {% internallink 'extensions/getting-extensions' %}
- {% internallink 'extensions/using-extensions' %}
- {% internallink 'extensions/installing-extensions-mechanic' %}
- {% internallink 'extensions/installing-extensions-manually' %
- {% internallink 'extensions/managing-extension-streams' %}
- {% internallink 'extensions/building-extensions-with-extension-builder' %}
- {% internallink 'extensions/building-extensions-with-script' %}
- {% internallink 'extensions/publishing-extensions' %}

</div>
</div>