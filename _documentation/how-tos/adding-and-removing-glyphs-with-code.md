---
layout: page
title: Adding and removing glyphs with code
tags:
  - character set
  - scripting
---

* Table of Contents
{:toc}

*This page shows how to add and remove glyphs to a font with Python.*


Adding glyphs
-------------

The example below shows how to add news glyphs to a font, including a check to avoid overwriting existing glyphs.

```python
font = CurrentFont()

# add a new glyph to the font
# if the glyph already exists, it will be overwritten
font.newGlyph('hello')

# use clear=False if you don't want to overwrite an existing glyph
font.newGlyph('A', clear=False)

# add several glyphs at once with a loop
newGlyphNames = ['test1', 'test2', 'test3']
for glyphName in newGlyphNames:
    font.newGlyph(glyphName)
```


Removing glyphs
---------------

The example below shows to remove glyphs from a gony, including a check to avoid an error if the font does not include a glyph with the given name.

```python
font = CurrentFont()

glyphName = 'hello'

# before removing a glyph, make sure it actually exists in the font
if glyphName in font:
    del font[glyphName]
else:
    print("font does not contain a glyph named '%s'" % glyphName)

# if we try to remove a glyph which is not in the font,
# and error will be raised
del font['spam']
```

```plaintext
Traceback (most recent call last):
  File "<untitled>", line 3, in <module>
  File "/Applications/RoboFont.app/Contents/Resources/lib/python3.6/fontParts/base/layer.py", line 136, in __delitem__
ValueError: No glyph with the name 'spam' exists.
```

> `font.removeGlyph(glyphName)` is deprecated.
{: .warning }

> When a glyph is removed from a font with a script, it is **not** automatically removed from groups, kerning, or components. *This is not necessarily a problem*, since the [UFO3 specification] allows groups, kerning and components to reference glyphs which are not in the font.
{: .note }

[UFO3 specification]: http://unifiedfontobject.org/versions/ufo3/

### Removing glyphs from groups

The example below shows how to remove all occurrences of a glyph in groups.

```python
font = CurrentFont()

# glyph to be removed
glyphName = 'B'

# iterate over all groups in the font
for groupName in font.groups.keys():
    # get the group as a list of glyph names
    group = list(font.groups[groupName])
    # if the glyph is in the group:
    if glyphName in group:
        # remove the glyph name
        print("removing '%s' from group '%s'..." % (glyphName, groupName))
        group.remove(glyphName)
        # put the group back into the font
        font.groups[groupName] = group
```

### Removing glyphs from kerning

The example below shows how to remove all kerning pairs which include a glyph.

```python
font = CurrentFont()

# the glyph to be removed
glyphName = 'A'

# iterate over all kerning pairs in the font
for kerningPair in font.kerning.keys():
    # if the glyph is in the kerning pair:
    if glyphName in kerningPair:
        # remove the kerning pair
        print('removing kerning pair (%s, %s)...' % kerningPair)
        del font.kerning[kerningPair]
```

### Removing glyphs from components

The example below shows how to remove all components referencing a glyph.

```python
font = CurrentFont()

# the glyph to be removed
glyphName = 'a'

# iterate over all glyphs in the font
for glyph in font:
    # skip glyphs which don't have components
    if not glyph.components:
        continue
    # iterate over all components in glyph
    for component in glyph.components:
        # if the base glyph is the glyph to be removed
        if component.baseGlyph == glyphName:
            # delete the component
            glyph.removeComponent(component)
```


Removing template glyphs
------------------------

The example below shows how to remove a template glyph from a font.

```python
font = CurrentFont()

# the template glyph to be removed
glyphName = 'a'

# get the current template glyphs
templateGlyphs = font.templateGlyphOrder

# remove template glyph name from list
templateGlyphs.remove(glyphName)

# save the list of template glyphs back into the font
f.templateGlyphOrder = templateGlyphs
```
