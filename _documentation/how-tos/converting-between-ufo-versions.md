---
layout: page
title: Converting between UFO versions
tags:
  - UFO
draft: false
---

* Table of Contents
{:toc}

Opening UFO2 fonts in RoboFont 3
--------------------------------

RoboFont 3 uses [UFO3] as its native format. RoboFont 1 used [UFO2] with custom additions.

RoboFont 3 opens UFO2 files natively and converts them to UFO3 using the [ufoLib].

If opening a UFO2 file fails (for example if it contains invalid data), RoboFont will try to run [ufoNormalizer] on it first, and then convert the normalized UFO2 font to UFO3.

Both RoboFont and ufoLib provide reports about problems during the conversion process.


Saving to another UFO version
-----------------------------

To save an open font as UFO1, UFO2 or UFO3, use the menu *File > Save As…* to open the Save As sheet:

{% image how-tos/converting-from-ufo2-to-ufo3_save-as.png %}

Then choose the desired UFO version from the drop-down list:

{% image how-tos/converting-from-ufo2-to-ufo3_ufo-versions.png %}

[UFO2]: http://unifiedfontobject.org/versions/ufo2/
[UFO3]: http://unifiedfontobject.org/versions/ufo3/
[ufoLib]: http://github.com/fonttools/fonttools/tree/master/Lib/fontTools/ufoLib
[ufoNormalizer]: http://github.com/unified-font-object/ufoNormalizer

> - {% internallink 'topics/the-ufo-format' %}
{: .seealso }


Batch converting between UFO formats
------------------------------------

If there are lots of fonts to convert at once, we can use Python to automate the process. Here’s a script to convert all fonts in a folder to a different UFO version:

{% showcode how-tos/batchConvertUFO.py %}
