---
layout: page
title: Converting from cubic to quadratic
tags:
  - TrueType
  - contours
slideShows:
  convertTo:
    - image: how-tos/convertTo_startingPoint.png
      caption: |
        Original outline
    - image: how-tos/convertTo_quadratics.png
      caption: |
        Convert to quadratic
    - image: how-tos/convertTo_backToCubic.png
      caption: |
        Back to cubic
  autoPlay: true
  height: 550
---

* Table of Contents
{:toc}

Converting to and from quadratic using the glyph editor
-----------------------------------------------

You can convert cubic → quadratic and quadratic → cubic using the glyph editor contextual menu.

{% image how-tos/converting-from-cubic-to-quadratic_glyph-editor.png %}

No consistent back and forth translation is possible between quadratics and cubic beziers. So, if you convert from cubic to quadratic, then back to cubic, you'll never get the same point structure. So, keep that in mind while using this feature.

{% video how-tos/cubic-quadratic-cubic.mp4 %}

Under the hood, RoboFont tries to convert the cubic bezier to quadratic maintaining the same point structure (two on-curve points, two off-curve points). If the result is not satisfying, it converts the cubic bezier using an algorithm that does not preserve the point structure.


Converting to and from quadratic using a script
--------------------------------------

The script below converts all glyphs in the current (cubic) font to quadratic and then back to cubic.

The conversion is made using RoboFont’s cubic-to-quadratic algorithm, which adds an implied on-curve point and splits each curve segment in two.

{% showcode how-tos/convertCubicToQuadraticAndBack.py %}

{% include slideshows items=page.slideShows.convertTo %}

> Consider that [UFO specs](https://unifiedfontobject.org/versions/ufo3/glyphs/glif/#point) – and RoboFont – allow mixed outlines within the same glyph.
{: .tip}

> - {% internallink "drawing-with-quadratic-curves" %}
{: .seealso }
