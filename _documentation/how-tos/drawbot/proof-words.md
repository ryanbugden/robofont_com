---
layout: page
title: Proof words
tags:
  - scripting
  - drawBot
---

This example shows how to set a line of text using glyphs from a UFO font.

{% image how-tos/drawbot/proofWords.png %}

In order to set text on the page, we need to convert a string of characters into a list of glyph names. This is done using the font’s *character map*, a dictionary that maps unicode values to glyph names.

After each glyph is drawn, the origin position is shifted horizontally for the next glyph.

{% showcode how-tos/drawbot/proofWords.py %}

> This example could be extended to support line breaks, so that the text could reflow after reaching the end of the page.
{: .tip }
