---
layout: page
title: Installing extensions manually
treeTitle: Installing manually
tags:
  - extensions
---

* Table of Contents
{:toc}


Installing extensions
---------------------

To install an extension, simply double-click a `.roboFontExt` file. A dialog will appear, asking you to confirm the operation.

{% image how-tos/extensions/installing-extensions_install.png %}

That’s it! The extension is installed and ready to use.

> - {% internallink "using-extensions" %}
{: .seealso }

### Updating extensions

If the extension was already installed, a dialog will appear asking if you would like to re-install it. Use this option to manually update an installed extension to a more recent version.
{: .note }

{% image how-tos/extensions/installing-extensions_update.png %}


Uninstalling extensions
-----------------------

To uninstall an extension, go to the {% internallink 'workspace/preferences-window/extensions' %}, select the extension which you would like to uninstall, and click the minus button at the bottom left of the list.

{% image how-tos/extensions/installing-extensions_uninstall.png %}
