---
layout: page
title: Fixing Errors in GLIF XML data
tags:
  - ufo
  - character set
---

{% image how-tos/A-glif-error.png %}

If you spot some red crosses over glyphs in the Font Overview window, it means that RoboFont has encountered issue while reading GLIF XML data. This might happen thanks to an external application manipulating the UFO data, or a filesharing operation gone wrong (Dropbox or similar).

How to proceed from here to minimize the damage?

1. Our first advice is always to store your UFOs into a version control system like `git`. In this way you'll be able to go safely to a previous moment in the "history" of your files. This is a great way to protect yourself from data corruption issues

2. What if it's too late for using `git`? If RoboFont is not able to fix automatically the issues through `ufoNormalizer`, the first step is to narrow down the problem: which glyphs are affected? What's the issue inside the glyph?

You can extract this information using a script like this:

```python
from mojo.roboFont import CurrentFont
for eachGlyph in CurrentFont():
    if eachGlyph.readingErrors():
        print(eachGlyph.readingErrors())
```

Using the information printed in console, you can inspect the ufo package with a code editor and fix manually the corrupted data.