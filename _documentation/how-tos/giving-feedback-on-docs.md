---
layout: page
title: Giving feedback on the documentation
tags:
  - documentation
---

**You can help to improve the RoboFont documentation!**

All forms of feedback are welcome:

- corrections of all kinds
- new content: specially tutorials and how-to’s
- any type of comments, suggestions, requests etc.


Feedback channels
-----------------

Please use one of the communication channels below to get in touch:

[RoboFont Forums > Documentation](http://forum.robofont.com/category/12/documentation)
: for questions and discussions

[GitLab Issues](http://gitlab.com/typemytype/robofont_com/issues)
: for bug reports, feature requests, etc.


Making changes directly
-----------------------

If you find an error and you are familiar with {% internallink 'tutorials/using-git' text='Git' %}, you are welcome to fix the error yourself and submit a merge request.

See {% internallink "editing-the-docs" %} for instructions.
