---
layout: page
title: Autohinting TrueType fonts with ttfautohint
tags:
  - hinting
  - TrueType
---

* Table of Contents
{:toc}

From the [ttfautohint] documentation:

> **ttfautohint** is a library written in C that takes a TrueType font as the input, removes its bytecode instructions (if any), and returns a new font where all glyphs are bytecode hinted using the information given by FreeType’s auto-hinting module.


Using ttfautohint with Batch
----------------------------

The [Batch extension] makes it easy to generate TTF fonts and hint them with ttfautohint in RoboFont.

> If you don’t have Batch installed yet, you can get it from [Mechanic].
{: .note }

Follow the steps below to generate TrueType fonts and apply ttfautohint to them:

1. Open the Batch extension.

2. In the *Fonts* section, select one or more UFOs to generate TTFs from (drag the fonts from Finder into the list, or use the *Open* or *Add Open Fonts* buttons in the toolbar).

3. In the *Web Fonts* section, select *Save TTF*…

   {% image how-tos/batch-webfont-formats.png %}

   …then click on the gears icon to open a sheet with ttfautohint settings:

   {% image how-tos/batch-ttfautohint.png %}

   Adjust the settings as needed – see the [ttfautohint documentation] for details about each option.
   
4. Click on *Save settings* when done to close the sheet.

5. Click on the *Generate* button to generate the selected fonts in OpenType-TTF format (`.ttf`) and autohint them using ttfautohint.


Using ttfautohint with a script
-------------------------------

You can use ttfautohint through the {% internallink "/reference/api/mojo/mojo-compile#mojo.compile.ttfautohint" text="mojo API" %}. ttfautohint does not come shipped with RoboFont, so you need to install it separately. The easiest way to install ttfautohint is to use [HomeBrew](https://brew.sh):

```plaintext
$ brew install ttfautohint
```

```python
from mojo.compile import hasTTFAutoHint, ttfautohint

fileName = 'someFont'

if hasTTFAutoHint():
    ttfautohint(fontPath=f'{fileName}.ttf',
                outputPath=f'{fileName}_hinted.ttf')
```

Always check if ttfautohint is available on your machine, otherwise the application will crash. If you are running a script outside RoboFont, you can call ttfautohint – as any other command line tool – using the [subprocess](https://docs.python.org/3/library/subprocess.html) module.

[ttfautohint]: http://www.freetype.org/ttfautohint/
[Batch extension]: http://github.com/typemytype/batchrobofontextension
[Mechanic]: http://robofontmechanic.com/
[ttfautohint documentation]: http://www.freetype.org/ttfautohint/doc/ttfautohint.html#options
