---
layout: page
title: MerzView accepting mouse events
tags:
  - merz
  - events
---

{% image how-tos/merz/merzview-accepting-mouse-events.png %}

This example shows how to create a simple mouse brush in a {% internallink "topics/merz" %} view stored into a {% internallink "topics/vanilla" %} window.

{% showcode how-tos/merz/merzview-accepting-mouse-events.py %}
