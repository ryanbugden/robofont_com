---
layout: page
title: Accordion View
tags:
  - mojo
---

RoboFont’s {% internallink "workspace/inspector" text="Inspector panel" %} uses a custom UI component, the [Accordion View]. It consists of multiple sections which can be expanded/collapsed as needed – this can be useful when there’s more information than screen space to display it.

{% image how-tos/mojo/AccordionView.png %}

{% showcode how-tos/mojo/accordionViewExample.py %}

[Accordion View]: /documentation/reference/api/mojo/mojo-ui/#mojo.UI.AccordionView