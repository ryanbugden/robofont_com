---
layout: page
title: Using the DecomposePointPen
tags:
  - mojo
  - pens
---

The script below shows how to decompose a glyph using the `DecomposePointPen`.

This approach is useful when the target glyph doesn’t have access to the parent font.

{% showcode how-tos/mojo/decomposePointPenExample.py %}
