---
layout: page
title: Glyph Preview
tags:
  - mojo
---

The {% internallink 'documentation/reference/api/mojo/mojo-glyphpreview' text='GlyphPreview' %} object is a simple glyph preview area for use with vanilla windows.

<!-- The example script below also shows `OpenWindow`, a small helper function which prevents RoboFont from opening the same window twice. -->

{% image how-tos/mojo/GlyphPreview.png %}

{% showcode how-tos/mojo/glyphPreviewExample.py %}
