---
layout: page
title: Multi-Line View
tags:
  - mojo
---

The Multi-Line view is a reflowable text preview area for use with vanilla, with support for zoom and glyph selection. This is the same component which is used to display glyphs in the {% internallink 'workspace/space-center' %}.

{% image how-tos/mojo/MultiLineView.png %}

{% showcode how-tos/mojo/multiLineViewExample.py %}
