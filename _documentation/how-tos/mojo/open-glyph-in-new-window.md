---
layout: page
title: Open glyph in new window
tags:
  - mojo
---

By default, you can only have one {% internallink 'workspace/glyph-editor' text='Glyph Window' %} open at once in RoboFont. But in some situations, it might be useful to have more glyph windows open.

{% showcode how-tos/mojo/openGlyphInNewWindow.py %}
