---
layout: page
title: Reading and writing defaults
tags:
  - mojo
  - preferences
draft: false
---

* Table of Contents
{:toc}


Application defaults
--------------------

RoboFont’s default settings can be edited manually using the {% internallink 'workspace/preferences-window' %} or the {% internallink 'workspace/preferences-editor' %}.

The application defaults can also be edited with a script:

```python
from mojo.UI import getDefault, setDefault, preferencesChanged

# get current value
value = getDefault("glyphViewGridx")
print(f"previous value: {value}")

# set a new value
setDefault("glyphViewGridx", 40)
value = getDefault("glyphViewGridx")
print(f"new value: {value}")

# tell the app that preferences were changed
preferencesChanged()
```

> To find out how a default value is named, look at the keys in the {% internallink 'workspace/preferences-editor' %}.
{: .tip }

### Validation

Values can be validated when saving preferences with `setDefault`:

```python
setDefault("glyphViewGridx", 40, validate=True)
```

Use validation to make sure that no bad values are stored in the defaults.


Extension defaults
------------------

Your own tools and extensions can also store defaults using a separate API.

Value names (keys) should be created using the *reverse domain name* scheme.

```python
from mojo.extensions import getExtensionDefault, setExtensionDefault

key = 'com.mydomain.mytool.avalue'

value = getExtensionDefault(key, fallback=10)
print(f"previous value: {value}")

setExtensionDefault(key, 50)
value = getExtensionDefault(key)
print(f"new value: {value}")
```
