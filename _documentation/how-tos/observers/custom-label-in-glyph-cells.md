---
layout: page
title: Custom label in glyph cells
tags:
  - observers
---

This example shows how to draw inside the glyph cells of the {% internallink 'workspace/font-overview' %}.

{% image how-tos/observers/custom-labels-cells.png %}

The script opens a dialog containing a button to add or remove a ‘label’ to the selected glyphs in the Font Overview.

{% image how-tos/observers/custom-labels-dialog.png %}

The state of this ‘label’ is stored in a custom key in the glyph lib. When the Font Overview is refreshed, a small rectangle is drawn inside the cells of all glyphs which have this value set to `True`.

When the dialog is closed, the observer is removed and the labels are cleared.

{% showcode how-tos/observers/drawInGlyphCells.py %}
