---
layout: page
title: Custom Space Center preview
tags:
  - mojo
---

This example shows how to draw something into the glyphs of a Space Center preview.

The script simply draws a rectangle at each glyph’s bounding box. Whenever a glyph is changed, its preview is updated automatically.

{% image how-tos/observers/customPreviewSpaceCenter.png %}

Important in this example is the use of [representations] to create and cache shapes – this approach is recommended for better performance.

{% showcode how-tos/observers/customPreviewSpaceCenter.py %}

> - [Defcon documentation: Representations][representations]
> - [RoboFont Forum: How to customize preview of Space Center?](http://forum.robofont.com/topic/861/how-to-customize-preview-of-space-center)
> - [Goldener extension]
{: .seealso }

[representations]: http://defcon.robotools.dev/en/latest/concepts/representations.html
[Goldener extension]: http://github.com/typemytype/goldnerRoboFontExtension