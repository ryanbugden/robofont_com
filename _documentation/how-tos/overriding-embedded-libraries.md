---
layout: page
title: Overriding embedded libraries
tags:
  - scripting
draft: false
---

* Table of Contents
{:toc}


Embedded libraries
------------------

RoboFont comes with *batteries included* – that is, common functionality for building font-related tools come with the application. Several core Python libraries are embedded and ready to use in your scripts.

> - {% internallink "reference/embedded-libraries" text='Embedded libraries' %}
{: .seealso }

Developers can rely on RoboFont for keeping essential libraries up-to-date: RoboFont updates also include updates to all embedded libraries, if they are available.

In some cases, however, it may be necessary to use a different version of a library that comes with RoboFont – for example, the latest development version, an older version without a recent bug, your own private fork etc.


Overriding embedded libraries
-----------------------------

The recommended way of overriding embedded modules in RoboFont is using the `external_packages` folder, which can be found inside your user’s *Application Support* folder. (If this folder does not yet exist, you’ll have to create it manually.)

```text
/Users/myUsername/Library/Application Support/RoboFont/external_packages
```

RoboFont scans this folder during start-up and loads these packages first, so they have precedence over everything else which is loaded later.

> Using the `external_packages` folder will install packages **for RoboFont 3 only** – not for the system-level Python.
{: .warning }


Example: Using the development version of fontTools
---------------------------------------------------

As an example, imagine that you need to use the [latest version of fontTools from GitHub][fontTools] instead of the one that comes embedded in RoboFont.

The first step is downloading the fontTools source code. You can do that by downloading and extracting the zip, or by using `git clone`:

```plaintext
cd my/parent/folder
git clone http://github.com/fonttools/fonttools.git
```

[fontTools]: http://github.com/fonttools/fonttools

Once you have the source code in your machine, there are two ways to make the package available to RoboFont:

1. moving or copying the whole package into the `external_packages` folder
2. creating a `.pth` file pointing to the package’s location


Checking the current version of a library
-----------------------------------------

Use Python to check which version of a library RoboFont is using:

```python
import fontTools
print(fontTools.__path__)
```

This is the version which comes embedded with RoboFont:

```plaintext
['/Applications/RoboFont.app/Contents/Resources/lib/python3.6/fontTools']
```

This is the locally installed version:

```plaintext
['/my/parent/folder/fontTools/Lib/fontTools']
```
