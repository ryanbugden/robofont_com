---
layout: page
title: Renaming glyphs
tags:
  - glyph names
---

It is possible to change the name of an existing glyph from the Inspector > {% internallink "reference/workspace/inspector#editing-glyph-names" text="Glyph section" %}.

{% image /reference/workspace/inspector_glyph.png %}

If the string in the text field is changed, a sheet appears to assist with renaming the glyph. If you need it, RoboFont can take care of renaming references in groups, kerning and components. In addition, RoboFont can assign a new unicode based on the new name and rename the related layers.

{% image /reference/workspace/font-overview_rename-glyph-sheet.png %}

You can access the same functionality using the using the {% internallink "/reference/api/fontParts/rfont#RFont.renameGlyph" text="`RFont.renameGlyph()`" %} method.

```python
myFont = CurrentFont()
myFont.renameGlyph(oldName='oldName',
                   newName='newName',
                   renameComponents=True,
                   renameGroups=True,
                   renameKerning=True)
```

> Consider that renaming a glyph from the user interface or with a script will not update references in the OpenType features stored in the UFO.
{: .warning }
