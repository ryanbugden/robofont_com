---
layout: page
title: Glyph Surface Representation
---

This example shows how to register a `defcon.Glyph` representation that outputs the area of a glyph and draws the value on the glyph editor.

{% showcode how-tos/representations/area.py %}

> - {% internallink "topics/subscriber" %}
> - {% internallink "topics/merz" %}
> - {% internallink "topics/defcon-representation" %}
{: .seealso}