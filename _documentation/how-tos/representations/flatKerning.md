---
layout: page
title: Flat Kerning Representation
---

This example shows how to register a `defcon.Kerning` representation that outputs the flat kerning of a font. Considered that kerning depends on `defcon.Groups` too, the script registers a subscriber to make sure the representation cache is invalidated if a kerning group is edited.

{% showcode how-tos/representations/flatKerning.py %}

> - {% internallink "topics/subscriber" %}
> - {% internallink "topics/defcon-representation" %}
{: .seealso}