---
layout: page
title: Glyph Bitmap Representation
---

This example shows how to register a `defcon.Glyph` representation that outputs a `merzPen` storing a glyph bitmap. The pixel size is controlled through a `vanilla.Window`.

{% showcode how-tos/representations/grid.py %}

> - {% internallink "topics/subscriber" %}
> - {% internallink "topics/merz" %}
> - {% internallink "topics/defcon-representation" %}
{: .seealso}