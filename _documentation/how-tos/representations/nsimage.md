---
layout: page
title: Glyph NSImage Representation
---

This example shows how to register a `defcon.Glyph` representation that outputs a `NSImage`. The `NSImage` is then drawn with `merz` on the glyph editor.

{% showcode how-tos/representations/nsimage.py %}

> - {% internallink "topics/subscriber" %}
> - {% internallink "topics/merz" %}
> - {% internallink "topics/defcon-representation" %}
{: .seealso}