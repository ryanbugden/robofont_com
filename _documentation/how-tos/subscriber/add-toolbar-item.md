---
layout: page
title: Add toolbar item
tags:
  - subscriber
---

The example script below shows how to add a custom button to the toolbar of the Font Overview by subclassing the `Subscriber` class and defining the `fontDocumentWantsToolbarItems` callback.

The gear icon is selected from macOS [system icons] using [`NSImageName`].

{% image how-tos/subscriber/AddToolbarItem.png %}

{% showcode how-tos/subscriber/addToolbarItem.py %}

> - [addToolbarItem problem (RoboFont Forum)](http://forum.robofont.com/topic/837/addtoolbaritem-problem)
{: .seealso }

[system icons]: http://developer.apple.com/design/human-interface-guidelines/macos/icons-and-images/system-icons/
[`NSImageName`]: http://developer.apple.com/documentation/appkit/nsimagename?language=objc
