---
layout: page
title: Custom contextual menu
tags:
  - subscriber
---

This example shows how to create a custom contextual menu in the {% internallink 'workspace/font-overview' %}.

{% image how-tos/subscriber/customFontOverviewContextualMenu.png %}

> To activate the contextual menu, right-click anywhere in the font window.
{: .note }

{% showcode how-tos/subscriber/customFontOverviewContextualMenu.py %}

> The same approach can be used to add custom items to other contextual menus in RoboFont. Use the following `Subscriber` methods to access the menus for space center, font overview and the glyph editor:
>
> - {% internallink "reference/api/mojo/mojo-subscriber#fontOverviewWantsContextualMenuItems" text="fontOverviewWantsContextualMenuItems" %}
> - {% internallink "reference/api/mojo/mojo-subscriber#spaceCenterWantsContextualMenuItems" text="spaceCenterWantsContextualMenuItems" %}
> - {% internallink "reference/api/mojo/mojo-subscriber#glyphEditorWantsContextualMenuItems" text="glyphEditorWantsContextualMenuItems" %}
{: .tip }