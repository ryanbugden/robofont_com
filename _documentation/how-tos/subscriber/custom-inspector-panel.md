---
layout: page
title: Custom Inspector section
tags:
  - subscriber
---

This example shows how to add a custom section to the {% internallink 'workspace/inspector' %} panel.

{% image how-tos/subscriber/custom-inspector-panel.png %}

{% showcode how-tos/subscriber/customInspectorPanel.py %}

> - {% internallink 'how-tos/mojo/accordion-window' %}
{: .seealso }
