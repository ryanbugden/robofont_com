---
layout: page
title: Draw info text in the Glyph View
tags:
  - subscriber
---

This example shows how to draw informative text into the {% internallink 'workspace/glyph-editor' %} canvas using {% internallink "topics/merz" text="`merz`" %}. Two labels with date and time since the last edit in the glyph are updated using {% internallink "topics/subscriber" text="`subscriber`" %}.

{% image how-tos/subscriber/drawTextAtPointExample.png %}

{% showcode how-tos/subscriber/drawTextAtPointExample.py %}
