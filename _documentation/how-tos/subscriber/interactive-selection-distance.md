---
layout: page
title: Displaying distances between selected points in the glyph editor
tags:
  - subscriber
  - merz
---

{% image how-tos/subscriber/pointDistance.png %}

This example shows how to display distances between selected points in the glyph editor using {% internallink 'topics/subscriber' text="`Subscriber`" %} and {% internallink 'topics/merz' text="`Merz`" %}.

{% showcode how-tos/subscriber/pointDistance.py %}

If you want to be able to toggle the visualization on and off, you can get some help from a {% internallink "reference/api/mojo/mojo-subscriber#mojo.subscriber.WindowController" text="WindowController" %}. Use the callback of a vanilla checkbox to register or unregister the subscriber object.

{% showcode how-tos/subscriber/pointDistanceWithCheckBox.py %}
