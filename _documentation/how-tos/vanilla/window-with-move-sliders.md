---
layout: page
title: Moving a glyph using sliders
tags:
  - scripting
  - vanilla
---

This example shows a simple tool with sliders to move the current glyph.

The trick is to store the slider movements in a variable, so we can compare the desired total distance with the current one.

{% image how-tos/vanilla/windowWithMoveSliders.png %}

{% showcode how-tos/vanilla/simpleMoveExample.py %}
