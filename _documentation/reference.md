---
layout: page
title: Reference
tree:
    - workspace
    - embedded-libraries
    - fontparts
    - mojo
    - extensions
---

*theoretical knowledge · information-oriented · useful when working*

<div class='row'>

<div class='col' markdown=1>
### Workspace

{% tree "/documentation/reference/workspace" levels=3 %}
</div>

<div class='col' markdown=1>
### APIs

- {% internallink 'reference/embedded-libraries' %}
- {% internallink 'reference/fontparts' %}
  {% tree "/documentation/reference/fontparts" levels=2 %}
- {% internallink 'reference/mojo' %}
  {% tree "/documentation/reference/mojo" levels=2 %}
- {% internallink 'reference/api/custom-observers' %}
- {% internallink 'reference/api/custom-lib-keys' %}

### Extensions

- {% internallink 'reference/extensions/extension-file-spec' %}
- {% internallink 'reference/extensions/extension-item-format' %}
</div>

</div>