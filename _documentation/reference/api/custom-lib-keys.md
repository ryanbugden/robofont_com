---
layout: page
title: Custom lib keys
tags:
  - lib keys
  - scripting
---

Here follows a list of custom font lib keys supported by RoboFont.
If you are looking the TrueType hinting keys check the {% internallink '/tutorials/truetype-hinting' %}

```python
# robofont font libkeys
# string, if non existing it will use the default layer
font.lib["com.typemytype.robofont.compileSettings.layerName"] = 'foreground'

# boolean, default is False
font.lib["com.typemytype.robofont.compileSettings.decompose"] = True

# boolean, default is True
font.lib["com.typemytype.robofont.compileSettings.checkOutlines"] = True

# boolean, default is True
font.lib["com.typemytype.robofont.compileSettings.autohint"] = True

# boolean, default is False
font.lib["com.typemytype.robofont.compileSettings.releaseMode"] = True

# string, path where the last binary is been stored
# overriden every time a binary is generated
font.lib["com.typemytype.robofont.compileSettings.path"] = 'path/to/binary'

# string, either 'ttf' or 'otf' or 'pfa'
font.lib["com.typemytype.robofont.compileSettings.generateFormat"] = 'otf'

# boolean, default is True
# it creates a dummy digital signature open type table
# necessary for using the in Microsoft Office applications
# check https://docs.microsoft.com/en-us/typography/opentype/spec/dsig
# and also https://typedrawers.com/discussion/192/making-ot-ttf-layout-features-work-in-ms-word-2010
font.lib["com.typemytype.robofont.compileSettings.createDummyDSIG"] = True

# number, default is 0
font.lib["com.typemytype.robofont.italicSlantOffset"] = -10

# boolean, default is True
font.lib["com.typemytype.robofont.shouldAddPointsInSplineConversion"] = True

# number, default is 2
# amount of distance in UPM between 1/4 of the curve and 3/4 of the curve
# that is accepted before adding an extra point
font.lib["com.typemytype.robofont.splineConversionFuzz"] = 2

# list of dictionaries (default is empty), with the following keys:
#    uniqueKey: unique key to identify the smart set item, must be unique, string
#    smartSetName: name of the smart set item, string
#    query: a predicate string, there is no query when glyphNames are set
#           see https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/Predicates/Articles/pSyntax.html#//apple_ref/doc/uid/TP40001795-SW1
#    glyphNames: a list of glyph names (strings) no need to set a query (build automatically when glyphNames are set)
#    group: a list of smart sets items
font.lib["com.typemytype.robofont.smartSets"]

# number, default is 0.5
# only applied on otfs
font.lib["com.typemytype.robofont.roundTolerance"] = 0.5

# check https://unifiedfontobject.org/versions/ufo3/lib.plist/#publicglyphorder
font.lib["public.glyphOrder"] = ['A', 'B', 'C']

# check https://unifiedfontobject.org/versions/ufo3/lib.plist/#publicskipexportglyphs
font.lib["public.skipExportGlyphs"] = ['A', 'B', 'C']

# check https://unifiedfontobject.org/versions/ufo3/lib.plist/#publicpostscriptnames
font.lib["public.postscriptNames"] = {'designName': 'productionName'}

# check https://unifiedfontobject.org/versions/ufo3/glyphs/glif/#publicmarkcolor
glyph.lib['public.markColor'] = (1, 0, 0, 1)

# check https://unifiedfontobject.org/versions/ufo3/glyphs/glif/#publictruetypeinstructions
glyph.lib['public.truetype.instructions']

# list, default is empty
# check https://defcon.robotools.dev/en/latest/objects/unicodedata.html#defcon.UnicodeData.sortGlyphNames
font.lib["com.defcon.sortDescriptor"] = ['']

# string, default is empty
# when a font has the 'public.postscriptNames' lib this will not be used
# otherwise, RoboFont will look for this one and it will send it to FDK
# check http://adobe-type-tools.github.io/afdko/MakeOTFUserGuide.html
font.lib["com.typesupply.ufo2fdk.glyphOrderAndAliasDB"] = "uni0BAF    taYa"

# boolean, default is False
# if True will compile OT features with feaPyFoFum
# check https://github.com/typesupply/feaPyFoFum
font.lib["com.typesupply.feaPyFoFum.compileFeatures"]

# boolean, default is False
font.lib["com.robofont.generateFeaturesWithFontTools"] = True

# boolean, default is True
# when decompose is False
# if this lib is set to True
# components with a transformation impling more than translation
# will be decomposed, otherwise they will stay as components
font.lib["com.robofont.generateCheckComponentMatrix"]

# boolean, default is False
# adds subroutinization in FDK
font.lib["com.robofont.generateTurnOnSubroutinzations"]

# boolean, default is False
# check -gs flag here http://adobe-type-tools.github.io/afdko/MakeOTFUserGuide.html#makeotf-options
font.lib["com.robofont.generateOmitNotMentionedGlyphs"]

# string, either "fontmake" or "afdko", default is "afdko"
font.lib["com.robofont.fontCompilerTool"]

```