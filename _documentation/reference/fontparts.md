---
layout: page
title: FontParts + extras
tags:
  - scripting
  - fontParts
tree:
  - ../api/fontParts/*
treeCanHide: true
---

<object data="{{site.baseurl}}/images/reference/api/fontPartsMap.svg" type="image/svg+xml"></object>

{% tree page.url levels=3 %}

*[FontParts] is an application-independent object model and API for programmatically creating and editing parts of fonts during the type design process.*

RoboFont 3 implements the FontParts API and extends it with several custom methods and attributes.

> - {% internallink "topics/understanding-contours" %}
> - {% internallink "topics/pens" %}
> - {% internallink "topics/glyphmath" %}
> - {% internallink "topics/boolean-glyphmath" %}
> - {% internallink "tutorials/using-glyphmath" %}
> - {% internallink "topics/transformations" %}
{: .seealso }

> - RoboFont 1 used the ealier [RoboFab] API
> - see the {% internallink "robofab-fontparts" text="differences between RoboFab and FontParts" %}
{: .note }

[RoboFab]: http://robofab.org/
[FontParts]: http://fontparts.robotools.dev/
