---
layout: page
title: Search Glyphs panel
tags:
  - character set
---

* Table of Contents
{:toc}

The {% internallink "workspace/font-overview" %} includes a *Search Glyphs* panel for creating search queries and finding glyphs in a font. It can toggled using the *Views* buttons in the Font Overview window, or using the keyboard shortcut ⌘ + F.


Search expressions
------------------

{% image reference/workspace/font-overview/search-glyphs-panel.png %}

A search query is made out of one or more search expressions, combined with a boolean operator. Search expressions may use different types of glyph attributes and conditions.

Use the **+** button on the right side of the panel to add a new search expression to the query, and the **-** button to remove an expression.

### Glyph attributes

Search expressions use the same glyph attributes as the {% internallink "workspace/font-overview#list-mode" text="Font Overview’s List Mode" %}.

{% image reference/workspace/font-overview/search-glyphs-panel_glyph-attributes.png %}

<table>
  <tr>
    <th width='25%'>attribute</th>
    <th width="55%">description</th>
    <th width="20%">type</th>
  </tr>
  <tr>
    <td>Name</td>
    <td>Glyph name.</td>
    <td><code>str</code></td>
  </tr>
  <tr>
    <td>Width</td>
    <td>Glyph width.</td>
    <td><code>int</code> or <code>float</code></td>
  </tr>
  <tr>
    <td>Left sidebearing</td>
    <td>Glyph left sidebearing.</td>
    <td><code>int</code> or <code>float</code></td>
  </tr>
  <tr>
    <td>Right sidebearing</td>
    <td>Glyph right sidebearing.</td>
    <td><code>int</code> or <code>float</code></td>
  </tr>
  <tr>
    <td>Unicode</td>
    <td>Glyph unicode.</td>
    <td><code>hex</code></td>
  </tr>
  <tr>
    <td>Contours</td>
    <td>Number of contours in the glyph.</td>
    <td><code>int</code></td>
  </tr>
  <tr>
    <td>Components</td>
    <td>Number of components in the glyph.</td>
    <td><code>int</code></td>
  </tr>
  <tr>
    <td>Anchors</td>
    <td>Number of anchors in the glyph.</td>
    <td><code>int</code></td>
  </tr>
  <tr>
    <td>Components names</td>
    <td>A comma-separated list of the base glyph names of all components in the glyph.</td>
    <td><code>list</code></td>
  </tr>
  <tr>
    <td>Anchor names</td>
    <td>A comma-separated list of all anchor names in the glyph.</td>
    <td><code>list</code></td>
  </tr>
  <tr>
    <td>Note</td>
    <td>Glyph note.</td>
    <td><code>str</code></td>
  </tr>
  <tr>
    <td>Empty</td>
    <td>A bool indicating if the glyph is empty.</td>
    <td><code>bool</code></td>
  </tr>
  <tr>
    <td>Glyph changed</td>
    <td>A bool indicating if the glyph has been changed since the last saved version.</td>
    <td><code>bool</code></td>
  </tr>
  <tr>
    <td>Template</td>
    <td>A bool indicating if the glyph is a template glyph.</td>
    <td><code>bool</code></td>
  </tr>
  <tr>
    <td>Skip Export</td>
    <td>A bool indicating if the glyph should not be included in generated fonts.</td>
    <td><code>bool</code></td>
  </tr>
  <tr>
    <td>Mark Color</td>
    <td>The mark color of the glyph.</td>
    <td><code>tuple</code></td>
  </tr>
</table>

### Conditions

Each type of attribute offers a different set of filtering conditions.

| type             | conditions                                            |
| ---------------- | ----------------------------------------------------- |
| `str`            | is, is not, contains, begins with, ends with, matches |
| `int` or `float` | is, is not, is less than, is greater than             |
| `list`           | is not, contains                                      |
| `bool`           | is, is not                                            |
| `tuple`          | is, is not, contains, matches                         |

### Boolean operators

Search expressions can be combined using boolean operators.

{% image how-tos/searching-glyphs_boolean-filter.png %}

<table>
  <tr>
    <th width='20%'>options</th>
    <th width='20%'>operator</th>
    <th width="60%">result</th>
  </tr>
  <tr>
    <td>All</td>
    <td><code>AND</code></td>
    <td>Show glyphs which match all queries.</td>
  </tr>
  <tr>
    <td>Any</td>
    <td><code>OR</code></td>
    <td>Show glyphs which match at least one query.</td>
  </tr>
  <tr>
    <td>None</td>
    <td><code>NOT</code></td>
    <td>Show glyphs which don’t match any query.</td>
  </tr>
</table>

### Regular expressions

The *matches* condition (available for glyph names and mark colors) supports regular expressions. Regex is useful for creating smarter, more compact search expressions.

Examples:

`[a-z]`
: Lists all lowercase glyphs.

`[A-Z]`
: Lists all uppercase glyphs.

`[A-z]`
: Lists all 52 uppercase and lowercase glyphs.

`[a-z](caron|cedilla|ogonek|commaaccent|grave|acute|dieresis)`
: Lists all lowercase glyphs containing these accents.

`[A-Z]*`
: Lists all glyphs with a named made entirely of uppercase letters (A, B, C, AE, IJ...)

> - [regex101](https://regex101.com)
> - [It's time to learn regular expressions - PyCon2017](https://youtu.be/abrcJ9MpF60)
{: .seealso }

Saving Search Queries as Smart Sets
-----------------------------------

Search queries can be saved as Query-based or List-based Smart Sets using the two buttons at the top of the Search Glyphs bar.

{% image how-tos/searching-glyphs_queries-save.png %}

<table>
  <tr>
    <th width="25%">option</th>
    <th width="75%">description</th>
  </tr>
  <tr>
    <td>Selection to Set</td>
    <td>
      <p>Save the selected glyphs as a <a href="../../../workspace/font-overview/smart-sets-panel#creating-list-based-smart-sets">List-based Smart Set</a>.</p>
      <p>List-based Smart Sets display glyphs based on a static list of glyph names.</p>
    </td>
  </tr>
  <tr>
    <td>Save Set</td>
    <td>
      <p>Save the current query as a <a href="../../../workspace/font-overview/smart-sets-panel#creating-query-based-smart-sets">Query-based Smart Set</a>.</p>
      <p>Query-based Smart Sets search the font dynamically, so the result is updated based on changes in the font.</p>
    </td>
  </tr>
</table>
