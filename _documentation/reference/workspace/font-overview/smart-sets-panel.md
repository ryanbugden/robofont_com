---
layout: page
title: Smart Sets panel
tags:
  - character set
  - smart sets
---

* Table of Contents
{:toc}

The Font Overview window includes a collapsible *Smart Sets* panel, which can be toggled using the *Sets* button.

{% image reference/workspace/font-overview/smart-sets.png %}

> The *Sets* button can be found at the top right of the toolbar in {% internallink "workspace/window-modes#multi-window-mode" text="Multi-Window Mode" %}, and at the bottom left of the window in {% internallink "workspace/window-modes#single-window-mode" text="Single Window mode" %}.
{: .note }

The Smart Sets panel shows both *Default Sets* (global) and *Font Sets* (local).

Smart Sets can be grouped into folders, and folders can be nested.

The Smart Sets interface works as a filter: when one or more sets are selected, only the glyphs which belong to those sets are displayed in the Font Overview.

> - {% internallink 'topics/smartsets' %}
{: .seealso }


Actions
-------

The Smart Sets panel supports the following actions:

<table>
  <tr>
    <th width='35%'>action</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>click</td>
    <td>Show only the glyphs in the selected set(s)</td>
  </tr>
  <tr>
    <td>⌥ + click</td>
    <td>Show all glyphs in the font, and select all glyphs in the set</td>
  </tr>
  <tr>
    <td>double click</td>
    <td>Edit the Smart Set item, either as a search query or as a list of glyph names</td>
  </tr>
  <tr>
    <td>right click (only for list‑based Smart Sets)</td>
    <td>Show a contextual menu with a list of missing glyphs and an option to create them</td>
  </tr>
  <tr>
    <td>click + drag</td>
    <td>
      <dl>
        <dt>drop between sets</dt>
        <dd>change the order of the sets</dd>
        <dt>drop on top of Default Sets / Font Sets</dt>
        <dd>copy dragged set to location</dd>
      </dl>
    </td>
  </tr>
  <tr>
    <td>delete (del or backspace)</td>
    <td>Remove the selected Smart Set</td>
  </tr>
  <tr>
    <td>drag and drop glyphs</td>
    <td>Drag selected glyphs from the Font Overview into a set to add them to the set</td>
  </tr>
  <tr>
    <td>esc</td>
    <td>Reset the font overview query</td>
  </tr>
  <tr>
    <td>⌘ + </td>
    <td>Reset the font overview query</td>
  </tr>
</table>


Options
-------

The popup menu at the bottom left of the panel provides the following options:

{% image reference/workspace/font-overview/smart-sets_options.png %}

<table>
  <thead>
    <tr>
      <th width='35%'>option</th>
      <th width='65%'>description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Create new set from glyph names</td>
      <td>Opens a sheet to create a new Smart Set from a list of glyph names</td>
    </tr>
    <tr>
      <td>New Default Sets Folder</td>
      <td>Create a new folder to group default sets</td>
    </tr>
    <tr>
      <td>New Font Sets Folder</td>
      <td>Create a new folder to group font sets</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Delete Item</td>
      <td>Delete the selected set</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Export Default Sets</td>
      <td>Export default sets to a <code>.roboFontSets</code> file</td>
    </tr>
    <tr>
      <td>Load Default Sets</td>
      <td>Load default sets from a <code>.roboFontSets</code> file</td>
    </tr>
    <tr>
      <td>Export Font Sets</td>
      <td>Export font sets to a <code>.roboFontSets</code> file</td>
    </tr>
    <tr>
      <td>Load Font Sets</td>
      <td>Load font sets from a <code>.roboFontSets</code> file</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Export To a Font Group</td>
      <td>Export the selected set to a group</td>
    </tr>
  </tbody>
</table>


Creating list-based Smart Sets
------------------------------

To create a new list-based Smart Set, choose *Create new set from glyph names* from the options menu. This will open a sheet where you can input the name and glyph names in the Smart Set:

{% image reference/workspace/font-overview/smart-sets_create-set-from-names.png %}

Choose between *Default Set* (saved in the app defaults and available for all fonts) or *Font Set* (saved in the font and available only for this font). Press OK to create the Smart Set.

### Add missing glyphs

Each list-based Smart Set displays a counter with the total number of glyphs in the set. If available, a second number shows how many glyphs in the set are missing from the font.

{% image reference/workspace/font-overview/smart-sets_counters.png %}

To create missing glyphs in a Smart Set, click on the counter next to the Smart Set – this will open a popover with a list of missing glyph names:

{% image reference/workspace/font-overview/smart-sets_create-missing-glyphs.png %}

Select which glyphs you wish to create, and click on *Create Missing Glyphs*. This will open the *Add Glyphs* sheet with additional options to automatically add unicodes, add as template glyphs, mark glyphs, etc. See [Adding glyphs using the Add Glyphs sheet](/documentation/how-tos/adding-and-removing-glyphs/#adding-glyphs-using-the-add-glyphs-sheet).


Creating query-based Smart Sets
-------------------------------

Query-based Smart Sets are defined by one or more search queries. The glyph names in these sets are the result of the query applied to the current font.

To create a query-based Smart Set, make a new search glyph query using the {% internallink 'workspace/font-overview/search-glyphs-panel' %}, then click on the *Save Set* button.

{% image reference/workspace/font-overview/search-glyphs-panel.png %}

> - {% internallink "how-tos/mojo/smart-sets" %}
> - {% internallink "workspace/font-overview/search-glyphs-panel" %}
> - {% internallink "adding-and-removing-glyphs" %}
> - {% internallink "defining-character-sets" %}
{: .seealso }
