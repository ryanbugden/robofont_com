---
layout: page
title: Anchors
tags:
  - anchors
---

* Table of Contents
{:toc}


Definition
----------

Anchors represent a position, and have a name. They are commonly used as reference points for attaching components to base glyphs.

{% image reference/workspace/glyph-editor/glyph-editor_anchors.png %}

Anchor names are required, but must not be unique. Anchor may also have a color and a unique identifier.

Individual anchor colors are displayed in the Glyph View if available, and can be edited in the [Anchors] section of the Inspector panel.


Adding anchors
--------------

Anchors are added with the *Add Anchor popover*. To open this popover, right-click at the desired anchor position using the {% internallink 'workspace/glyph-editor/tools/editing-tool' %}, and choose *Add Anchor* from the [contextual menu](../#contextual-menu). The popover will close as soon as you click outside of it.

{% image reference/workspace/glyph-editor/glyph-editor_add-anchor.png %}

<table>
  <tr>
    <th width='35%'>action</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Anchor Name</td>
    <td>Provide a name for the anchor (required).<br/>Anchor names must not be unique.</td>
  </tr>
  <tr>
    <td>Add or Enter</td>
    <td>Add the anchor at the mouse click position.</td>
  </tr>
</table>


Removing anchors
----------------

To remove one or more anchors, select them with the {% internallink 'workspace/glyph-editor/tools/editing-tool' %} and hit the backspace key.


Anchors Inspector
-----------------

Anchor properties can be edited in the {% internallink "inspector#anchors" text="Anchors" %} section of the Inspector.

{% image reference/workspace/glyph-editor/inspector_anchors.png %}


Edit an anchor
--------------

To edit an anchor you need to double-click on the anchor using the {% internallink 'glyph-editor/tools/editing-tool' %}. You will see a popover appear. The popover allows you to change the anchor’s name and position.

{% image reference/workspace/glyph-editor/glyph-editor_contextual-menu-anchor.png %}


- - -

> - [UFO3 Specification > Anchor](http://unifiedfontobject.org/versions/ufo2/glyphs/glif/#anchor)
> - {% internallink 'reference/api/fontParts/ranchor' text='FontParts API > RAnchor' %}
> - {% internallink "how-tos/building-accented-glyphs" %}
{: .seealso }

[Anchors]: ../../inspector#anchors



