---
layout: page
title: Contours
tags:
  - contours
---

* Table of Contents
{:toc}


Description
-----------

Contours are visual descriptions of glyph shapes using bezier curves.

Bezier curves were introduced into type design in the 1980s as two competing flavors, PostScript (Adobe) and TrueType (Microsoft/Apple). The PostScript format used cubic bezier curves, while the TrueType used quadratic ones. The formats evolved and were merged into the OpenType format, but the distiction remains between OpenType-CFF (cubic) and OpenType-TTF (quadratic) fonts.

The Ikarus format which existed before that was based on circle segments.

{% image tutorials/glyph-editor.png %}

<!-- Segment types -->

Contours can be made of line and curve segments.

Line segments
: are made of two on-curve points

Curve segments
: are made of two on-curve points and off-curve points

<!-- Open and close contours -->

Contours can be open or closed. Open contours are allowed in UFO, but not in OpenType.

### Cubic and quadratic beziers

RoboFont uses cubic bezier curves by default. This is the type of contours which is most familiar to designers, since it’s used by vector design applications. Cubic beziers are part of Adobe’s PostScript page description language, and are used by OpenType-CFF fonts.

Since version 4.0, RoboFont offers full support for quadratic bezier curves. This allows for a finer control over outlines meant to be exported as TrueType fonts.

<div class='row'>
  <div class='col' style='vertical-align:top;'>
    {% image reference/workspace/glyph-editor/display-cubics.png caption="cubic curves" %}
  </div>
  <div class='col' style='vertical-align:top;'>
    {% image reference/workspace/glyph-editor/display-quadratics.png caption="quadratic curves" %}
  </div>
</div>

> - UFO3 specification allows the existence of both quadratics and beziers segments in the same contour. RoboFont abides to the specification, be aware!
{: .warning }

> - {% internallink "tutorials/drawing-with-quadratic-curves" %}
{: .seealso }

### Point smoothness

Bezier points can be *smooth* or *unsmooth*.

smooth point
: segments are continuous

unsmooth point
: segments are at an angle

Point smoothness can be toggled on / off by double clicking the point.

Smooth points retain continuity between segments when its handles are moved.

The angle of handle is locked to the angle of the opposing line segment of or to the angle of the handle of the oppposing curve segment.

### Point representations

Points are represented by different icons depending on their smoothness and the type of segments they connect:

{% image /reference/workspace/glyph-editor_contours-point-types.png %}

<table>
  <thead>
    <tr>
      <th width="12%">Icon</th>
      <th width="88%">Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <img src="../../../../../images/reference/workspace/glyph-editor/point-types/square.png">
      </td>
      <td>
        <p><strong>Square</strong></p>
        <p>Unsmooth connection between line and curve segments, or two line segments</p>
      </td>
    </tr>
    <tr>
      <td>
        <img src="../../../../../images/reference/workspace/glyph-editor/point-types/triangle.png">
      </td>
      <td>
        <p><strong>Triangle</strong></p>
        <p>Smooth connection between line segment and curve segment</p>
      </td>
    </tr>
    <tr>
      <td>
        <img src="../../../../../images/reference/workspace/glyph-editor/point-types/circle.png">
      </td>
      <td>
        <p><strong>Circle without stroke</strong></p>
        <p>Unsmooth connection between two curve segments, handles can move independently</p>
      </td>
    </tr>
    <tr>
      <td>
        <img src="../../../../../images/reference/workspace/glyph-editor/point-types/circleWithBlackStroke.png">
      </td>
      <td>
        <p><strong>Circle with stroke</strong></p>
        <p>Smooth connection between two curve segments, handles locked to the same angle</p>
      </td>
    </tr>
    <tr>
      <td>
        <img src="../../../../../images/reference/workspace/glyph-editor/point-types/bezierControlPoint.png">
      </td>
      <td>
        <p><strong>Control point only connected to anchor</strong></p>
        <p>Cubic bezier control points. Cubic beziers only allow for two control points, each one connected to an anchor</p>
      </td>
    </tr>
    <tr>
      <td>
        <img src="../../../../../images/reference/workspace/glyph-editor/point-types/quadraticsControlPoint.png">
      </td>
      <td>
        <p><strong>Connected control points</strong></p>
        <p>Quadratic bezier control point. Quadratic beziers allow for an infinite amount of control points</p>
      </td>
    </tr>
    <tr>
      <td>
        <img src="../../../../../images/reference/workspace/glyph-editor/point-types/cross.png">
      </td>
      <td>
        <p><strong>Circle or square with cross underneath</strong></p>
        <p>On-curve point matching with off-curve point</p>
      </td>
    </tr>
    <tr>
      <td>
        <img src="../../../../../images/reference/workspace/glyph-editor/point-types/firstPoint.png">
      </td>
      <td>
        <p><strong>Circle around on-curve or off-curve point</strong></p>
        <p>Signals the first point in the contour, either on-curve or off-curve</p>
      </td>
    </tr>
    <tr>
      <td>
        <img src="../../../../../images/reference/workspace/glyph-editor/point-types/arrow.png">
      </td>
      <td>
        <p><strong>Arrow</strong></p>
        <p>Placed on the first on-curve point of the contour (there might be off-curve points preceding it), it indicates the direction of the contour</p>
      </td>
    </tr>
  </tbody>
</table>


Creating and editing contours
-----------------------------

Contours are usually created using the {% internallink 'workspace/glyph-editor/tools/bezier-tool' %} and edited using the {% internallink 'workspace/glyph-editor/tools/editing-tool' %}. It is also possible to draw inside a glyph with code – see {% internallink "tutorials/scripts-glyph#drawing-inside-a-glyph-with-a-pen" text='this example' %}.


Contours Inspector
------------------

Contour and point properties can be edited in the {% internallink "inspector#points" text="Points" %} section of the Inspector.

{% image reference/workspace/glyph-editor/inspector_points.png %}


First Contour Point
-------------------

According to the UFO specification, an open contour must start with a `move` type point:

```xml
<?xml version='1.0' encoding='UTF-8'?>
<glyph name="contours" format="2">
  <advance width="500"/>
  <outline>
    <contour>
      <point x="144" y="314" type="move"/>
      <point x="204" y="159" type="line"/>
      <point x="251" y="117"/>
      <point x="308" y="100"/>
      <point x="377" y="106" type="curve"/>
    </contour>
  </outline>
</glyph>
```

while a closed contour can start with either `curve`, `line` or `qCurve` point

```xml
<?xml version='1.0' encoding='UTF-8'?>
<glyph name="contours" format="2">
  <advance width="500"/>
  <outline>
    <contour>
      <point x="120" y="167" type="line"/>
      <point x="64" y="314" type="line"/>
      <point x="96" y="150" type="line"/>
    </contour>
    <contour>
      <point x="164" y="314" type="curve"/>
      <point x="224" y="159" type="line"/>
      <point x="178" y="203"/>
      <point x="158" y="254"/>
    </contour>
    <contour>
      <point x="324" y="159" type="qcurve"/>
      <point x="264" y="314" type="line"/>
      <point x="260" y="269"/>
      <point x="290" y="192"/>
    </contour>
  </outline>
</glyph>
```

> If you want to inspect closely the content of a glif file inside a UFO, you can use the [glifViewer](https://github.com/typemytype/glifViewerRoboFontExtension) extension by Frederik Berlaen. Using the same extension, you can paste these examples and observe the outlines directly in RoboFont
{: .seealso}

As you can see from the previous examples, off-curve points do not have a type attribute, as they can be inferred based on their relative position to the on-curve points. So, it is possible to move an off-curve point at the top of the list without breaking the UFO specification.

```xml
<?xml version='1.0' encoding='UTF-8'?>
<glyph name="contours" format="2">
  <advance width="500"/>
  <outline>
    <contour>
      <point x="260" y="269"/>
      <point x="290" y="192"/>
      <point x="324" y="159" type="qcurve"/>
      <point x="264" y="314" type="line"/>
    </contour>
  </outline>
</glyph>
```

This is not an issue while interpolating outlines with RoboFont or Skateboard, because their underlying API – [fontMath](https://github.com/robotools/fontMath) – automatically picks the first on-curve point as the first contour point. Differently, tools like [fontMake](https://github.com/googlefonts/fontmake) require strict compatibility between outlines, so if contour FOO starts with an off-curve point, contour BAR should do the same.

To provide finer control over outlines compatibility, RoboFont visualizes the first point of the contour on an off-curve point if necessary, which will detach the "first contour point" icon from the arrow that indicates the contour direction. 

{% image reference/workspace/glyph-editor/detached_first_contour_point.png %}

Contextual menu
---------------

<!-- The Glyph Editor’s contextual menu offers several actions to modify contours. -->

The contextual menu can be opened with a right-click using the {% internallink 'glyph-editor/tools/editing-tool' %}. A different set of options is displayed depending on the context:

<div class='row'>
<div class='col' style='vertical-align:top;'>
{% image reference/workspace/glyph-editor/glyph-editor_contextual-menu.png caption='no selection' %}
</div>
<div class='col' style='vertical-align:top;'>
{% image reference/workspace/glyph-editor/glyph-editor_contextual-menu-points.png caption='contour / multiple points' %}
</div>
<div class='col' style='vertical-align:top;'>
{% image reference/workspace/glyph-editor/glyph-editor_contextual-menu-point.png caption='single point' %}
</div>
</div>

### Contour / Multiple points selection actions

<table>
  <thead>
    <tr>
      <th width="35%">action</th>
      <th width="65%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Remove Overlap</td>
      <td>Remove overlaps in the selected contours</td>
    </tr>
    <tr>
      <td>Add Extreme Points</td>
      <td>Add extreme points to the selected contours</td>
    </tr>
    <tr>
      <td>Close Open Contours</td>
      <td>Connects two start or end points included in the selection, giving priority to start and end points of the same open contour. The selection might include non-end points, they will be ignored</td>
    </tr>
    <tr>
      <td>Reverse Contours</td>
      <td>Reverse selected contours</td>
    </tr>
    <tr>
      <td>Convert Contour to Cubic</td>
      <td>Convert selected contour or segments to cubic</td>
    </tr>
    <tr>
      <td>Convert Contour to Quadratic</td>
      <td>Convert selected contour or segments to quadratic</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Break Contours</td>
      <td>If an entier contour is selected, the contour is deleted. Otherwise only the selected segments are deleted</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Edit Labels</td>
      <td>Edits labels to the selected points</td>
    </tr>
  </tbody>
</table>


### Single point actions
<table>
  <thead>
    <tr>
      <th width="35%">action</th>
      <th width="65%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Break Contour</td>
      <td>Breaks contour at the selected point</td>
    </tr>
    <tr>
      <td>Reverse Contour</td>
      <td>Reverse contour</td>
    </tr>
    <tr>
      <td>Set Start Point</td>
      <td>Set the selected point as starting point</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Add Horizontal Guideline</td>
      <td>Adds a horizontal guidelines lying on the selected point</td>
    </tr>
  </tbody>
</table>


- - -

> - [UFO3 Specification > glyf > Point](http://unifiedfontobject.org/versions/ufo3/glyphs/glif/#point)
> - FontParts API > {% internallink 'reference/api/fontParts/rcontour' %} / {% internallink 'reference/api/fontParts/rpoint' %} / {% internallink 'reference/api/fontParts/rbpoint' %} / {% internallink 'reference/api/fontParts/rsegment' %}
{: .seealso }
