---
layout: page
title: Images
tags:
  - images
---

* Table of Contents
{:toc}


Description
-----------

Each glyph layer may include a bitmap image for reference – for example scans of drawings or printed typeface samples for digitisation.


Adding images
-------------

To add an image to the current layer of the current glyph, drag & drop an image file from Finder into the Glyph View. Image formats `.png`, `.jpeg` or `.tiff` are supported.

{% image reference/workspace/glyph-editor/image_editing.png %}

Each layer can contain only one image.

> To add multiple images to a glyph, use multiple layers (one for each image).
{: .tip }

Imported images are stored in an `images` directory inside the UFO3 font.


Removing images
---------------

To delete an image, select it with the {% internallink 'glyph-editor/tools/editing-tool' %}, and press the backspace key.


Transforming images
-------------------

Images can be freely moved, scaled and rotated using the {% internallink 'editing-tool' %}. To activate {% internallink 'glyph-editor/transform' %} mode, select the image and use the shortkeys ⌘ + T or choose *Glyph > Transform* from the main application menu.

Images can also be transformed using the [Set Scale sheet](#set-scale-sheet).


Contextual menu
---------------

Image color, opacity and scale can be adjusted using a contextual menu, which can be opened by right-clicking in the Glyph View using the Editing Tool.

{% image reference/workspace/glyph-editor/image_menu.png %}

<table>
  <thead>
    <tr>
      <th width="25%">action</th>
      <th width="75%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Brightness</td>
      <td>Adjust the brightness of the image.</td>
    </tr>
    <tr>
      <td>Contrast</td>
      <td>Adjust the contrast of the image.</td>
    </tr>
    <tr>
      <td>Saturation</td>
      <td>Adjust the saturation of the image.</td>
    </tr>
    <tr>
      <td>Sharpness</td>
      <td>Adjust the Sharpness of the image.</td>
    </tr>
    <tr>
      <td>Color</td>
      <td>Colorize the image. Use the <em>Red</em>, <em>Green</em>, <em>Blue</em> & <em>Alpha</em> sliders to adjust the color.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Set Scale</td>
      <td>Opens the <a href="#set-scale-sheet">Set Scale sheet</a> to adjust image size and positioning.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Lock Image</td>
      <td>Locks the image, to prevent it from being edited accidentally while editing the contours. Use the contextual menu to unlock it.</td>
    </tr>
  </tbody>
</table>


Set Scale sheet
---------------

The *Set Image Scale sheet* is a helper to adjust an image to the glyph’s coordinate space. It can be opened by choosing *Set Scale* from the contextual menu.

The sheet makes it easy to transform an image by clicking on 3 reference points:

{% image reference/workspace/glyph-editor/image_scaling.png %}

1. The origin point, or `(0, 0)`.

2. A point along the baseline. This will define the horizontal axis.

3. A point along the vertical axis, to define the height of the image. This point can be located at the x-height, cap-height, ascender or descender.

These positions are editable and can be fine tuned after they have been created – just click on a point to move it.

Click on the *Scale* button to apply the settings. The image will be transformed to match the positions defined by the 3 points.


- - -

> - [UFO3 Specification > images](http://unifiedfontobject.org/versions/ufo3/images/)
> - {% internallink 'reference/api/fontParts/rimage' text='FontParts API > RImage' %}
{: .seealso }

