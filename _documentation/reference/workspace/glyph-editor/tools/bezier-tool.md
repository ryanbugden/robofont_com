---
layout: page
title: Bezier Tool
tags:
  - contours
---

* Table of Contents
{:toc}

{% image reference/workspace/toolbarToolsPen.png %}

The *Bezier Tool* is an interactive tool to draw Bezier contours. To activate the tool, you can click on the pen icon in the toolbar of the {% internallink 'workspace/glyph-editor' %} or you can select "Drawing Tool" in the {% internallink "reference/workspace/application-menu#glyph" text="Glyph application menu" %}.

RoboFont supports both Cubic and Quadratic bezier curves.

> - {% internallink "tutorials/drawing-with-quadratic-curves" %}
{: .seealso }


Actions
-------

{% video tutorials/drawingGlyphs_bezier-tool.mp4 %}

Actions with an active drawing contour:

<table class="table-35-65">
  <tr>
    <th>action</th>
    <th>description</th>
  </tr>
  <tr>
    <td>click</td>
    <td>Adds a line segment to the outline</td>
  </tr>
  <tr>
    <td>⇧ + click</td>
    <td>Add point constrained to x, y or 45° of previous on-curve point.</td>
  </tr>
  <tr>
    <td>drag</td>
    <td>Add BCPs. All new points will have a <code>smooth</code> attribute.</td>
  </tr>
  <tr>
    <td>⇧ + drag</td>
    <td>Move BCPs of last added point on x, y or 45° axis.</td>
  </tr>
  <tr>
    <td>⌥ + drag</td>
    <td>Remove the <code>smooth</code> attribute from BCP.</td>
  </tr>
  <tr>
    <td>⌥ + click on open contour</td>
    <td>Connect the active countour with the open contour</td>
  </tr>
  <tr>
    <td>⌥ + ⌘ + click</td>
    <td>Add a point on a contour.</td>
  </tr>
  <tr>
    <td>move over starting point</td>
    <td>Indicates if the contour can be closed.</td>
  </tr>
  <tr>
    <td>double click</td>
    <td>Add a point and close the contour.</td>
  </tr>
  <tr>
    <td>right click</td>
    <td>Add a quadratic off curve point.</td>
  </tr>
  <tr>
    <td>right click + drag</td>
    <td>Drag the added quadratic off curve point.</td>
  </tr>
</table>

Actions with no selection:

<table class="table-35-65">
  <tr>
    <th>action</th>
    <th>description</th>
  </tr>
  <tr>
    <td>click on open contour last point</td>
    <td>Make the open contour the active contour for drawing</td>
  </tr>
  <tr>
    <td>click anywhere else on the canvas</td>
    <td>Start a new outline by adding a point on the canvas</td>
  </tr>
</table>

> If the option *Use Shift 45° constrain* is selected in the {% internallink 'workspace/preferences-window/glyph-view' %}, angles will be constrained to multiples of 45° degrees instead of 90°.
{: .note }


Arrow keys
----------

{% video reference/workspace/glyph-editor/tools/bezier-arrowkeys.mp4 %}


| action | description |
| --- | --- |
| arrow keys  | Move last added point by 1 unit. |
| ⇧ + arrow keys | Move last added point by 10 units |
| ⇧ + ⌘ + arrow keys | Move last added point by 100 units. |
{: .table-35-65 }

> - {% internallink 'tutorials/drawing-glyphs' %}
> - {% internallink 'topics/drawing-environment' %}
{: .seealso }
