---
layout: page
title: License Window
---

The License Window is a dedicated window for activating your RoboFont license and displaying your license info.

{% image reference/workspace/license-window.png %}

<table>
  <thead>
    <tr>
      <th width="25%">title</th>
      <th width="75%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Import</td>
      <td>Open a dialog to import the license from a <code>.roboFontLicense</code> file. Alternatively, drag a <code>.roboFontLicense</code> file into the license box to import it.</td>
    </tr>
    <tr>
      <td>Remove license</td>
      <td>Remove the current license.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>robofont.com</td>
      <td>Open the <a href="http://robofont.com/">RoboFont website</a> in your browser.</td>
    </tr>
    <tr>
      <td>EULA</td>
      <td>Open the <a href="http://doc.robofont.com/license-agreement/">RoboFont EULA</a> in your browser.</td>
    </tr>
  </tbody>
</table>

## Getting a license

RoboFont licenses are encoded in `.roboFontLicense` files, which can be imported by the License Window.

{% image reference/workspace/license_icon.png %}

> - {% internallink "licensing" %}
{: .seealso }

## Activating your license

Drag your `.roboFontLicense` file into the License Window:

{% image reference/workspace/license-window_empty.png %}

The license box will display the information in your license file.
