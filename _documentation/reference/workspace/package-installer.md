---
layout: page
title: Package Installer
tags:
  - scripting
---

> [RoboFont 3.4]({{site.baseurl}}/downloads/)
{: .version-badge }

The Package Installer is an interface for installing packages from the {% glossary Python Package Index (PyPI) %} using {% glossary pip %}.

{% image reference/workspace/package-installer.png %}

The Package Installer makes packages available *only inside RoboFont*.

> If you wish to install packages for the system Python, use `pip` from the Terminal instead. These packages will also become available in RoboFont after a restart.
{: .note}

> - [Python Package Index (PyPI)](http://pypi.org/)
> - [pip](http://pip.pypa.io/)
{: .seealso }


Usage
-----

1. Choose a `pip` command from the popup list at the top left.
2. Type the name of a package. 
3. Click on the *Go!* button to run the command.
4. The output will be displayed in the text area at the bottom.

### Commands

<table>
  <tr>
    <th width='30%'>command</th>
    <th width='70%'>description</th>
  </tr>
  <tr>
    <td>search PyPI</td>
    <td>Search the PyPI database for packages with a name or summary containing a given string.</td>
  </tr>
  <tr>
    <td>install / upgrade</td>
    <td>Install a package from PyPI. If the package is already installed and a newer version is available, it will be updated.</td>
  </tr>
  <tr>
    <td>uninstall</td>
    <td>Uninstall a package.</td>
  </tr>
  <tr>
    <td>show package info</td>
    <td>Show information (such as version number, author, requirements etc.) about a given package.</td>
  </tr>
</table>

### Get info

Use the gears icon at the top right to:

- get a list of all packages installed with the Package Installer
- get the current version of `pip`
- open the folder where all packages are saved in Finder

