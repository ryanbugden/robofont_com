---
layout: page
title: Safe Mode
tags:
  - scripting
  - extensions
  - bugs
---

*Safe Mode* is a way to launch RoboFont without loading any extensions and start-up scripts. **It is very useful to debug and troubleshoot extensions.**

To open RoboFont in Safe Mode, hold the Shift key while launching the application.

{% image reference/workspace/safe-mode_application-menu.png %}

A *Safe Mode…* item will appear in the main menu, with an option to load all extensions and start-up scripts.