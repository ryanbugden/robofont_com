---
layout: page
title: Window Modes
tags:
  - UI
---

* Table of Contents
{:toc}

RoboFont consists of several windows, views and panels. The application’s interface can be organized in two different modes: *Single Window* and *Multi-Window*.

> The window mode can be set in {% internallink "preferences-window/miscellaneous" text="Preferences > Miscellaneous" %}.
{: .note }


Single Window Mode
------------------

In *Single Window* mode, all windows are collected as panels of one single application window. Individual panels can be shown/hidden using the *Views* icons at the bottom.

{% image reference/workspace/window-modes_single.png %}


Multi-Window Mode
-----------------

In *Multi-Window* mode, windows are available separately and each window can be freely resized and positioned. This can be useful when working with multiple monitors or with several fonts at the same time.

{% image reference/workspace/window-modes_multi.png %}

