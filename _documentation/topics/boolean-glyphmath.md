---
layout: page
title: Boolean Glyph Math
tags:
  - contours
draft: false
---

RoboFont adds common boolean path operations – *difference*, *union*, *intersection* and *exclusive or* – to glyph objects, using special characters as operators.

<table>
  <tr>
    <th width='50%'>operation</th>
    <th width='50%'>operator</th>
  </tr>
  <tr>
    <td>difference</td>
    <td><code>%</code></td>
  </tr>
  <tr>
    <td>union</td>
    <td><code>|</code></td>
  </tr>
  <tr>
    <td>intersection</td>
    <td><code>&amp;</code></td>
  </tr>
  <tr>
    <td>exclusive or</td>
    <td><code>^</code></td>
  </tr>
</table>

The example below shows the four boolean path operations in action:

{% image topics/glyph-booleans1.png %}

> When performing a *difference* operation, the order of the operands matters!
{: .note }

{% showcode topics/booleanGlyphMath.py %}
