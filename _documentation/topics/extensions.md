---
layout: page
title: Extensions
tags: extensions
hideTitle: false
draft: false
slideShows:
  extensions:
    - image: extraDocs/carousel/extensions/Ramsay-St.png
      caption: |
        [Ramsay St.](http://github.com/typemytype/ramsayStreetRoboFontExtension) by Frederik Berlaen
    - image: extraDocs/carousel/extensions/GlyphConstruction.png
      caption: |
        [GlyphConstruction](http://github.com/typemytype/glyphconstruction) by Frederik Berlaen
    - image: extraDocs/carousel/extensions/OverlayUFOs.png
      caption: |
        [Overlay UFOs](http://github.com/FontBureau/fbOpenTools) by David Jonathan Ross
    - image: extraDocs/carousel/extensions/Outliner.png
      caption: |
        [Outliner](http://github.com/typemytype/outlinerRoboFontExtension) by Frederik Berlaen
    - image: extraDocs/carousel/extensions/Prepolator.png
      caption: |
        [Prepolator](http://extensionstore.robofont.com/extensions/prepolator/) by Tal Leming
    - image: extraDocs/carousel/extensions/word-o-mat.png
      caption: |
        [word-o-mat](http://github.com/ninastoessinger/word-o-mat) by Nina Stössinger
    - image: extraDocs/carousel/extensions/CornerTools.png
      caption: |
        [CornerTools](http://github.com/roboDocs/CornerTools) by Loïc Sander
    - image: extraDocs/carousel/extensions/MetricsMachine.png
      caption: |
        [MetricsMachine](http://extensionstore.robofont.com/extensions/metricsMachine/) by Tal Leming
    - image: extraDocs/carousel/extensions/GlyphBrowser.png
      caption: |
        [GlyphBrowser](http://github.com/LettError/glyphBrowser) by Erik van Blokland
    - image: extraDocs/carousel/extensions/SpeedPunk.png
      caption: |
        [SpeedPunk](http://github.com/yanone/speedpunk) by Yanone
    - image: extraDocs/carousel/extensions/GlyphNanny.png
      caption: |
        [Glyph Nanny](http://github.com/typesupply/glyph-nanny) by Tal Leming
  autoPlay: true
  height: 550
---

*RoboFont offers an extensions infrastructure which makes it easy for developers to build and distribute tools, and for users to install and update them.*

* Table of Contents
{:toc}

{% include slideshows items=page.slideShows.extensions %}


Why extensions?
---------------

<!--
**RoboFont does not try to do everything a font editor could do.** We think this approach is problematic and leads to:

- rigid tools, which force users to doing things one way
- cluttered interface, with too many options which confuse users
- bloated, monolithic software which is harder to maintain
-->

RoboFont is built with a modular approach. Extensions are the optional modules.

RoboFont aims at being a {% internallink "topics/robofont-design-principles#the-role-of-robofont" text='solid and extensible core application' %}. It provides all the basic functionality required for type design and font production work, and also serves as a foundation – on top of which others can build their own specialized tools.

<!--
Extensions provide users with options. Instead of having a single tool which tries to solve all problems, RoboFont users have access to a flexible infra-structure which enables an *ecosystem of specialized tools* – built by multiple developers with different needs and different ideas.

> - {% internallink "topics/robofont-design-principles" %}
{: .seealso }

-->


What are extensions?
--------------------

<style>
.col { vertical-align: top; }
.col1 { width: 30%; }
</style>

*Extensions are additional software packages which add tools and functionality to RoboFont.*

<div class='row'>
<div class='col col1'>
<img src="{{ site.baseurl }}/images/topics/extension-icon.png" height='180px' />
</div>
<div class='col' markdown=1>
RoboFont extensions are simple macOS packages (folders that behave like single files in Finder) with a `.roboFontExt` file extension.

The {% internallink "reference/extensions/extension-file-spec" text='extension file format' %} is open and the process of creating extensions is easy and {% internallink "tutorials/from-tool-to-extension"  text='fully documented' %}.
</div>
</div>


Where to get extensions?
------------------------

<div class='row'>
<div class='col' markdown=1>
Both open-source and commercial extensions can be browsed and installed with {% internallink "topics/mechanic-2" text='Mechanic 2' %} – an extension to manage RoboFont extensions.

Open-source extensions
: are listed in the [Mechanic website](http://robofontmechanic.com/)

Commercial extensions
: can be purchased in the [Extension Store](http://extensionstore.robofont.com/)
</div>
<div class='col col1'>
<img src="{{ site.baseurl }}/images/topics/managing-extension-streams_mechanic2-file-icon.png" height='200px' />
</div>
</div>


How to install extensions?
--------------------------

The easiest way to install extensions is {% internallink 'installing-extensions-mechanic' text='using Mechanic' %}, which helps you to find extensions by developer or tags, and to check for and install updates automatically.

{% image topics/Mechanic2.png %}

Extensions can be {% internallink 'installing-extensions' text='installed manually' %} too – simply double-click the extension file!


How to use extensions?
----------------------

Once installed, extensions {% internallink 'extensions/using-extensions' text='integrate seamlessly' %} into the RoboFont interface, adding themselves to existing menus or toolbars. Check the extension’s documentation for specific usage instructions.


> - Check all the posts tagged with {% internallink "tags#extensions" text='#extensions' %}
{: .seealso}
