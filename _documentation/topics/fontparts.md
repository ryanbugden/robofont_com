---
layout: page
title: FontParts
tags:
  - scripting
  - FontParts
---

* Table of Contents
{:toc}

*[FontParts] is an application-independent font object API for creating and editing fonts.*

RoboFont implements the {% internallink 'reference/fontparts' text='FontParts object model' %} and adds some methods and attributes of its own.

{% image topics/fontPartsMap.gif %}

[FontParts]: http://github.com/robotools/fontParts


Font objects out-of-the-box
---------------------------

When writing code in the {% internallink 'workspace/scripting-window' %}, objects from the [fontParts.world] module are available out-of-the-box – so you can use `CurrentFont`, `CurrentGlyph`, `AllFonts` etc. directly, without having to import them at the top of your scripts.

{% image topics/fontparts-scripting-window.png %}

The main font object classes – `RFont`, `RGlyph`, `RContour` etc. – are also available.

[fontParts.world]: http://fontparts.robotools.dev/en/stable/objectref/fontpartsworld/


Font objects in imported modules
--------------------------------

To use font objects in external modules, it is necessary to import them from `mojo.roboFont` first:

```python
# myModule.py
from mojo.roboFont import CurrentFont

def getFont():
    return CurrentFont()
```

{% image topics/fontparts-scripting-window-module.png %}
