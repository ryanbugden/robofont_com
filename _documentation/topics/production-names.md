---
layout: page
title: Production names and final names
tags:
  - character set
  - glyph names
draft: false
---

* Table of Contents
{:toc}


Why different sets of names
---------------------------

The demands for glyph names during the design process and at the end of the production line are different.

### Production names

During the design process, glyph names should be logical, legible and descriptive – so it’s easy to find a particular glyph while working on a font, and it’s easy to guess what a glyph looks like (even when it’s still empty).

### Final names

Glyph names in final OpenType fonts must obey certain rules in order to work correctly in all environments.

For example, in order to enable text search in PDFs, it is necessary that glyph names follow the [Adobe Glyph List Specification]. In practice, this means that any glyph which does not have a readable name in the {% glossary AGLNF %} must be named with its unicode value: `uniXXXX`.

> - [Where and why glyph names are used (AGL)][Adobe Glyph List Specification]
{: .seealso }

[Adobe Glyph List Specification]: http://github.com/adobe-type-tools/agl-specification


glyphOrderAndAliasDB
--------------------

Thanks to the `glyphOrderAndAliasDB` file (GOADB) it is possible to get the best of both worlds:

- use ‘friendly’ glyph names during development (production names)
- switch to the final glyph names when generating OpenType fonts

This is specially useful when working with scripts other than Latin and Greek, for which the AGLNF dictates ‘unfriendly’ `uniXXXX` glyph names.

Under the hood, the switch from production names to final names is enabled by the GOADB file, which is supplied to `makeotf` or `fontmake` when generating OpenType fonts.

> When generating fonts with `makeotf`, it is necessary to enable the option *Release Mode* in order to have the GOADB names applied to the font.
{: .note }
