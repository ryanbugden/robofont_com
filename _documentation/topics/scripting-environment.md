---
layout: page
title: The scripting environment
---

* Table of Contents
{:toc}


Scripting Window
----------------

Code is written in the {% internallink "workspace/scripting-window" %}, where you can also run and browse through your scripts.

{% image topics/scripting-window.png %}

It is also possible to {% internallink 'tutorials/using-external-code-editors' text='use external code editors' %} to run scripts in RoboFont.

A separate {% internallink "workspace/output-window" %} is available to collect output from interactive tools.

{% image topics/scripting-environment.png %}


Python embedded
---------------

RoboFont comes with its own embedded Python interpreter, so you don’t need to install anything else to start scripting. RoboFont {{site.data.versions.roboFont}} comes with Python {{site.data.versions.python}}.

All modules from the {% internallink 'tutorials/python/standard-modules' text='Python Standard Library' %} are also included.

[Python Standard Library]: http://docs.python.org/3.7/library/


Batteries included
------------------

RoboFont’s APIs are all open and documented. Users are encouraged to write Python scripts to automate and customize the application to suit their needs.

All {% internallink 'reference/fontparts' text='FontParts' %} world objects – `CurrentFont`, `CurrentGlyph`, `AllFonts`, etc. – are available out-of-the-box for use in your scripts.

{% image topics/fontparts-scripting-window.png %}

You can use {% internallink 'topics/vanilla' %} to create interfaces, {% internallink 'reference/mojo' %} to access RoboFont’s own objects, [fontTools] to edit font binaries, … and so much more.

> - {% internallink 'reference/embedded-libraries' text='List of embedded libraries' %}
{: .seealso }

[fontTools]: http://github.com/fonttools/fonttools/


Package installer
-----------------

RoboFont also includes the {% internallink 'workspace/package-installer' %}, a {% glossary pip %} terminal which makes it easy to install modules from the {% glossary Python Package Index (PyPI) %}.

{% image reference/workspace/package-installer.png %}


Extensions platform
-------------------

{% image topics/extension-icon.png %}

In addition to the Scripting environment where you can write and execute code, RoboFont also offers an {% internallink 'topics/extensions' text='extensions platform' %} which makes it easy for developers to build and distribute extensions, and for users to install and update them.

{% internallink 'workspace/extension-builder' %}
: A dedicated window for building and editing extensions.

{% internallink 'extensions/building-extensions-with-script' text='ExtensionBundle' %}
: A Python object that lets you build extensions with a script.

{% internallink 'topics/mechanic-2' text='Mechanic 2' %}
: An extension to install and manage RoboFont extensions.
