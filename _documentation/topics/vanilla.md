---
layout: page
title: vanilla
tags:
  - vanilla
slideShows:
  widgets:
    - image: topics/vanilla-tests_1.png
    - image: topics/vanilla-tests_2.png
    - image: topics/vanilla-tests_3.png
  autoPlay: true
  height: 550
---

* Table of Contents
{:toc}


What is vanilla?
----------------

*vanilla* is a Python wrapper around the macOS native [Cocoa] user interface layer. It offers a {% glossary pythonic %} API for creating interfaces with code, rather than using visual tools like Apple’s own [Interface Builder].

vanilla was developed by Tal Leming, inspired by the classic {% glossary W %} library used in {% glossary RoboFog %}.

A wide array of tools are created with vanilla, from small scripts to extensions and applications. RoboFont itself is built using vanilla.

[Cocoa]: http://en.wikipedia.org/wiki/Cocoa_(API)
[Interface Builder]: http://developer.apple.com/xcode/interface-builder/

> - [macOS Human Interface Guidelines](http://developer.apple.com/library/content/documentation/UserExperience/Conceptual/OSXHIGuidelines/)
{: .seealso }


Using vanilla
-------------

vanilla is embedded in RoboFont, so you can start using it right away in your scripts:

```python
from vanilla import *
```

vanilla has a very complete [documentation][vanilla documentation] which is available directly from your scripting environment:

```python
from vanilla import Window
help(Window)
```

The above snippet will return the provided documentation for the `Window` object, including some sample code:

```python
from vanilla import *

class WindowDemo:

    def __init__(self):
        self.w = Window((200, 70), "Window Demo")
        self.w.myButton = Button((10, 10, -10, 20), "My Button")
        self.w.myTextBox = TextBox((10, 40, -10, 17), "My Text Box")
        self.w.open()

WindowDemo()
```

Run the sample code to open a demo window:

{% image topics/WindowDemo.png %}


Widgets overview
----------------

The screenshots below give an overview of the UI elements included in vanilla. These images are produced using vanilla’s own test files, which are included in the module.

```python
from vanilla.test.testAll import Test
Test()
```

{% include slideshows items=page.slideShows.widgets %}


> - [vanilla source code on GitHub](http://github.com/robotools/vanilla)
> - [vanilla documentation]
> - [Tal Leming introducing vanilla (RoboThon 2012)](http://vimeo.com/116064787#t=17m00s)
> - [Tal Leming: Building Stuff (RoboThon 2012)](http://vimeo.com/38358195)
> - [RoboFont Forum: vanilla](http://forum.robofont.com/tags/vanilla)
{: .seealso}

[vanilla documentation]: http://vanilla.robotools.dev
