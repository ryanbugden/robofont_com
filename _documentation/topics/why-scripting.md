---
layout: page
title: Why scripting?
tags:
  - scripting
---

Scripting plays an important role in typeface design. The process of creating a complete font or font family involves many repetitive tasks: drawing and spacing related shapes, building accented glyphs, setting font infos, interpolating weights, generating fonts, creating proofs – just to name a few tasks which are common to every project.

On top of that, your design might bring its own repetitive challenges: a special shading effect, some smart OpenType feature, a huge glyph set with hundreds of ligatures… *there should be no limits to imagination*.

{% image topics/C3PO-R2D2.jpg caption='droids in the desert of Tatooine — photo: Luke Skywalker' %}

Scripting allows us to build robots that take care of repetitive tasks, so we can focus on what we do best: *being creative*.

Scripting allows us to:

- save time by automating repetitive tasks
- be more precise by avoiding human errors
- explore and evaluate more options in less time
- generate complex shapes which are difficult to draw by hand
- iterate faster and make better decisions

In short, *scripting helps us to become better designers*.
