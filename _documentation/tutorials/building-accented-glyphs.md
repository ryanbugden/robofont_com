---
layout: page
title: Building accented glyphs
tags:
  - accents
  - components
---

* Table of Contents
{:toc}

*This page gives an overview of the different methods to build accented glyphs in RoboFont.*

> - {% internallink "topics/accented-glyphs" %}
{: .seealso }


Using the Add Glyphs sheet
--------------------------

The *Add Glyphs* sheet is RoboFont’s native interface for adding new glyphs to a font. It supports a very basic syntax for building glyphs using components and anchors.

{% image how-tos/adding-and-removing-glyphs_add-glyphs-sheet.png %}

> - {% internallink "how-tos/adding-and-removing-glyphs#glyph-construction-rules" %}
{: .seealso }


Using Glyph Construction
------------------------

The Glyph Construction language offers a more powerful syntax for building glyphs from components. It supports other positioning methods besides anchors, and includes many other features such as variables, transformations, setting glyph metrics, decomposing contours, etc.

Glyph Construction is available as the `glyphConstruction` Python module, which comes embedded in RoboFont 3, and as the [Glyph Construction] extension, which contains the module (for RoboFont 1 users) and the Glyph Builder, a glyph construction editor with live preview and an *Analyzer* function for debugging.

{% image how-tos/building-accented-glyphs_glyph-builder.png %}

> - {% internallink "how-tos/building-accented-glyphs-with-glyph-construction"  %}
{: .seealso }

[Glyph Construction]: http://github.com/typemytype/glyphconstruction


Building accented glyphs with code
----------------------------------

It is also possible to build accented glyphs with a script using the `glyphConstruction` module. This method does not provide a visual preview, but it’s very fast – so you can build accented glyphs for several fonts at once in batch.

If you choose this approach to build your accented glyphs, don’t forget to:

- backup your fonts before running the script!
- check the resulting glyphs visually, to make sure that they are correct

> - {% internallink "building-accented-glyphs-with-a-script" %}
{: .seealso }


Useful Tools
-----------------

The following tools can be helpful while building accented glyphs:

### Anchor Overlay Tool

{% image tutorials/AnchorOverlay.png %}

[Anchor Overlay Tool] by Jens Kutilek is a custom {% internallink 'topics/interactive-tools' text='interactive tool' %} to assist in workflows which use anchors to align components and base glyphs. It offers special functions for placing anchors, previewing accents and positioning components.

### Adjust Anchors

{% image tutorials/AdjustAnchors.png %}

[Adjust Anchors] by the Adobe Type Team is another tool built around an anchor-based workflow. It offers a window to interactively preview combinations of base glyphs and accents, and a live preview in the Glyph Editor.

### GlyphPalette

{% image tutorials/GlyphPalette.png %}

[GlyphPalette] by Rafał Buchner is a tool to visualize all accented versions of a glyph, and to quickly navigate between components and base glyphs.

### Generate Mark Files

The Adobe Type Team distributes two scripts to generate Mark Files for {% internallink "/topics/accented-glyphs#precomposed-vs-decomposed-characters" text="decomposed" %} accented glyphs:
- [generateSingleMarkFiles.py](https://github.com/adobe-type-tools/python-scripts/blob/master/generateSingleMarkFiles.py)
- [generateAllMarkFiles.py](https://github.com/adobe-type-tools/python-scripts/blob/master/generateAllMarkFiles.py)

Check the [ReadMe](https://github.com/adobe-type-tools/python-scripts) for instructions on how to use these tools.

[Anchor Overlay Tool]: http://github.com/jenskutilek/AnchorOverlayTool
[GlyphPalette]: http://github.com/rafalbuchner/glyph-palette
[Adjust Anchors]: http://github.com/adobe-type-tools/adjust-anchors-rf-ext
