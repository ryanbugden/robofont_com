---
layout: page
title: Creating designspace files
tags:
  - variable fonts
  - interpolation
  - designspace
---

* Table of Contents
{:toc}

*A `.designspace` file is an XML-based description of a multi-dimensional interpolation space.*

> - [The designSpace format](https://github.com/fonttools/fonttools/tree/main/Doc/source/designspaceLib) specification
{: .seealso }


Creating designspace files with DesignSpaceEditor
-------------------------------------------------

[DesignSpaceEditor] is a RoboFont extension which provides a simple interface to create and edit designspace data – without any interactive preview, map etc. DesignSpaceEditor is open-source and can be {% internallink 'extensions/installing-extensions-mechanic' text='installed with Mechanic 2' %}. If you want to navigate and visualize your design space you can use [Skateboard for RoboFont](https://superpolator.com/skateboard.html).

{% image how-tos/creating-designspace-files_DesignSpaceEditor.png %}

1. Design your designspace using DesignSpaceEditor’s interface.
2. Click on the *Save* button to save the data to a `.designspace` file.

> - [DesignSpaceEditor documentation][DesignSpaceEditor]
{: .seealso }


Creating designspace files with designSpaceLib
----------------------------------------------

Designspace files can also be created with code: {% glossary FontTools %} supports reading & writing `.designspace` files using the [designSpaceLib].

> DesignspaceLib started as [DesignSpaceDocument], a separate library, and was later incorporated into fontTools.
{: .note }

The script below will create a `.designspace` file for MutatorSans. To try it out, save it in the MutatorSans folder next to the UFOs.

{% showcode tutorials/makeDesignSpace.py %}

> - [designSpaceLib documentation][designSpaceLib]
{: .seealso }


[designSpaceLib]: http://github.com/fonttools/fonttools/blob/master/Doc/source/designspaceLib/scripting.rst
[designSpaceDocument]: http://github.com/LettError/designSpaceDocument
[DesignSpaceEditor]: http://github.com/LettError/designSpaceRoboFontExtension
[MutatorMath]: http://github.com/LettError/mutatorMath
[varLib]: http://github.com/fonttools/fonttools/tree/master/Lib/fontTools/varLib
