---
layout: page
title: Creating a simple font library browser
tags:
  - scripting
  - vanilla
---

* Table of Contents
{:toc}

*This tutorial shows how to create a simple tool to browse through font folders and open fonts.*


Introduction
------------

Imagine that you have several UFO projects sitting side-by-side in a parent folder:

> myLibrary
> ├── myFamily1
> │   ├── Bold.ufo
> │   ├── Condensed.ufo
> │   ├── Italic.ufo
> │   └── Regular.ufo
> ├── myFamily2
> │   └── ...
> └── myFamily3
>     └── ...
{: .asCode }

We’ll create a simple tool to browse through the folders, showing a list of all UFOs in each folder, and to open selected fonts.

{% image tutorials/FontLibraryBrowser.png %}

> The code can be extended to do more than just opening the fonts – for example, setting font infos, building accents, generating fonts etc.
{: .tip }

This example will illustrate the following concepts and patterns:

### Layout variables

The buttons and lists are placed and sized using layout variables (padding, button height, column widths, etc). This makes it easier to change dimensions during development.

### Get Folder dialog

1. When the user clicks on the *get folder…* button, a `getFolder` dialog is opened.

2. After the folder is selected, a list of subfolders is displayed in the left column.

### List selection

Items selection works differently in each list:

- The list of families allows only one item to be selected at once.
- The list of fonts allows selection of multiple items (default `List` behavior).

### Show fonts in folder

As the selection in the left column changes, the right column is updated to show a list of UFOs in the selected family folder.


The code
--------

Read the comments and docstrings for explanations about what the code does.

{% showcode tutorials/simpleFontLibraryBrowser.py %}

