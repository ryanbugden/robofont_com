---
layout: page
title: Creating a window with action buttons
tags:
  - scripting
  - vanilla
---

* Table of Contents
{:toc}

*This tutorial shows how to create a very simple tool with user interface using {% internallink 'topics/vanilla' %}.*


Introduction
------------

The interface will consist of a floating window and two action buttons:

{% image tutorials/windowWithActionButtons.png %}

Each button in the tool will trigger a separate action:

1. print the names of the selected glyphs
2. apply a mark color to the selected glyphs

This example will illustrate the following common concepts and patterns:

### Callbacks

A *callback* is a function that is called by another function which takes the first function as a parameter. Usually, a callback is a function that is called when an *event* happens.

In this example, the event will be the button clicks. Whenever the event is triggered, the dialog will call the function associated with it – the callback.

Callbacks are used extensively in `vanilla` and in user interfaces in general.

### Defensive programming

The tool wants to do something to the selected glyphs in the current font. But what if there is no font open, or no glyph selected? The program should handle these situations elegantly, avoiding errors and providing informative messages to the user.

Notice how the program will exit the function if these two conditions (a font open, at least one glyph selected) are not met. Whenever that happens, a window will be opened with a message to the user.

### Undo

If a tool makes changes to user data, it should also make it possible for the user to *revert* these changes if necessary.

In RoboFont, this is done by wrapping the action that alters the data in a `with` statement using the `.undo()` method on the object we are going to change, like

```python
with glyph.undo("fixing left margin"):
    glyph.leftMargin = 10
```
or

```python
with font.undo("updating font info"):
    font.info.copyright = "Copyright (c) 1532 Claude Garamond"
```
You can find another example of undo in the latest snippet of code of this article


Writing the code
----------------

We’ll write the code in 3 steps:

1. [Creating the interface](#1-creating-the-interface)
2. [Adding behavior to the interface](#2-adding-behavior-to-the-interface)
3. [Filling in the actions](#3-filling-in-the-actions)

### 1. Creating the interface

We’ll start by creating the interface.

We define an object to represent the tool. When this object is initialized, it opens a [floating window] with the two buttons.

{% showcode tutorials/simpleToolDemo_0.py %}

> The `FloatingWindow` is used to make the tool stay on top of the other windows. Vanilla includes other types of macOS windows, like `Window` and `HUDFloatingWindow`.
> 
> See Apple’s [Human Interface Guidelines] for info on when to use which window.
{: .note }

[floating window]: http://vanilla.robotools.dev/en/latest/objects/FloatingWindow.html
[Human Interface Guidelines]: http://developer.apple.com/design/human-interface-guidelines/macos/windows-and-views/window-anatomy/

### 2. Adding behavior to the interface

Now that the interface is in place, we can start adding behavior to it. This is done by adding callbacks to link each button click to a function.

The functions don’t do anything useful yet – we’ll develop them in the next section.

{% showcode tutorials/simpleToolDemo_1.py %}

### 3. Filling in the callback functions

With interface and callbacks in place, we can now add code to perform the actual actions (printing glyph names and applying mark colors).

Some things to notice in this version of the code:

Defensive programming
: The code in the callbacks use the ‘defensive approach’ described in the introduction: before applying the action, we make sure that there is a font open, and that at least one glyph is selected. If these conditions are not met, we show an informative message to the user and exit the function.

Using a message window to show text
: In the previous version, the `print` function was used to print some dummy text in the {% internallink 'workspace/output-window' %}. In this version we’re showing text to the user using a `Message` window.

Opening the dialog with OpenWindow
: This version makes use of `OpenWindow` to open the dialog – this prevents it from being opened twice. We’re now also using a conditional check to open the dialog only if the script is executed as the main script – this allows us to use the script as a module and import the dialog from another file.

Documentation strings
: ^
  Since this is the final version of the code, we’re adding [documentation strings] to the object and its methods. Docstrings are plain language descriptions about what a piece of code does. They are similar to comments; but unlike comments, they become attributes of the object they describe, and can be used in automated documentation systems.

  Docstrings are not required for your program to work – but if you are planning to share your code with other developers, it is good practice to include them.

[documentation strings]: http://www.python.org/dev/peps/pep-0257/

{% showcode tutorials/simpleToolDemo.py %}

> In RoboFont 1.8, use:
>
> - `glyph.mark` instead of `glyph.markColor`
> - `font.selection` instead of `font.selectedGlyphNames`
{: .note }
