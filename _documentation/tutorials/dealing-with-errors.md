---
layout: page
title: Dealing with errors in your code
tags:
  - getting help
  - bugs
---

* Table of Contents
{:toc}

RoboFont makes scripting more accessible to designers; designers who get into scripting quickly become familiar with software errors.

This page offers some tips to help you look for answers and solve errors by yourself.

If the error persists, or if you think you’ve found a bug: please {% internallink 'asking-for-help' text='ask for help' %} or {% internallink 'submitting-bug-reports' text='submit a bug report' %} on the forum.

## Finding errors

In order to fix an error, we must first find it in the source code. Every error message includes a *traceback* which displays a stack of all nested function calls during program execution. Thanks to the traceback we can see where the error originated, and where the failure actually occurred.

> - When running your own scripts, error messages appear at the bottom of the {% internallink 'workspace/scripting-window' %}.
> - Error messages related to extensions, to RoboFont itself, or to any external code libraries appear in the {% internallink 'workspace/output-window' %}.
{: .note }

> - {% internallink 'workspace/safe-mode' %}
{: .seealso }

### Error and Traceback example

Here’s a simple faulty script which raises an error:

```python
def aFunction():
    anotherFunction()

def anotherFunction():
    aThirdFunction()

aFunction()
```

```plaintext
Traceback (most recent call last):
  File "<untitled>", line 7, in <module>
  File "<untitled>", line 2, in aFunction
  File "<untitled>", line 5, in anotherFunction
NameError: name 'aThirdFunction' is not defined
```

The traceback tells the story of the error:

- `aFunction()` executes `aFunction`
- `aFunction` calls `anotherFunction`
- `anotherFunction` calls `aThirdFunction`
- `aThirdFunction` is not defined, so a `NameError` is raised

> - [Errors and Exceptions (Python Tutorial)](http://docs.python.org/3.7/tutorial/errors)
{: .seealso }

## Avoiding errors

Getting errors while writing your own scripts is quite common, and it’s part of the process. As your script evolves, you can add conditions and checks to avoid triggering predictable errors.

For example, let’s say you have a script that does something to the selected glyphs in the current font:

```python
f = CurrentFont()
for glyphName in f.selection:
    print(glyphName)
```

If you try to run this script without having a font open, you will get an error:

```plaintext
Traceback (most recent call last):
  File "<untitled>", line 2, in <module>
TypeError: object of type 'NoneType' has no len()
```

A more robust script could first check if a font is open, and if not, print out a message to the user while avoiding the error. We can also warn the user if the selection is empty:

```python
f = CurrentFont()

# make sure we have an open font
if f is not None:

    # make sure at least one glyph is selected
    if len(f.selection):

        # do something to selected glyphs
        for glyphName in f.selection:
            print(glyphName)

    # no glyph selected
    else:
        print('please select at least one glyph!')

# no font open
else:
    print('please open a font first!')
```

<!--
## Warnings

from mojo.UI import setDefault
setDefault("warningWithFullStack", True)
-->
