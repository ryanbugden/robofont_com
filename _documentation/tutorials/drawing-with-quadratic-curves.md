---
layout: page
title: Drawing with quadratic curves
tags:
  - TrueType
  - contours
---

* Table of Contents
{:toc}

## Implementation of quadratic beziers in RoboFont

Starting from version 4.0, RoboFont offers full support for quadratic bezier contours, with the possibility to draw an infinite (theoretically 😅) amount of control points for each segment.

The full support of quadratic curves provides finer control over outlines for TrueType fonts required by multidimensional interpolation schemes allowed by variable fonts technology.

{% image how-tos/drawing-with-quadratic-curves_cubic-quadratic.png %}

Other cubic-to-quadratic conversion algorithms are possible using code libraries or extensions.

> - {% internallink 'how-tos/converting-from-cubic-to-quadratic' %}
{: .seealso }

## Display colors for quadratic points

The color of quadratic points in the Glyph Editor can also be configured in the {% internallink 'workspace/preferences-window/glyph-view' %}. Look for the following settings in the section *Appearance*:

- Off-curve Stroke Color (Quadratic Beziers, TrueType)
- Handles Stroke Color (Quadratic Beziers, TrueType)

The same two settings are also available in the {% internallink 'workspace/preferences-editor' %}:

- `glyphViewQuadraticHandlesStrokeColor`
- `glyphViewOffCurveQuadPointsStroke`

{% image how-tos/drawing-with-quadratic-curves_s-cubic.png caption='cubic curves (PostScript)' %}

{% image how-tos/drawing-with-quadratic-curves_s-quadratic.png caption='quadratic curves (TrueType)' %}

> Choose distinctive colors for PostScript and TrueType points, to avoid confusion between the two.
{: .tip }
