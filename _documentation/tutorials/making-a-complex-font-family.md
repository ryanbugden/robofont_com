---
layout: page
title: Making a complex font family
tags:
  - mastering
---

## Setting fonts infos

This tutorial is an extension of {% internallink '/tutorials/making-a-simple-font-family' %}.
The following tables provide a detailed example on how to set font infos for a project diverging from the typographic quartett Regular/Italic/Bold/BoldItalic.

The example presents a project organized over a two-axes design space: width and weight. It also presents a monospaced variant (last table), technically sitting in a separate design space and with a smaller amount of weights.

The following tables only concern the generation of static instances, not variable fonts. In the case of variable fonts, the font info is specified in the `.designspace` document, not into the ufo files.

> - {% internallink "/topics/variable-fonts" %}
> - {% internallink "/tutorials/creating-variable-fonts#generating-variable-fonts" %}
{: .seealso }

Practically, the following method suggests to split the project into sub-families organized per width. So, the "Family Name" menu of the user application will show `MyFont Condensed`, `MyFont`, `MyFont Extended`, `MyFont Wide`, `MyFont Mono`. The "Style Name" menu for each of those will instead show the weight. This approach is desirable for two reasons:
+ it is quite conventional, so users will probably be used to this kind of organization
+ it avoids to group everything into a single family. In such case, every style would fit into a single dropdown "Style Name" menu, causing a very long list of styles (66!) to choose from


| familyName       | styleName      | styleMapStyle | usWeightClass | usWidthClass | italicAngle |
| -----------------|----------------|---------------|---------------|--------------|-------------|
| MyFont Condensed | Thin           | -             | 100           | Condensed    | -           |
| MyFont Condensed | Thin Cursive   | -             | 100           | Condensed    | 7           |
| MyFont Condensed | Light          | -             | 300           | Condensed    | -           |
| MyFont Condensed | Light Cursive  | -             | 300           | Condensed    | 7           |
| MyFont Condensed | Roman          | Regular       | 400           | Condensed    | -           |
| MyFont Condensed | Roman Cursive  | Italic        | 400           | Condensed    | 7           |
| MyFont Condensed | Book           | -             | 450           | Condensed    | -           |
| MyFont Condensed | Book Cursive   | -             | 450           | Condensed    | 7           |
| MyFont Condensed | Medium         | -             | 500           | Condensed    | -           |
| MyFont Condensed | Medium Cursive | -             | 500           | Condensed    | 7           |
| MyFont Condensed | Bold           | Bold          | 700           | Condensed    | -           |
| MyFont Condensed | Bold Cursive   | Bold Italic   | 700           | Condensed    | 7           |
| MyFont Condensed | Black          | -             | 900           | Condensed    | -           |
| MyFont Condensed | Black Cursive  | -             | 900           | Condensed    | 7           |

<br/>

| familyName | styleName      | styleMapStyle | usWeightClass | usWidthClass    | italicAngle |
| -----------|----------------|---------------|---------------|-----------------|-------------|
| MyFont     | Thin           | -             | 100           | Medium (normal) | -           |
| MyFont     | Thin Cursive   | -             | 100           | Medium (normal) | 7           |
| MyFont     | Light          | -             | 300           | Medium (normal) | -           |
| MyFont     | Light Cursive  | -             | 300           | Medium (normal) | 7           |
| MyFont     | Roman          | Regular       | 400           | Medium (normal) | -           |
| MyFont     | Roman Cursive  | Italic        | 400           | Medium (normal) | 7           |
| MyFont     | Book           | -             | 450           | Medium (normal) | -           |
| MyFont     | Book Cursive   | -             | 450           | Medium (normal) | 7           |
| MyFont     | Medium         | -             | 500           | Medium (normal) | -           |
| MyFont     | Medium Cursive | -             | 500           | Medium (normal) | 7           |
| MyFont     | Bold           | Bold          | 700           | Medium (normal) | -           |
| MyFont     | Bold Cursive   | Bold Italic   | 700           | Medium (normal) | 7           |
| MyFont     | Black          | -             | 900           | Medium (normal) | -           |
| MyFont     | Black Cursive  | -             | 900           | Medium (normal) | 7           |

<br/>

| familyName      | styleName      | styleMapStyle | usWeightClass | usWidthClass    | italicAngle |
| ----------------|----------------|---------------|---------------|-----------------|-------------|
| MyFont Extended | Thin           | -             | 100           | Expanded        | -           |
| MyFont Extended | Thin Cursive   | -             | 100           | Expanded        | 7           |
| MyFont Extended | Light          | -             | 300           | Expanded        | -           |
| MyFont Extended | Light Cursive  | -             | 300           | Expanded        | 7           |
| MyFont Extended | Roman          | Regular       | 400           | Expanded        | -           |
| MyFont Extended | Roman Cursive  | Italic        | 400           | Expanded        | 7           |
| MyFont Extended | Book           | -             | 450           | Expanded        | -           |
| MyFont Extended | Book Cursive   | -             | 450           | Expanded        | 7           |
| MyFont Extended | Medium         | -             | 500           | Expanded        | -           |
| MyFont Extended | Medium Cursive | -             | 500           | Expanded        | 7           |
| MyFont Extended | Bold           | Bold          | 700           | Expanded        | -           |
| MyFont Extended | Bold Cursive   | Bold Italic   | 700           | Expanded        | 7           |
| MyFont Extended | Black          | -             | 900           | Expanded        | -           |
| MyFont Extended | Black Cursive  | -             | 900           | Expanded        | 7           |

<br/>

| familyName  | styleName      | styleMapStyle | usWeightClass | usWidthClass    | italicAngle |
| ------------|----------------|---------------|---------------|-----------------|-------------|
| MyFont Wide | Thin           | -             | 100           | Extra-Expanded  | -           |
| MyFont Wide | Thin Cursive   | -             | 100           | Extra-Expanded  | 7           |
| MyFont Wide | Light          | -             | 300           | Extra-Expanded  | -           |
| MyFont Wide | Light Cursive  | -             | 300           | Extra-Expanded  | 7           |
| MyFont Wide | Roman          | Regular       | 400           | Extra-Expanded  | -           |
| MyFont Wide | Roman Cursive  | Italic        | 400           | Extra-Expanded  | 7           |
| MyFont Wide | Book           | -             | 450           | Extra-Expanded  | -           |
| MyFont Wide | Book Cursive   | -             | 450           | Extra-Expanded  | 7           |
| MyFont Wide | Medium         | -             | 500           | Extra-Expanded  | -           |
| MyFont Wide | Medium Cursive | -             | 500           | Extra-Expanded  | 7           |
| MyFont Wide | Bold           | Bold          | 700           | Extra-Expanded  | -           |
| MyFont Wide | Bold Cursive   | Bold Italic   | 700           | Extra-Expanded  | 7           |
| MyFont Wide | Black          | -             | 900           | Extra-Expanded  | -           |
| MyFont Wide | Black Cursive  | -             | 900           | Extra-Expanded  | 7           |

<br/>

| familyName  | styleName      | styleMapStyle | usWeightClass | usWidthClass    | italicAngle |
| ------------|----------------|---------------|---------------|-----------------|-------------|
| MyFont Mono | Light          | -             | 300           | Medium (normal) | -           |
| MyFont Mono | Light Cursive  | -             | 300           | Medium (normal) | 7           |
| MyFont Mono | Roman          | Regular       | 400           | Medium (normal) | -           |
| MyFont Mono | Roman Cursive  | Italic        | 400           | Medium (normal) | 7           |
| MyFont Mono | Book           | -             | 450           | Medium (normal) | -           |
| MyFont Mono | Book Cursive   | -             | 450           | Medium (normal) | 7           |
| MyFont Mono | Medium         | -             | 500           | Medium (normal) | -           |
| MyFont Mono | Medium Cursive | -             | 500           | Medium (normal) | 7           |
| MyFont Mono | Bold           | Bold          | 700           | Medium (normal) | -           |
| MyFont Mono | Bold Cursive   | Bold Italic   | 700           | Medium (normal) | 7           |


> - {% internallink "/tutorials/making-a-simple-font-family" %}
> - {% internallink "/topics/interpolation" %}
> - {% internallink "/topics/preparing-for-interpolation" %}
> - {% internallink "/tutorials/making-italic-fonts" %}
> - {% internallink "/tutorials/setting-font-names" %}
> - {% internallink "/tutorials/defining-character-sets" %}
> - {% internallink "/tutorials/generating-fonts" %}
{: .seealso}
