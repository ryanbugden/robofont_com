---
layout: page
title: Making a simple font family
tags:
  - mastering
---

## Setting fonts infos

To make sure that applications as the Adobe Suite or Microsoft Office perceive a group of fonts as part of the same family, it is necessary to set the {% internallink "/reference/workspace/font-overview/font-info-sheet" text="info" %} of each font in a specific way:

+ the [Family Name](https://unifiedfontobject.org/versions/ufo3/fontinfo.plist/#generic-identification-information) should be equal across the fonts
+ differently from what the name of the style attributes might suggest, the [Style Name](https://unifiedfontobject.org/versions/ufo3/fontinfo.plist/#generic-identification-information) does not have to match the value from the [Style Map Style](https://unifiedfontobject.org/versions/ufo3/fontinfo.plist/#generic-identification-information) radio group. So choose the [Style Name](https://unifiedfontobject.org/versions/ufo3/) you prefer
+ you can leave the [Style Map Family Name](https://unifiedfontobject.org/versions/ufo3/fontinfo.plist/#generic-identification-information) as a default value, RoboFont will fill that for you when generating the font
+ remember to set the `usWeightClass` from the [OpenType OS/2 table](https://unifiedfontobject.org/versions/ufo3/fontinfo.plist/#opentype-os2-table-fields)

Here is a synthetical table with a very traditional setup:

| familyName | styleName   | styleMapStyle | usWeight | italicAngle |
|------------|-------------|---------------|----------|-------------|
| MyFont     | Regular     | Regular       | 400      | -           |
| MyFont     | Bold        | Bold          | 700      | -           |
| MyFont     | Italic      | Italic        | 400      | 7           |
| MyFont     | Bold Italic | Bold Italic   | 700      | 7           |

As mentioned above in the article, you might decide to choose a different approach for the [Style Name](https://unifiedfontobject.org/versions/ufo3/fontinfo.plist/#generic-identification-information), like using `Roman` instead of `Regular` or `Cursive` instead of `Italic`. Then, you just need to match the `usWeight` value of the `Roman` style with the `Cursive` style using the Style Map Style radio group

| familyName | styleName    | styleMapStyle | usWeight | italicAngle |
|------------|--------------|---------------|----------|-------------|
| MyFont     | Roman        | Regular       | 400      | -           |
| MyFont     | Bold         | Bold          | 700      | -           |
| MyFont     | Cursive      | Italic        | 400      | 7           |
| MyFont     | Bold Cursive | Bold Italic   | 700      | 7           |


> - {% internallink '/tutorials/making-a-complex-font-family' %}
> - {% internallink '/topics/interpolation' %}
> - {% internallink '/topics/preparing-for-interpolation' %}
> - {% internallink "/tutorials/making-italic-fonts" %}
> - {% internallink "/tutorials/setting-font-names" %}
> - {% internallink "/tutorials/defining-character-sets" %}
> - {% internallink "/tutorials/generating-fonts" %}
{: .seealso}