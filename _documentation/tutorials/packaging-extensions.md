---
layout: page
title: Packaging extensions
tags:
  - extensions
  - recommendations
---

{::options toc_levels='2' /}

* Table of Contents
{:toc}

The RoboFont extension landscape is very diverse: different developers have different preferences and different levels of skill; functionality provided by extensions varies from simple scripts to complex RoboFont-based applications.

**There is not one single pattern for packaging RoboFont extensions.**

*This page gives an overview of possible packaging patterns found in existing extensions.*


## 1. The extension is the source

This is the simplest and most widely used scheme. There is no separation between the extension source and the built extension: the code is written as an extension according to the {% internallink 'reference/extensions/extension-file-spec' %}.

> http://github.com/user/myExtension
> ├── myExtension.roboFontExt
> │   ├── lib/
> │   ├── html/
> │   ├── info.plist
> │   └── license
> ├── README.md
> └── license.txt
{: .asCode }

### Workflow

1. Install the extension. The files will be copied into the plugins folder:

    ```text
    /Users/username/Library/Application Support/RoboFont/plugins
    ```

2. If you need to make changes to the code, you can do it directly in the installed extension – so you can test the changes right away.

3. Once you’ve finished making changes, you can copy the modified files back into the source folder, overwriting the older files.

4. Follow the usual process to update the extension: add and commit your changes, and push to the remote server.

### Advantages & disadvantages

<table>
<tr>
  <th width='50%'>advantages</th>
  <th width='50%'>disadvantages</th>
</tr>
<tr>
  <td>
    <ul>
      <li>no build process</li>
    </ul>
  </td>
  <td>
    <ul>
      <li>working inside the plugins folder</li>
      <li>updating files manually</li>
    </ul>
  </td>
</tr>
</table>

### Examples

- [Batch](http://github.com/typemytype/batchRoboFontExtension)
- [Outliner](http://github.com/typemytype/outlinerRoboFontExtension)
- [CornerTools](http://github.com/roboDocs/CornerTools)


## 2. Source and extension in the same repository

This scheme is common for extensions which are built with a script. The repository contains both the source files and the built extension.

**The source files are used for development, the extension for distribution.**

> http://github.com/user/myExtension
> ├── source/
> │   ├── code/
> │   └── documentation/
> ├── build/myExtension.roboFontExt
> │   ├── lib/
> │   ├── html/
> │   ├── info.plist
> │   └── license
> ├── README.md
> ├── build.py
> └── license.txt
{: .asCode }

### Workflow

1. The code is developed as unpackaged source code by calling scripts directly.

2. When you are done making changes, run a build script to update the extension.

3. The generated extension can be installed for testing.

4. Follow the usual process to update the extension: add and commit your changes, and push to the remote server.

### Advantages & disadvantages

<table>
<tr>
  <th width='50%'>advantages</th>
  <th width='50%'>disadvantages</th>
</tr>
<tr>
  <td>
    <ul>
      <li>source folder can be anywhere</li>
      <li>not necessary to work inside the plugins folder like <a href='#workflow'>Pattern 1</a></li>
    </ul>
  </td>
  <td>
    <ul>
      <li>duplication of data in the same repository (source / extension)</li>
    </ul>
  </td>
</tr>
</table>

### Examples

- [Boilerplate](http://github.com/roboDocs/rf-extension-boilerplate)
- [BlueZoneEditor](http://github.com/andyclymer/BlueZoneEditor-roboFontExt)
- [GlyphConstruction](http://github.com/typemytype/GlyphConstruction)

[Pattern 2]: #2-source-and-extension-in-the-same-repository


## 3. Source and extension in separate repositories

Same as [Pattern 2], but with source files and extension separated in two repositories.

> http://github.com/user/myExtensionSource
> ├── code/
> ├── documentation/
> ├── README.md
> ├── build.py
> └── license.txt
>
> http://github.com/user/myExtension
> ├── myExtension.roboFontExt
> │   ├── lib/
> │   ├── html/
> │   ├── info.plist
> │   └── license
> ├── README.md
> └── license.txt
{: .asCode }

### Workflow

Same as [Pattern 2](#workflow-1), but with separate commit histories for source and extension.

- The source repository is the primary one, and includes the complete commit history.

- The extension repository is secondary, its commit history shows only the ‘squashed’ changes between versions.

> This pattern is recommended for binary extensions – the source repository is private, and the extension repository contains only the binary files for distribution.
{: .tip }

### Advantages & disadvantages

<table>
<tr>
  <th width='50%'>advantages</th>
  <th width='50%'>disadvantages</th>
</tr>
<tr>
  <td>
    <ul>
      <li>no code duplication in the same repository, cleaner commit history</li>
      <li>source repository can be used by itself as a module</li>
    </ul>
  </td>
  <td>
    <ul>
      <li>two separate repositories to maintain and update</li>
    </ul>
  </td>
</tr>
</table>

### Examples

- hTools2: [source](http://github.com/gferreira/hTools2) / [extension](http://github.com/gferreira/hTools2_extension)

## 4. Multiple extensions in a single repository

This approach can be found in older extensions, and is now discouraged.

> http://github.com/user/myRepository
> ├── myExtension1.roboFontExt
> │   ├── lib/
> │   ├── html/
> │   ├── info.plist
> │   └── license
> ├── myExtension2.roboFontExt
> │   ├── lib/
> │   ├── html/
> │   ├── info.plist
> │   └── license
> ├── myExtension3.roboFontExt
> │   ├── lib/
> │   ├── html/
> │   ├── info.plist
> │   └── license
> ├── README.md
> └── license.txt
{: .asCode }

### Workflow

Same as [Pattern 1](#workflow).

<table>
<tr>
  <th width='50%'>advantages</th>
  <th width='50%'>disadvantages</th>
</tr>
<tr>
  <td>
    <ul>
      <li>centralized workflow, only one repository to update and maintain</li>
    </ul>
  </td>
  <td>
    <ul>
      <li>not modular, commit history shows changes to different projects</li>
    </ul>
  </td>
</tr>
</table>

### Examples

- [FontBureau/fbOpenTools](http://github.com/FontBureau/fbOpenTools)
- [loicsander/Robofont-scripts](http://github.com/loicsander/Robofont-scripts)


