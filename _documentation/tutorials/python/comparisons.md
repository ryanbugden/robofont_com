---
layout: page
title: Comparisons
---

* Table of Contents
{:toc}

When comparing two objects it is important to make a distinction between *value* and *identity*.

Python has comparison and identity operators to compare two objects and decide the relation between them.


Comparing values
----------------

Comparison operators allow us to compare two objects numerically. The result of a comparison expression is always a boolean value.

Here is an overview of all the comparisons we can make between two values:

Do the objects have the same value?

```python
>>> 12 == 20
```
```plaintext
False
```

```python
>>> 20 == 20.0
```
```plaintext
True
```

Do the objects have different values?

```python
>>> 12 != 20
```
```plaintext
True
```

```python
>>> 20 != 20.0
```
```plaintext
False
```

Is the first object bigger than the second?

```python
>>> 12 > 20
```
```plaintext
False
```

```python
>>> 20 > 20.0
```
```plaintext
False
```

Is the first object smaller than the second?

```python
>>> 12 < 20
```
```plaintext
True
```

```python
>>> 20 < 20.0
```
```plaintext
False
```

Is the first object bigger than or equal to the second?

```python
>>> 12 >= 20.0
```
```plaintext
False
```

```python
>>> 20 >= 20.0
```
```plaintext
True
```

Is the first object smaller than or equal to the second?

```python
>>> 12 <= 20
```
```plaintext
True
```

```python
>>> 20 <= 20.0
```
```plaintext
True
```

It is possible to check if a number sits in range of values by grouping two comparison expressions:

```python
>>> n = 22
>>> 10 < n < 100
```
```plaintext
True
```

```python
>>> n = 1
>>> 10 < n < 100
```
```plaintext
False
```


Comparing identity
------------------

Two objects can have the same *value* and still have different *identities* – they are not the same ‘thing’.

To compare identities we use the identity operator `is`:

```python
>>> 10 is 10.0
```
```plaintext
False
```

In this example, the two objects have the same value but different identities – the first one is an `int`, and the second is a `float`.

> - [Comparisons](http://docs.python.org/3.7/library/stdtypes.html#comparisons)
{: .seealso }


Testing ‘truthiness’
--------------------

Every object in Python can be converted into a boolean. The general rule is that `0` and any empty collections are converted to `False`, and anything else is converted to `True`.

Let’s have a look at examples with different data types:

```python
>>> bool('hello') # string
```
```plaintext
True
```

```python
>>> bool('') # empty string
```
```plaintext
False
```

```python
>>> bool(['a', 'b', 'c']) # list
```
```plaintext
>>> True
```

```python
>>> bool([]) # empty list
```
```plaintext
False
```

```python
>>> bool(1.1) # float
```
```plaintext
True
```

```python
>>> bool(0.0) # float zero
```
```plaintext
False
```

```python
>>> bool(None)
```
```plaintext
False
```

> - [Truth Value Testing](http://docs.python.org/3.7/library/stdtypes.html#truth-value-testing)
{: .seealso }
