---
layout: page
title: Introduction
---

* Table of Contents
{:toc}

Thousands of people from different fields have learned Python, and so can you. It’s just a language, and a way of thinking in terms of rules, systems and patterns. **There is no magic involved!** With a bit of time and effort you too can become fluent in Python.


Some facts about Python
-----------------------

- The Python programming language was developed by Guido van Rossum in the late 1980s at the Centrum Wiskunde & Informatica in The Netherlands.

- Python is a *high-level* programming language with emphasis on readability. ‘High level’ means that it is distant from the 0s and 1s from the computer, and closer to human language (English, in this case).

- Python is free/open-source software and is available for all major platforms.

- Because Python is a *dynamic* programming language (executed at runtime), it is also often used as a scripting language in various kinds of applications.

### Why is Python called Python?

The name ‘Python’ was inspired by the the British comedy group [Monty Python](http://en.wikipedia.org/wiki/Monty_python).

{% image tutorials/python/monty-python-flying-circus.jpg caption='Programming does not have to be serious and boring!' %}


Where to use Python
-------------------

Python can be used in many different environments. MacOS comes with Python out of the box, as do most Linux distributions; downloads for Windows and several other platforms are available from the [Python website](http://www.python.org/downloads/).

Some applications are written entirely in Python, while others support it as a scripting language – to automate things which would otherwise have to be done ‘by hand’ (clicking around in menus and icons).

> - {% internallink '/tutorials/python/uses' %}
{: .seealso }


Python and typeface design
--------------------------

Python plays an important role in type design and font production. Most font editors today support scripting with Python.

> - [Python in Typeface and Font Development: An Interview with Just van Rossum](http://talkpython.fm/episodes/show/47/python-in-typeface-and-font-development)
> - [FontParts > History](http://fontparts.readthedocs.io/en/latest/index.html#history)
{: .seealso }


Python 2 vs. Python 3
---------------------

Python 3 was a major update to the Python language. The language was cleaned up and some mistakes were fixed – but in a way that broke full compatibility with the 2.X branch. Some of the new features in Python 3 have been ported backwards to Python 2.6 and 2.7, and are available via the `__future__` module.

Python 3 adoption was slow at first. But with the retirement of Python 2.7 scheduled for 2020, Python 3 has finally become the default version of Python in a RoboFont-based production workflow.

- A py3 version of DrawBot was announced in [November 2017](http://twitter.com/drawbotapp/status/930181039871483905).
- RoboFont 3 (the first py3 version of RoboFont) was announced in [March 2018](#).
- Most {% internallink 'reference/embedded-libraries' text='core libraries' %} and [RoboFont extensions][Mechanic 2] have also been upgraded.

[Mechanic 2]: http://robofontmechanic.com/

> The latest version of macOS (10.14 Mojave) still comes with Python 2.7
 pre-installed as its default version. Python 3 can be installed separately.
{: .note }

> - [Python FAQ: Why should I use Python 3?](http://eev.ee/blog/2016/07/31/python-faq-why-should-i-use-python-3/)
{: .seealso }


Python’s Design Philosophy
--------------------------

[According to Guido][Python’s Design Philosophy], the following guidelines helped to make decisions while designing and implementing Python:

- Borrow ideas from elsewhere whenever it makes sense.
- “Things should be as simple as possible, but no simpler.” (Einstein)
- Do one thing well (The “UNIX philosophy”).
- Don’t fret too much about performance – plan to optimize later when needed.
- Don’t fight the environment and go with the flow.
- Don’t try for perfection because “good enough” is often just that.
- (Hence) it’s okay to cut corners sometimes, especially if you can do it right later.

These values are carried over to much of the code written in Python, including RoboFont.

[Python’s Design Philosophy]:http://python-history.blogspot.com/2009/01/pythons-design-philosophy.html

### The Zen of Python

The philosophy behind the design of the Python language is summarized in a series of aphorisms called *The Zen of Python*. This text is available as an ‘easter egg’ in all Python distributions. Simply type the following line of code in your favorite Python console:

```python
import this
```

...and the complete Zen of Python will be printed out:

```plaintext
Beautiful is better than ugly.
Explicit is better than implicit.
Simple is better than complex.
Complex is better than complicated.
Flat is better than nested.
Sparse is better than dense.
Readability counts.
...
```

Try it out for yourself to read the rest!
A couple more easter eggs bundled with the language interpreter:

> ```python
> import antigravity
> ```
> ```python
> from __future__ import braces
> ```
{: .seealso }
