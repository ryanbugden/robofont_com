---
layout: page
title: Doing things to fonts
tags:
  - scripting
  - FontParts
---

* Table of Contents
{:toc}

A collection of simple scripts to do things to fonts.


Set fixed width in all glyphs
-----------------------------

Set a fixed width in all glyphs in the current font.

{% showcode tutorials/setFixedWidth.py %}

Set a fixed width in all selected glyphs in the current font. Enable undo/redo.

{% showcode tutorials/setFixedWidthUndo.py %}


Set infos in all open fonts
---------------------------

Set font infos from a dictionary into all open fonts.

{% showcode tutorials/setInfosDict.py %}


Copy font infos from one font to another
----------------------------------------

Copy font info data from one open font to another.

{% showcode tutorials/copyFontInfo.py %}


Batch generate fonts for all UFOs in folder
-------------------------------------------

Batch generate OTFs from a folder of UFOs.

1. open a dialog to select a folder
2. open each UFO in folder (without the UI)
3. generate OpenType-CFF font from UFO

{% showcode tutorials/batchGenerateFonts.py %}


Import a font into a layer of the current font
----------------------------------------------

Import glyphs from a second font into a layer of the current font.

{% showcode tutorials/importFontIntoLayer.py %}


Building accented glyphs
------------------------

Build accented glyphs in RoboFont3 using Glyph Construction.

{% showcode tutorials/buildAccentedGlyphsRF3.py %}

Build accented glyphs in RoboFont1 using RoboFab’s `font.compileGlyph`.

{% showcode tutorials/buildAccentedGlyphsRF1.py %}


Scale a font
------------

Scale different types of data in a font:

- glyph contours and metrics
- anchors
- font guides and glyph guides
- kerning
- font dimensions

{% showcode tutorials/scaleFont.py %}
