---
layout: page
title: Doing things to glyphs
tags:
  - scripting
  - FontParts
---

* Table of Contents
{:toc}

A selection of scripts to do various things to glyphs.


Move points by a random number of units
---------------------------------------

Move every point in the current glyph by a random number of units.

{% showcode tutorials/movePointsRandom.py %}

> In RoboFont 1.8, use `glyph.update()` instead of `glyph.changed()`.
{: .note }


Add anchor to selected glyphs
-----------------------------

Add anchors with a given name and vertical position in the selected glyphs.

The horizontal position is calculated as the center of the glyph’s bounding box.

{% showcode tutorials/addAnchors.py %}

> In RoboFont 1.8, use `glyph.box()` instead of `glyph.bounds()`.
{: .note }


Drawing inside a glyph with a pen
---------------------------------

Draw inside a glyph using a pen.

{% showcode tutorials/drawWithPen.py %}

> - {% internallink 'tutorials/using-pens' %}
> - {% internallink 'topics/pens' %}
{: .seealso }


Rasterizing a glyph
-------------------

Rasterize a glyph with a given grid size.

*converted from [`robofab.objects.objectsBase.BaseGlyph`](https://github.com/robotools/robofab/blob/master/Lib/robofab/objects/objectsBase.py#L1652)*

{% showcode tutorials/rasterizeGlyph.py %}


Get glif path for glyph
-----------------------

In [UFO3] fonts, each glyph is stored in a single [glif] file inside a layer folder.

The script below returns the `.glif` path for the current glyph.

{% showcode tutorials/getGlifPath.py %}

```plaintext
/myFolder/myFont.ufo/glyphs/one.glif
```

[glif]: http://unifiedfontobject.org/versions/ufo3/glyphs/glif/
[UFO3]: http://unifiedfontobject.org/versions/ufo3/


> Relevant threads from the RoboFont forum
> - [copy between layers](http://forum.robofont.com/topic/524/copy-glyph-from-default-layer-to-another)
> - [get selected objects](http://forum.robofont.com/topic/529/find-out-what-part-of-a-glyph-is-selected/8)
{: .seealso}
