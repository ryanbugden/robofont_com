---
layout: page
title: Interpolation scripts
tags:
  - interpolation
  - FontParts
---

* Table of Contents
{:toc}

> - {% internallink "topics/interpolation" %}
{: .seealso }


Interpolating colors
--------------------

This example shows how to interpolate two RGB colors by interpolating each R, G, B color value separately.

{% image tutorials/interpolate-colors.png %}

{% showcode tutorials/interpolateColors.py %}


Interpolating position and size
-------------------------------

The next example shows the same approach used to interpolate position and size.

{% image tutorials/interpolate-pos-size.png %}

{% showcode tutorials/interpolatePosSize.py %}


Checking interpolation compatibility
------------------------------------

Before interpolating two glyphs, we need to make sure that they are compatible. We can do that with code using a glyph’s `isCompatible` method. This function returns two values:

- The first is a boolean indicating if the two glyphs are compatible.
- If the first value is `False`, the second will contain a report of the problems.

```python
f = CurrentFont()
g = f['O']
print(g.isCompatible(f['o']))
```

```plaintext
(True, '')
```

```python
print(g.isCompatible(f['n']))
```

```plaintext
(False, '[Fatal] Contour 0 contains a different number of segments.\n[Fatal] Contour 1 contains a different number of segments.\n[Warning] The glyphs do not contain components with exactly the same base glyphs.')
```

> - {% internallink "topics/preparing-for-interpolation" %}
{: .seealso }


Interpolating glyphs in the same font
-------------------------------------

The script below shows how to generate a number of interpolation and extrapolation steps between two selected glyphs in the current font.

{% image tutorials/interpolate-in-font.png %}

{% showcode tutorials/interpolateGlyphs.py %}


Interpolating between two sources
---------------------------------

This example shows how to interpolate glyphs from two source fonts into a third font.

The script expects 3 fonts open:

1. the current font, where the glyphs will be interpolated
2. one source font named “Regular”
4. another source font named “Bold”

{% showcode tutorials/interpolateGlyphsIntoCurrentFont.py %}


Interpolating fonts
-------------------

Generate a new font by interpolating two source fonts.

{% showcode tutorials/interpolateFonts.py %}


Batch interpolating fonts
-------------------------

The following example generates a set of instances by interpolating two source fonts.

The names and interpolation values of each instance are defined in a list of tuples.

{% showcode tutorials/batchInterpolateFonts.py %}

```plaintext
generating instances...

   generating Light (0.25)...
   saving instances at /myFolder/MyFamily_Light.ufo...

   generating Regular (0.5)...
   saving instances at /myFolder/MyFamily_Regular.ufo...

   generating Bold (0.75)...
   saving instances at /myFolder/MyFamily_Bold.ufo...

...done.
```


Condensomatic
-------------

A handy script to generate a *Condensed* from a *Regular* and a *Bold*.

- calculates a scaling factor from stem widths of *Regular* and *Bold*
- uses interpolation and horizontal scaling to create condensed glyphs

{% image tutorials/condensomatic.png %}

{% showcode tutorials/condensomatic.py %}
