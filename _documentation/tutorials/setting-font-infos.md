---
layout: page
title: Setting font infos
tags:
  - mastering
  - OpenType
---

* Table of Contents
{:toc}


OpenType fonts
--------------

Fonts are digital files and must be produced according to technical standards. All bits of data in a font are relevant and should be set properly.

An OpenType font consists of several tables, and each table contains a particular kind of information. The official reference for what goes where in an OpenType font is the [OpenType specification].

[OpenType specification]: http://www.microsoft.com/en-us/Typography/OpenTypeSpecification.aspx


Setting font infos in RoboFont
------------------------------

In RoboFont, font info data can be set manually with the {% internallink "font-info-sheet" %}, or programatically {% internallink "tutorials/scripts-font#set-infos-in-all-open-fonts" text="using a script" %}.

However you choose to set your infos, **it is not necessary to fill in all fields.** RoboFont can compute missing data automatically if some basic attributes are set. Just keep the *Use default value* option checked if you don't have specific needs.

> When generating OpenType fonts, RoboFont uses the [ufo2fdk] library to pass the UFO to the {% glossary AFDKO %}. The actual computation of fallback values is handled by [ufo2fdk/fontInfoData.py].
{: .note }

[ufo2fdk]: http://github.com/typemytype/ufo2fdk/blob/master/Lib/ufo2fdk/
[ufo2fdk/fontInfoData.py]: http://github.com/typemytype/ufo2fdk/blob/master/Lib/ufo2fdk/fontInfoData.py


Required attributes
-------------------

The following attributes are the minimum necessary for generating a working OpenType font:

- Family Name
- Style Name
- Units Per Em
- Descender
- x-height
- Cap-height
- Ascender
{: .required }

Make sure to set these values in the early stages of a project, so you can generate valid test fonts.


Recommended attributes
----------------------

The following attributes provide important information about a font, so it is highly recommended to set them:

- Style Map Family Name
- Version Major
- Version Minor
- Copyright
- Trademark
- License
- License URL
- Designer
- Designer URL
- Manufacturer
- Manufacturer URL
- Sample Text
- usWidthClass
- usWeightClass
- achVendID
- fsType
- ulUnicodeRange
- ulCodePageRange
- sTypoLineGap
{: .recommended }

Make sure to set these values before you distribute your fonts.


Other attributes
----------------

If left empty, all other font info attributes are calculated automatically.

You can, of course, override automatic values and set any attribute manually. However, do that only if you know what you’re doing.

> - {% internallink "setting-font-names" %}
> - {% internallink "postscript-hinting" %}
> - {% internallink "adding-localized-name-table" %}
{: .seealso }
