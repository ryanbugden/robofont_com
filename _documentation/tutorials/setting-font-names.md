---
layout: page
title: Setting font family names
tags:
  - OpenType
  - mastering
---

* Table of Contents
{:toc}

Setting font names for small families with up to 4 conventional styles (Regular, Italic, Bold, Bold Italic) is fairly easy and uncomplicated. Setting font names for larger families, however, requires a bit more work.

## Two naming systems

There are essentially two naming systems an application can use to display font choices to users. Each naming system relies on particular font info fields.

The following examples show how the same family with 4 weights (Light, Regular, Bold, Black) and Roman/Italic styles appears under each system.

### 1. All styles in one family

In this scheme, all styles appear under a single family name in the application’s font menu:

> MyFamily
> ├── Light
> ├── Light Italic
> ├── Regular
> ├── Italic
> ├── Bold
> ├── Bold Italic
> ├── Black
> └── Black Italic
{: .asCode }

This is how most modern applications work, including graphic and typographic design tools.

> The order of styles in the menu is defined by the `OS/2` weight values in the fonts. [See below](#os2-weight-values).
{: .note }

### 2. Style-linked sub-families

Some applications cannot support more than 4 members in each family, and these members must be called Regular, Italic, Bold or Bold Italic. Bold and Italic buttons are usually provided for activating Bold and Italic styles separately.

To work under this system, large families must be broken down into smaller 4-member families:

> MyFamily
> ├── Regular
> ├── Italic
> ├── Bold
> └── Bold Italic
>
> MyFamily Light
> ├── Regular
> └── Italic
>
> MyFamily Black
> ├── Regular
> └── Italic
{: .asCode }

This font naming system is typically used by Office applications, and older versions of Windows.

## Font naming

Fonts can support both font naming schemes: just set the appropriate values for Family Name and Style Name (naming scheme 1), and for Style Map Family and Name Style Map Style (naming scheme 2).

<table>
  <thead>
    <tr>
      <th>font name</th>
      <th width="16%">Family Name</th>
      <th width="16%">Style Name</th>
      <th width="22%">Style Map Family Name</th>
      <th width="16%">Style Map Style</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>MyFamily Regular</td>
      <td>MyFamily</td>
      <td>Regular</td>
      <td>MyFamily</td>
      <td>Regular</td>
    </tr>
    <tr>
      <td>MyFamily Italic</td>
      <td>MyFamily</td>
      <td>Italic</td>
      <td>MyFamily</td>
      <td>Italic</td>
    </tr>
    <tr>
      <td>MyFamily Bold</td>
      <td>MyFamily</td>
      <td>Bold</td>
      <td>MyFamily</td>
      <td>Bold</td>
    </tr>
    <tr>
      <td>MyFamily Bold Italic</td>
      <td>MyFamily</td>
      <td>Bold Italic</td>
      <td>MyFamily</td>
      <td>Bold Italic</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>MyFamily Light</td>
      <td>MyFamily</td>
      <td>Light</td>
      <td>MyFamily Light</td>
      <td>Regular</td>
    </tr>
    <tr>
      <td>MyFamily Light Italic</td>
      <td>MyFamily</td>
      <td>Light</td>
      <td>MyFamily Light</td>
      <td>Italic</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>MyFamily Black</td>
      <td>MyFamily</td>
      <td>Black</td>
      <td>MyFamily Black</td>
      <td>Regular</td>
    </tr>
    <tr>
      <td>MyFamily Black Italic</td>
      <td>MyFamily</td>
      <td>Black</td>
      <td>MyFamily Black</td>
      <td>Italic</td>
    </tr>
  </tbody>
</table>

These settings will produce the groupings shown in the examples above.

## OS/2 weight values

Naming system 1 requires setting appropriate OS/2 weight values in order to have fonts sorted by weight. If these values are not set, the styles will be sorted alphabetically, which is not what users expect.

While any number between `1` to `1000` is valid, the following is recommended:

- use `400` for Regular, and `700` for Bold
- values below `200` should be avoided, as some apps may apply automatic bold
- use multiples of `50` for intermediate weights

This is how the values should be set for our example family:

<table>
  <thead>
    <tr>
      <th>font name</th>
      <th width="50%">OS/2 weight</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>MyFamily Light</td>
      <td>300</td>
    </tr>
    <tr>
      <td>MyFamily Light Italic</td>
      <td>300</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>MyFamily Regular</td>
      <td>400</td>
    </tr>
    <tr>
      <td>MyFamily Italic</td>
      <td>400</td>
    </tr>
    <tr>
      <td>MyFamily Bold</td>
      <td>600</td>
    </tr>
    <tr>
      <td>MyFamily Bold Italic</td>
      <td>600</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>MyFamily Black</td>
      <td>700</td>
    </tr>
    <tr>
      <td>MyFamily Black Italic</td>
      <td>700</td>
    </tr>
  </tbody>
</table>
