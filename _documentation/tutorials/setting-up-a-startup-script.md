---
layout: page
title: Setting up start-up scripts
tags:
  - scripting
draft: false
---

* Table of Contents
{:toc}


A start-up script is a script which is executed when RoboFont is initialized.

Start-up scripts are typically used to add custom observers, menu items or toolbar buttons to RoboFont. They can be included in extensions, or added manually in the Preferences.

*This tutorial explains how to add a start-up script manually using the {% internallink 'workspace/preferences-window/extensions' %}.*


An example script
-----------------

As an example, we’ll use the script below to make your computer read a welcome message every time RoboFont is opened.

{% showcode tutorials/welcomeToRoboFont.py %}

Save this script somewhere in your computer.


Editing the Start-Up Scripts Preferences
----------------------------------------

Now go to the {% internallink 'workspace/preferences-window/extensions' %}, select the *Start Up Scripts* tab, and use the **+** button to add the script to the list (or drag the script file from Finder into the list).

{% image reference/workspace/preferences-window/preferences_extensions-startup-scripts.png %}

Apply the changes, and restart RoboFont.

You should now hear a welcome message every time RoboFont is launched!
