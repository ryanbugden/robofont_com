---
layout: page
title: TrueType hinting
tags:
  - hinting
  - TrueType
---

RoboFont offers a direct way to inject TrueType hinting instructions in
a binary font while generating a TrueType font. The lib keys were initiated by Petr van Blokland for the [RoboHint](https://vimeo.com/38350058) tools he was developing. Black Foundry developped [TTH](https://vimeo.com/436015188) on top of these lib keys.

From early 2021 the UFO specification has a similar way to support TrueType hinting data, 
RoboFont does not yet support this yet. Read more of the public lib keys for [font.lib](https://unifiedfontobject.org/versions/ufo3/lib.plist/#publictruetypeinstructions) and public lib keys for [glyph.lib](https://unifiedfontobject.org/versions/ufo3/glyphs/glif/#publictruetypeinstructions)


# Glyph Lib keys

## glyf table

**com.robofont.robohint.assembly**
: list of assembly code

# Font Lib keys

## maxp

**com.robofont.robohint.maxp**
: base name

**com.robofont.robohint.maxp.maxZones**
: int

**com.robofont.robohint.maxp.maxTwilightPoints**
: int

**com.robofont.robohint.maxp.maxStorage**
: int

**com.robofont.robohint.maxp.maxFunctionDefs**
: int

**com.robofont.robohint.maxp.maxInstructionDefs**
: int

**com.robofont.robohint.maxp.maxStackElements**
: int

**com.robofont.robohint.maxp.maxSizeOfInstructions**
: int

**com.robofont.robohint.maxp.maxComponentElements**
: int

## cvt

**com.robofont.robohint.cvt** 
: list
: needs to be a continuous list of values with `0` at the missing slots.
: notice the extra space at the end of the lib key: `"com.robofont.robohint.cvt "`

## fpgm

**com.robofont.robohint.fpgm**
: list of assembly code

## prep

**com.robofont.robohint.prep**
: list of assembly code

## gasp

**com.robofont.robohint.gasp**
: dict of int ranges fe `{8: 2, 16: 1, 65535: 3}`

## hdmx

**com.robofont.robohint.hdmx**
:   dict 
:   <pre>
    {ppem : {glyphname: width}}
    </pre>


## VDMX

**com.robofont.robohint.VDMX**
:   dict
:   <pre>
    {
      version : 1, # recommended
      ratRanges : [
                      {bCharSet : int, xRatio : int, yStartRatio : int, yEndRatio: int, groupIndex : int}
                  ],
      groups : [
              {yPel : yMax, yMin}
              ]
    }
    </pre>
  

## LTSH

**com.robofont.robohint.LTSH**
:   dict 
:   <pre>
    {glyphname : yPel}
    </pre>

> - {% internallink "tutorials/postscript-hinting" %}
> - {% internallink "how-tos/hinting-ttfautohint" %}
{: .seealso}

