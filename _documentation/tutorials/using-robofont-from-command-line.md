---
layout: page
title: Using RoboFont from the command line
tags:
  - scripting
---

RoboFont can be used from the command line. You just need to install the command line tool in the {% internallink "/reference/workspace/preferences-window/extensions#shell" text="Preferences Window" %}.

{% image tutorials/roboFont-terminal.png %}

The command line tool has a simple API:

```text
roboFont [-h] [-p] [-c] [-o]
```

<table>
    <tr>
        <td><code>--help -h</code></td>
        <td>show options</td>
    </tr>
    <tr>
        <td><code>--pythonpath -p &lt;path&gt; &lt;path&gt; ...</code></td>
        <td><code>.py</code> files path(s) to run inside RoboFont</td>
    </tr>
    <tr>
        <td><code>--pythoncode -c "print(5*5")</code></td>
        <td>Python code to run inside RoboFont</td>
    </tr>
    <tr>
        <td><code>--openfile -o &lt;path&gt;</code></td>
        <td>files to open with RoboFont</td>
    </tr>
</table>

> [RoboFont shell command in action (Vimeo)](http://vimeo.com/42008861)
{: .seealso }
