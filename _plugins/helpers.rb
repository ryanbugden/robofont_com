
module HelpersTools

  TrailingIndex = /\/$/

  def page_hash(context)
    reg = context.registers
    site = reg[:site]

    if reg[:page_url_hash].nil?
      all = []
      site.collections.each do |key, value|
        all += value.docs
      end
      all += site.pages
      reg[:page_url_hash] = Hash[ (all).collect {
        |x| [x.url.sub(TrailingIndex, ''), x]}]
    end
    return reg[:page_url_hash]
  end

  def get_value(context, expression)
    if expression.nil?
      return
    end
    if (expression[0]=='"' and expression[-1]=='"') or (expression[0]=="'" and expression[-1]=="'")
      # it is a literal
      return expression[1..-2]
    else
      # it is a variable
      lookup_path = expression.split('.')
      result = context
      lookup_path.each do |variable|
        result = result[variable] if result
      end
      return result
    end
  end

  AllWarnings = []

  def colorize(text, color_code)
    unless AllWarnings.include? text
      AllWarnings.push(text)
      warn "\e[#{color_code}m#{text}\e[0m"
    end
  end

  def redWarning(text); colorize(text, 31); end
  def pinkWarning(text); colorize(text, 95); end

end
