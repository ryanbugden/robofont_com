

module Jekyll
  class TreePagesOrder < Generator

    TrailingIndex = /\/$/

    priority :lowest

    def generate(site)
      page_hash(site)
      getOrder(site.pages.sort_by {|i| i["menuOrder"] || 999999999} )
      site.config['tree_order'] = @orderedPages
      if not @orderedPages.nil?
        inject_nav(@orderedPages)
      end
    end

    def page_hash(site)
      all = []
      site.collections.each do |key, value|
        all += value.docs
      end
      all += site.pages
      @page_url_hash = Hash[ (all).collect {
        |x| [x.url.sub(TrailingIndex, ''), x]}]
    end

    def checkTree(page)
      unless page.url.start_with? "/documentation"
        return
      end
      if page.data.has_key?("draft-hidden") and page["draft-hidden"]
        return
      end
      unless @orderedPages.include? page
        @orderedPages.push(page)
      end
      if page.data.has_key?("tree")
        page["tree"].each do |subPage|
          if subPage.end_with? "*"
            subPageRoot = subPage.sub("*", "")
            subPageRoot = File.join(page.url.gsub(TrailingIndex, ''), subPageRoot)
            subPageRoot = File.absolute_path(subPageRoot)
            @page_url_hash.sort.map.each do |key, value|
              if key.include? subPageRoot and key != subPageRoot and key != subPageRoot+"/"
                checkTree(value)
              end
            end
          end
          subPage = File.join(page.url.gsub(TrailingIndex, ''), subPage)
          subPage = File.absolute_path(subPage)
          subPage = @page_url_hash[subPage]
          unless subPage.nil?
            checkTree(subPage)
          end
        end
      end
    end

    def getOrder(pages)
      @orderedPages = []
      pages.each do |page|
        checkTree(page)
      end
    end

    # Inject next / prev navigation in page.data
    def inject_nav(pages)
      len = pages.length
      if len > 1
        pages[0].data['tree_next'] = pages[1]
        pages[len-1].data['tree_previous'] = pages[len-2]
        pages.each_cons(3) { |pages|
          pages[1].data['tree_previous'] = pages[0]
          pages[1].data['tree_next'] = pages[2]
        }
      end
    end

  end
end