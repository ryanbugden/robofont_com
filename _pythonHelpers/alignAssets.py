#!/usr/bin/env python3

"""
Aligns assets (images and python scripts to the same markdown file content structure)

"""

# -- Modules -- #
import re
import os
from pathlib import Path
import shutil

# -- Constants -- #
DOCS_FOLDER = Path('../_documentation/')
IMAGES_FOLDER = Path('../images')
SCRIPTS_FOLDER = Path('../assets/code')

SHOWCODE_PATTERN = re.compile(r'{% showcode ([A-Za-z0-9\/-]+.py) %}')
IMAGE_PATTERN = re.compile(r'{% image ([a-zA-Z-\/0-9.]+) .*?%}')

PATTERN_2_FOLDER = {
    SHOWCODE_PATTERN: SCRIPTS_FOLDER,
    IMAGE_PATTERN: IMAGES_FOLDER
}


# -- Objects, Functions, Procedures -- #
def alignAssetsToMarkdowns():

    usedAssets = {}
    for docPath in DOCS_FOLDER.glob('**/*.md'):

        # open each doc
        with open(docPath, mode='r', encoding='utf-8') as mdFile:
            txt = mdFile.read()

        for pattern, folder in PATTERN_2_FOLDER.items():

            # check if showcode or image filter
            for eachMatch in re.finditer(pattern, txt):

                # building a posix path with the match
                assetPath = folder / eachMatch.group(1).lstrip('/')

                # correct structure, next match
                docTree = docPath.parts[len(DOCS_FOLDER.parts):-1]
                assetTree = assetPath.parts[len(folder.parts):-1]
                if docTree == assetTree:
                    print(f'{assetPath} skipping!')
                    continue

                # structure ain't correct, let's fix it
                if assetPath not in usedAssets:

                    # if encountered for the first time: move it to a different place
                    midDocParts = docPath.parts[len(DOCS_FOLDER.parts):-1]
                    newAssetPath = folder / f"{'/'.join(midDocParts)}/{assetPath.name}"

                    # target destination folder could not exist
                    if not newAssetPath.parent.exists():
                        os.makedirs(newAssetPath.parent)

                    # move file to destination
                    if editingFiles:
                        shutil.move(assetPath, newAssetPath)

                    # add newAssetPath to dictionary, so we won't try to move it twice or more
                    usedAssets[assetPath] = newAssetPath

                else:
                    # otherwise: use the already existing path
                    newAssetPath = usedAssets[assetPath]

                # inject the path into the doc file
                if folder == IMAGES_FOLDER:
                    newAssetPathToInject = '/'.join(newAssetPath.parts[2:])
                else:
                    newAssetPathToInject = '/'.join(newAssetPath.parts[3:])

                txt = txt.replace(eachMatch.group(1), newAssetPathToInject)

        # open the doc again and inject the content
        if editingFiles:
            with open(docPath, mode='w', encoding='utf-8') as mdFile:
                mdFile.write(txt)


# -- Variables -- #
editingFiles = True

# -- Instructions -- #
if __name__ == '__main__':
    alignAssetsToMarkdowns()
