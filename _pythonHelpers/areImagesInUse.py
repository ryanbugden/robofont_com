#!/usr/bin/env python3

# ----------------------------------- #
# Checking if images are used in docs #
# ----------------------------------- #

# -- Modules -- #
from pathlib import Path

# -- Objects, Functions, Procedures -- #
def grabImagesPaths(folder):
    return [pp for pp in folder.glob('**/*')
            if not pp.is_dir() and not pp.name.startswith('.')]

def loadMarkdowns(folder):
    txts = []
    for eachPath in folder.glob('**/*.md'):
        with open(eachPath, mode='r', encoding='utf-8') as mdFile:
            txts.append(mdFile.read())
    return '\n'.join(txts)


# -- Variables -- #
imagesFolder = Path('../images')
markdownsFolder = Path('../..')

# -- Instructions -- #
if __name__ == '__main__':
    imagesPaths = grabImagesPaths(imagesFolder)
    txts = loadMarkdowns(markdownsFolder)

    for eachImagePath in imagesPaths:
        if eachImagePath.name not in txts:
            print(f'DELETE: {eachImagePath.name}')
            eachImagePath.unlink()
