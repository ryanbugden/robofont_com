#!/usr/bin/env python3

# ------------------------------------------- #
# BALANCED HANDLES "checking contour quality" #
# ------------------------------------------- #

# --- Modules --- #
import drawBot as dB

# --- Constants --- #
MODULE = 90
PADDING = 40

# --- Functions & Classes --- #
def lineAttributes(thickness=1):
    dB.lineDash(None)
    dB.stroke(0)
    dB.strokeWidth(thickness)
    dB.fill(None)

def shapeAttributes():
    dB.fill(0)
    dB.stroke(None)

def typeAttributes():
    shapeAttributes()
    dB.font("RoboType-Mono", 18)

def drawBezier(bcpInRatio, bcpOutRatio):
    pt1    = 0, 0
    bcpOut = 0, MODULE * bcpOutRatio
    bcpIn  = MODULE - MODULE * bcpInRatio, MODULE
    pt2    = MODULE, MODULE

    offCurveRad = 3
    onCurveSide = 6

    # the curve
    bz = dB.BezierPath()
    bz.moveTo(pt1)
    bz.curveTo(bcpOut, bcpIn, pt2)
    bz.endPath()
    lineAttributes(thickness=2)
    dB.drawPath(bz)

    # the handles
    lineAttributes()
    dB.line(pt1, bcpOut)
    dB.line(bcpIn, pt2)

    # the handle paths
    lineAttributes()
    dB.lineDash(2)
    dB.line(pt1, (0, MODULE))
    dB.line((0, MODULE), pt2)

    # offcurve points
    shapeAttributes()
    dB.oval(bcpOut[0]-offCurveRad, bcpOut[1]-offCurveRad,
            offCurveRad*2, offCurveRad*2)
    dB.oval(bcpIn[0]-offCurveRad, bcpIn[1]-offCurveRad,
            offCurveRad*2, offCurveRad*2)

    # oncurve points
    shapeAttributes()
    dB.rect(pt1[0]-onCurveSide/2, pt1[1]-onCurveSide/2, onCurveSide, onCurveSide)
    dB.rect(pt2[0]-onCurveSide/2, pt2[1]-onCurveSide/2, onCurveSide, onCurveSide)

    # captions
    typeAttributes()
    dB.text(f"{bcpInRatio:.0%}", (pt2[0]-10, pt2[1]-22))
    dB.text(f"{bcpOutRatio:.0%}", (pt1[0]+10, pt1[1]))


if __name__ == '__main__':
    # --- Variables --- #
    outputPath = "../images/tutorials/balanced-handles-options.png"
    bcpsRange = [0.3, 0.5, 0.7, 0.9]

    # --- Instructions --- #
    dB.newDrawing()
    dB.newPage(800*2, 600*2)
    dB.scale(2)
    dB.translate(MODULE, MODULE*.75)

    for bcpInRatio in bcpsRange:
        with dB.savedState():
            for bcpOutRatio in bcpsRange:
                drawBezier(bcpInRatio, bcpOutRatio)
                dB.translate(MODULE+PADDING*2, 0)
        dB.translate(0, MODULE+PADDING)
    dB.saveImage(outputPath)
    dB.endDrawing()
