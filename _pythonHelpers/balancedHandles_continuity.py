#!/usr/bin/env python3

# -------------------------------------------------------- #
# BALANCED HANDLES (continuity) "checking contour quality" #
# -------------------------------------------------------- #

# --- Modules --- #
import drawBot as dB
from balancedHandles import lineAttributes, shapeAttributes, typeAttributes

# --- Constants --- #
MODULE = 80
PADDING = 25

# --- Functions & Classes --- #
def drawTwoBeziers(inRatio, outRatio, fixedRatio, wdt, hgt):
    pt1 = 0, 0
    out1 = 0, hgt * fixedRatio

    pt2 = wdt/2, hgt
    in2 = pt2[0] - wdt/2 * inRatio, pt2[1]
    out2 = pt2[0] + wdt/2 * outRatio, pt2[1]

    pt3 = wdt, 0
    in3 = wdt, hgt * fixedRatio

    offCurveRad = 3
    onCurveSide = 6

    bz = dB.BezierPath()
    bz.moveTo(pt1)
    bz.curveTo(out1, in2, pt2)
    bz.curveTo(out2, in3, pt3)
    bz.endPath()

    lineAttributes(thickness=2)
    dB.drawPath(bz)

    # the handles
    lineAttributes()
    dB.line(pt1, out1)
    dB.line(in2, pt2)
    dB.line(out2, pt2)
    dB.line(in3, pt3)

    # offcurve points
    shapeAttributes()
    dB.oval(out1[0]-offCurveRad, out1[1]-offCurveRad,
            offCurveRad*2, offCurveRad*2)
    dB.oval(in2[0]-offCurveRad, in2[1]-offCurveRad,
            offCurveRad*2, offCurveRad*2)
    dB.oval(out2[0]-offCurveRad, out2[1]-offCurveRad,
            offCurveRad*2, offCurveRad*2)
    dB.oval(in3[0]-offCurveRad, in3[1]-offCurveRad,
            offCurveRad*2, offCurveRad*2)

    # oncurve points
    shapeAttributes()
    dB.rect(pt1[0]-onCurveSide/2, pt1[1]-onCurveSide/2, onCurveSide, onCurveSide)
    dB.rect(pt2[0]-onCurveSide/2, pt2[1]-onCurveSide/2, onCurveSide, onCurveSide)
    dB.rect(pt3[0]-onCurveSide/2, pt3[1]-onCurveSide/2, onCurveSide, onCurveSide)

    # captions
    typeAttributes()
    dB.text(f"{bcpInRatio:.0%}", (in2[0], in2[1]+8), align='center')
    dB.text(f"{bcpOutRatio:.0%}", (out2[0], out2[1]+8), align='center')


if __name__ == '__main__':
    # --- Variables --- #
    outputPath = "../images/tutorials/balanced-handles-continuity.png"
    bcpsRange = [0.3, 0.5, 0.7, 0.9]

    bezWdt = MODULE*1.5
    bezHgt = MODULE*0.8

    # --- Instructions --- #
    dB.newDrawing()
    dB.newPage(800*2, 600*2)
    dB.scale(2)
    dB.translate(MODULE*.5, MODULE*.5)

    for bcpInRatio in bcpsRange:
        with dB.savedState():
            for bcpOutRatio in bcpsRange:
                drawTwoBeziers(bcpInRatio, bcpOutRatio, 0.6, bezWdt, bezHgt)
                dB.translate(bezWdt+PADDING*2, 0)
        dB.translate(0, bezHgt+PADDING*3)
    dB.saveImage(outputPath)
    dB.endDrawing()
