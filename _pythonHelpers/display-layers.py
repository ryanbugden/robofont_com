imgs = [
    '../images/videos/display-layers/display-layers-00.png',
    '../images/videos/display-layers/display-layers-01.png',
    '../images/videos/display-layers/display-layers-02.png',
    '../images/videos/display-layers/display-layers-03.png',
    '../images/videos/display-layers/display-layers-04.png',
    '../images/videos/display-layers/display-layers-05.png',
    '../images/videos/display-layers/display-layers-06.png',
    '../images/videos/display-layers/display-layers-07.png',
    '../images/videos/display-layers/display-layers-08.png',
    '../images/videos/display-layers/display-layers-09.png',
    '../images/videos/display-layers/display-layers-10.png',
    '../images/videos/display-layers/display-layers-11.png'
]

gifPath = '../images/videos/display-layers/display-layers.gif'

for img in imgs:
    w, h = imageSize(img)
    newPage(w, h)
    fill(1)
    rect(0, 0, w, h)
    frameDuration(1)
    image(img, (0, 0))

saveImage(gifPath)