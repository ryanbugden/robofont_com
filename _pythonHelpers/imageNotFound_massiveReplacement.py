#!/usr/bin/env python3

# ------------------------------ #
# Let's finish this alignment... #
# ------------------------------ #

# -- Modules -- #
from alignAssets import IMAGES_FOLDER
from pathlib import Path
import os
import shutil

# -- Constants -- #

# -- Objects, Functions, Procedures -- #

# -- Variables -- #
imageNotFound_2_mdPath = [
    ('images/workspace/preferences_character-set_delete-glyph.png', '_documentation/how-tos/adding-and-removing-glyphs.md'),
    ('images/extensions/installing-extensions_install.png', '_documentation/how-tos/extensions/installing-extensions-manually.md'),
    ('images/extensions/installing-extensions_update.png', '_documentation/how-tos/extensions/installing-extensions-manually.md'),
    ('images/extensions/installing-extensions_uninstall.png', '_documentation/how-tos/extensions/installing-extensions-manually.md'),
    ('images/extensions/installing-extensions-mechanic_extensions-list.png', '_documentation/how-tos/extensions/installing-extensions-mechanic.md'),
    ('images/extensions/installing-extensions-mechanic_update-available.png', '_documentation/how-tos/extensions/installing-extensions-mechanic.md'),
    ('images/extensions/installing-extensions-mechanic_install-update.png', '_documentation/how-tos/extensions/installing-extensions-mechanic.md'),
    ('images/extensions/managing-extension-streams_extension-streams.png', '_documentation/how-tos/extensions/managing-extension-streams.md'),
    ('images/extensions/managing-extension-streams_single-items.png', '_documentation/how-tos/extensions/managing-extension-streams.md'),
    ('images/extensions/managing-extension-streams_check-for-updates.png', '_documentation/how-tos/extensions/managing-extension-streams.md'),
    ('images/building-tools/extensions/publishing-extensions_data-folder.png', '_documentation/how-tos/extensions/publishing-extensions.md'),
    ('images/extensions/using-extensions_extensions-menu.png', '_documentation/how-tos/extensions/using-extensions.md'),
    ('images/extensions/using-extensions_custom-tools.png', '_documentation/how-tos/extensions/using-extensions.md'),
    ('images/extensions/using-extensions_existing-menus.png', '_documentation/how-tos/extensions/using-extensions.md'),
    ('images/extensions/using-extensions_application-menu.png', '_documentation/how-tos/extensions/using-extensions.md'),
    ('images/extensions/using-extensions_show-info.png', '_documentation/how-tos/extensions/using-extensions.md'),
    ('images/building-tools/observers/stencilPreview_0.png', '_documentation/how-tos/observers/stencil-preview.md'),
    ('images/building-tools/observers/stencilPreview_1.png', '_documentation/how-tos/observers/stencil-preview.md'),
    ('images/building-tools/vanilla/bulkList_0.png', '_documentation/how-tos/vanilla/bulk-list.md'),
    ('images/building-tools/vanilla/bulkList_1.png', '_documentation/how-tos/vanilla/bulk-list.md'),
    ('images/workspace/application-menu_robofont.png', '_documentation/reference/workspace/application-menu.md'),
    ('images/workspace/application-menu_file.png', '_documentation/reference/workspace/application-menu.md'),
    ('images/workspace/application-menu_edit.png', '_documentation/reference/workspace/application-menu.md'),
    ('images/workspace/application-menu_font.png', '_documentation/reference/workspace/application-menu.md'),
    ('images/workspace/application-menu_glyph.png', '_documentation/reference/workspace/application-menu.md'),
    ('images/workspace/application-menu_python.png', '_documentation/reference/workspace/application-menu.md'),
    ('images/workspace/application-menu_window.png', '_documentation/reference/workspace/application-menu.md'),
    ('images/workspace/revert-to-saved_update-found.png', '_documentation/reference/workspace/external-changes-window.md'),
    ('images/workspace/revert-to-saved_diff-view.png', '_documentation/reference/workspace/external-changes-window.md'),
    ('images/workspace/font-overview_toolbar.png', '_documentation/reference/workspace/font-overview.md'),
    ('images/workspace/font-overview_display-modes.png', '_documentation/reference/workspace/font-overview.md'),
    ('images/workspace/font-overview_glyph-cells.png', '_documentation/reference/workspace/font-overview.md'),
    ('images/workspace/font-overview_list-mode.png', '_documentation/reference/workspace/font-overview.md'),
    ('images/workspace/font-overview_list-mode-columns.png', '_documentation/reference/workspace/font-overview.md'),
    ('images/workspace/font-overview_template-glyphs.png', '_documentation/reference/workspace/font-overview.md'),
    ('images/workspace/font-overview_load-all-glyphs.png', '_documentation/reference/workspace/font-overview.md'),
    ('images/workspace/features-editor_include.png', '_documentation/reference/workspace/font-overview/features-editor.md'),
    ('images/workspace/features-editor_stand-alone.png', '_documentation/reference/workspace/font-overview/features-editor.md'),
    ('images/workspace/features-editor_export.png', '_documentation/reference/workspace/font-overview/features-editor.md'),
    ('images/workspace/font-info_general_identification.png', '_documentation/reference/workspace/font-overview/font-info-sheet/general.md'),
    ('images/workspace/font-info_general_dimensions.png', '_documentation/reference/workspace/font-overview/font-info-sheet/general.md'),
    ('images/workspace/font-info_general_legal.png', '_documentation/reference/workspace/font-overview/font-info-sheet/general.md'),
    ('images/workspace/font-info_general_parties.png', '_documentation/reference/workspace/font-overview/font-info-sheet/general.md'),
    ('images/workspace/font-info_general_note.png', '_documentation/reference/workspace/font-overview/font-info-sheet/general.md'),
    ('images/workspace/font-info_misc_fond.png', '_documentation/reference/workspace/font-overview/font-info-sheet/misc.md'),
    ('images/workspace/font-info_opentype_gasp.png', '_documentation/reference/workspace/font-overview/font-info-sheet/opentype.md'),
    ('images/workspace/font-info_opentype_head.png', '_documentation/reference/workspace/font-overview/font-info-sheet/opentype.md'),
    ('images/workspace/font-info_opentype_name.png', '_documentation/reference/workspace/font-overview/font-info-sheet/opentype.md'),
    ('images/workspace/font-info_opentype_hhea.png', '_documentation/reference/workspace/font-overview/font-info-sheet/opentype.md'),
    ('images/workspace/font-info_opentype_vhea.png', '_documentation/reference/workspace/font-overview/font-info-sheet/opentype.md'),
    ('images/workspace/font-info_opentype_os2.png', '_documentation/reference/workspace/font-overview/font-info-sheet/opentype.md'),
    ('images/workspace/font-info_postscript_identification.png', '_documentation/reference/workspace/font-overview/font-info-sheet/postscript.md'),
    ('images/workspace/font-info_postscript_hinting.png', '_documentation/reference/workspace/font-overview/font-info-sheet/postscript.md'),
    ('images/workspace/font-info_postscript_dimensions.png', '_documentation/reference/workspace/font-overview/font-info-sheet/postscript.md'),
    ('images/workspace/font-info_postscript_characters.png', '_documentation/reference/workspace/font-overview/font-info-sheet/postscript.md'),
    ('images/workspace/font-info_robofont.png', '_documentation/reference/workspace/font-overview/font-info-sheet/robofont.md'),
    ('images/workspace/font-info_woff_identification.png', '_documentation/reference/workspace/font-overview/font-info-sheet/woff.md'),
    ('images/workspace/font-info_woff_vendor.png', '_documentation/reference/workspace/font-overview/font-info-sheet/woff.md'),
    ('images/workspace/font-info_woff_credits.png', '_documentation/reference/workspace/font-overview/font-info-sheet/woff.md'),
    ('images/workspace/font-info_woff_description.png', '_documentation/reference/workspace/font-overview/font-info-sheet/woff.md'),
    ('images/workspace/font-info_woff_legal.png', '_documentation/reference/workspace/font-overview/font-info-sheet/woff.md'),
    ('images/workspace/groups-editor_add-group.png', '_documentation/reference/workspace/font-overview/groups-editor.md'),
    ('images/workspace/groups-editor_show-all-glyphs.png', '_documentation/reference/workspace/font-overview/groups-editor.md'),
    ('images/workspace/groups-editor_stacked.png', '_documentation/reference/workspace/font-overview/groups-editor.md'),
    ('images/workspace/kern-center_groups.png', '_documentation/reference/workspace/font-overview/kern-center.md'),
    ('images/workspace/search-glyphs-panel_glyph-attributes.png', '_documentation/reference/workspace/font-overview/search-glyphs-panel.md'),
    ('images/workspace/smart-sets_options.png', '_documentation/reference/workspace/font-overview/smart-sets-panel.md'),
    ('images/workspace/smart-sets_create-set-from-names.png', '_documentation/reference/workspace/font-overview/smart-sets-panel.md'),
    ('images/workspace/smart-sets_counters.png', '_documentation/reference/workspace/font-overview/smart-sets-panel.md'),
    ('images/workspace/smart-sets_create-missing-glyphs.png', '_documentation/reference/workspace/font-overview/smart-sets-panel.md'),
    ('images/workspace/sort-glyphs-sheet_character-set.png', '_documentation/reference/workspace/font-overview/sort-glyphs-sheet.md'),
    ('images/workspace/sort-glyphs-sheet_custom.png', '_documentation/reference/workspace/font-overview/sort-glyphs-sheet.md'),
    ('images/workspace/inspector_glyph.png', '_documentation/reference/workspace/glyph-editor.md'),
    ('images/workspace/glyph-editor_display-options.png', '_documentation/reference/workspace/glyph-editor.md'),
    ('images/workspace/glyph-editor_zoom-menu.png', '_documentation/reference/workspace/glyph-editor.md'),
    ('images/videos/jump_in_preview_mode.mp4', '_documentation/reference/workspace/glyph-editor.md'),
    ('images/workspace/jump_to_glyph.png', '_documentation/reference/workspace/glyph-editor.md'),
    ('images/workspace/glyph-editor_transform.png', '_documentation/reference/workspace/glyph-editor.md'),
    ('images/workspace/glyph-editor_editing-tool_contextual-menu.png', '_documentation/reference/workspace/glyph-editor.md'),
    ('images/workspace/application-menu_glyph.png', '_documentation/reference/workspace/glyph-editor.md'),
    ('images/workspace/glyph-editor_anchors.png', '_documentation/reference/workspace/glyph-editor/anchors.md'),
    ('images/workspace/glyph-editor_add-anchor.png', '_documentation/reference/workspace/glyph-editor/anchors.md'),
    ('images/workspace/inspector_anchors.png', '_documentation/reference/workspace/glyph-editor/anchors.md'),
    ('images/workspace/glyph-editor_contextual-menu-anchor.png', '_documentation/reference/workspace/glyph-editor/anchors.md'),
    ('images/workspace/components_add.png', '_documentation/reference/workspace/glyph-editor/components.md'),
    ('images/workspace/inspector_components.png', '_documentation/reference/workspace/glyph-editor/components.md'),
    ('images/workspace/glyph-editor_contextual-menu_component.png', '_documentation/reference/workspace/glyph-editor/components.md'),
    ('images/workspace/glyph-editor_components-transform.png', '_documentation/reference/workspace/glyph-editor/components.md'),
    ('images/workspace/glyph-editor_components-decompose-after.png', '_documentation/reference/workspace/glyph-editor/components.md'),
    ('images/workspace/glyph-editor_components-decompose-before.png', '_documentation/reference/workspace/glyph-editor/components.md'),
    ('images/workspace/inspector_points.png', '_documentation/reference/workspace/glyph-editor/contours.md'),
    ('images/workspace/glyph-editor_contextual-menu.png', '_documentation/reference/workspace/glyph-editor/contours.md'),
    ('images/workspace/glyph-editor_contextual-menu-points.png', '_documentation/reference/workspace/glyph-editor/contours.md'),
    ('images/workspace/glyph-editor_contextual-menu-point.png', '_documentation/reference/workspace/glyph-editor/contours.md'),
    ('images/workspace/glyph-editor_display-options-buttons.png', '_documentation/reference/workspace/glyph-editor/display-options.md'),
    ('images/workspace/glyph-editor_scale-options.png', '_documentation/reference/workspace/glyph-editor/display-options.md'),
    ('images/workspace/glyph-editor_display-options.png', '_documentation/reference/workspace/glyph-editor/display-options.md'),
    ('images/workspace/add_guideline.png', '_documentation/reference/workspace/glyph-editor/guidelines.md'),
    ('images/videos/remove_guideline.mp4', '_documentation/reference/workspace/glyph-editor/guidelines.md'),
    ('images/workspace/guidelines_menu.png', '_documentation/reference/workspace/glyph-editor/guidelines.md'),
    ('images/workspace/inspector_guidelines.png', '_documentation/reference/workspace/glyph-editor/guidelines.md'),
    ('images/workspace/image_editing.png', '_documentation/reference/workspace/glyph-editor/images.md'),
    ('images/workspace/image_menu.png', '_documentation/reference/workspace/glyph-editor/images.md'),
    ('images/workspace/image_scaling.png', '_documentation/reference/workspace/glyph-editor/images.md'),
    ('images/workspace/glyph-editor_layers-menu.png', '_documentation/reference/workspace/glyph-editor/layers.md'),
    ('images/workspace/glyph-editor_jump-to-layer.png', '_documentation/reference/workspace/glyph-editor/layers.md'),
    ('images/workspace/glyph-editor_add-layer-sheet.png', '_documentation/reference/workspace/glyph-editor/layers.md'),
    ('images/workspace/inspector_layers.png', '_documentation/reference/workspace/glyph-editor/layers.md'),
    ('images/workspace/glyph-editor_measurement-line.png', '_documentation/reference/workspace/glyph-editor/tools/measurement-tool.md'),
    ('images/workspace/glyph-editor_transform.png', '_documentation/reference/workspace/glyph-editor/transform.md'),
    ('images/workspace/inspector_transform.png', '_documentation/reference/workspace/glyph-editor/transform.md'),
    ('images/workspace/inspector_collapsed.png', '_documentation/reference/workspace/inspector.md'),
    ('images/workspace/inspector_glyph.png', '_documentation/reference/workspace/inspector.md'),
    ('images/workspace/font-overview_rename-glyph-sheet.png', '_documentation/reference/workspace/inspector.md'),
    ('images/workspace/font-overview_change-unicode-sheet.png', '_documentation/reference/workspace/inspector.md'),
    ('images/workspace/inspector_preview.png', '_documentation/reference/workspace/inspector.md'),
    ('images/workspace/inspector_layers.png', '_documentation/reference/workspace/inspector.md'),
    ('images/workspace/inspector_layers-contextual-menu.png', '_documentation/reference/workspace/inspector.md'),
    ('images/workspace/inspector_transform.png', '_documentation/reference/workspace/inspector.md'),
    ('images/workspace/inspector_transform_apply.png', '_documentation/reference/workspace/inspector.md'),
    ('images/workspace/inspector_transform_options.png', '_documentation/reference/workspace/inspector.md'),
    ('images/workspace/inspector_points.png', '_documentation/reference/workspace/inspector.md'),
    ('images/workspace/inspector_points-contextual-menu.png', '_documentation/reference/workspace/inspector.md'),
    ('images/workspace/inspector_components.png', '_documentation/reference/workspace/inspector.md'),
    ('images/workspace/inspector_anchors.png', '_documentation/reference/workspace/inspector.md'),
    ('images/workspace/inspector_guidelines.png', '_documentation/reference/workspace/inspector.md'),
    ('images/workspace/inspector_note.png', '_documentation/reference/workspace/inspector.md'),
    ('images/workspace/license_icon.png', '_documentation/reference/workspace/license-window.md'),
    ('images/workspace/license-window_empty.png', '_documentation/reference/workspace/license-window.md'),
    ('images/workspace/preferences_apply.png', '_documentation/reference/workspace/preferences-window.md'),
    ('images/workspace/preferences_restart.png', '_documentation/reference/workspace/preferences-window.md'),
    ('images/workspace/preferences_reset.png', '_documentation/reference/workspace/preferences-window.md'),
    ('images/workspace/preferences_extensions.png', '_documentation/reference/workspace/preferences-window/extensions.md'),
    ('images/workspace/preferences_extensions-startup-scripts.png', '_documentation/reference/workspace/preferences-window/extensions.md'),
    ('images/workspace/preferences_extensions-shell.png', '_documentation/reference/workspace/preferences-window/extensions.md'),
    ('images/workspace/preferences_font-overview_character-set.png', '_documentation/reference/workspace/preferences-window/font-overview.md'),
    ('images/workspace/preferences_font-overview_appearance.png', '_documentation/reference/workspace/preferences-window/font-overview.md'),
    ('images/workspace/preferences_glyph-view-display.png', '_documentation/reference/workspace/preferences-window/glyph-view.md'),
    ('images/workspace/preferences_glyph-view-appearance.png', '_documentation/reference/workspace/preferences-window/glyph-view.md'),
    ('images/workspace/preferences_glyph-view-hotkeys.png', '_documentation/reference/workspace/preferences-window/glyph-view.md'),
    ('images/workspace/preferences_misc.png', '_documentation/reference/workspace/preferences-window/miscellaneous.md'),
    ('images/workspace/preferences_python.png', '_documentation/reference/workspace/preferences-window/python.md'),
    ('images/workspace/preferences_short-keys.png', '_documentation/reference/workspace/preferences-window/short-keys.md'),
    ('images/workspace/preferences_short-keys-edit.png', '_documentation/reference/workspace/preferences-window/short-keys.md'),
    ('images/workspace/preferences_space-center_appearance.png', '_documentation/reference/workspace/preferences-window/space-center.md'),
    ('images/workspace/preferences_space-center_input-text.png', '_documentation/reference/workspace/preferences-window/space-center.md'),
    ('images/workspace/preferences_space-center_hotkeys.png', '_documentation/reference/workspace/preferences-window/space-center.md'),
    ('images/workspace/safe-mode_application-menu.png', '_documentation/reference/workspace/safe-mode.md'),
    ('images/workspace/scripting-window_options.png', '_documentation/reference/workspace/scripting-window.md'),
    ('images/workspace/scripting-window_script-browser.png', '_documentation/reference/workspace/scripting-window.md'),
    ('images/workspace/space-center_default-strings.png', '_documentation/reference/workspace/space-center.md'),
    ('images/workspace/space-center_pre-after.png', '_documentation/reference/workspace/space-center.md'),
    ('images/workspace/space-center_options.png', '_documentation/reference/workspace/space-center.md'),
    ('images/workspace/space-center_needs-update.png', '_documentation/reference/workspace/space-center.md'),
    ('images/workspace/window-modes_single.png', '_documentation/reference/workspace/window-modes.md'),
    ('images/workspace/window-modes_multi.png', '_documentation/reference/workspace/window-modes.md'),
    ('images/building-tools/merz/merz_diagram.png', '_documentation/topics/merz.md'),
    ('images/building-tools/interpolation/compatibility-scheme_2_01.gif', '_documentation/topics/preparing-for-interpolation.md'),
    ('images/building-tools/interpolation/compatibility-scheme_2_02.gif', '_documentation/topics/preparing-for-interpolation.md'),
    ('images/building-tools/interpolation/compatibility-scheme_2_03.gif', '_documentation/topics/preparing-for-interpolation.md'),
    ('images/building-tools/interpolation/compatibility-scheme_2_04.gif', '_documentation/topics/preparing-for-interpolation.md'),
    ('images/building-tools/interpolation/compatibility-scheme_2_05.gif', '_documentation/topics/preparing-for-interpolation.md'),
    ('images/building-tools/interpolation/compatibility-scheme_2_06.gif', '_documentation/topics/preparing-for-interpolation.md'),
    ('images/building-tools/interpolation/compatibility-scheme_2_07.gif', '_documentation/topics/preparing-for-interpolation.md'),
    ('images/building-tools/interpolation/compatibility-scheme_2_08.gif', '_documentation/topics/preparing-for-interpolation.md'),
    ('images/workspace/search-glyphs-panel_glyph-attributes.png', '_documentation/topics/smartsets.md'),
    ('images/building-tools/observers/observerPattern_1.png', '_documentation/topics/the-observer-pattern.md'),
    ('images/building-tools/observers/observerPattern_2.png', '_documentation/topics/the-observer-pattern.md'),
    ('images/building-tools/observers/observerPattern_3.png', '_documentation/topics/the-observer-pattern.md'),
    ('images/building-tools/observers/observerPattern_4.png', '_documentation/topics/the-observer-pattern.md'),
    ('images/building-tools/observers/observerPattern_5.png', '_documentation/topics/the-observer-pattern.md'),
    ('images/building-tools/observers/observerPattern_6.png', '_documentation/topics/the-observer-pattern.md'),
    ('images/building-tools/observers/observerPattern_7.png', '_documentation/topics/the-observer-pattern.md'),
    ('images/building-tools/contours_segments.jpg', '_documentation/topics/understanding-contours.md'),
    ('images/building-tools/contours_segments_points.jpg', '_documentation/topics/understanding-contours.md'),
    ('images/building-tools/contours_points.jpg', '_documentation/topics/understanding-contours.md'),
    ('images/building-tools/contours_bpoints.jpg', '_documentation/topics/understanding-contours.md'),
    ('images/workspace/glyph-editor_tools.png', '_documentation/topics/workspace-overview.md'),
    ('images/workspace/inspector_glyph.png', '_documentation/topics/workspace-overview.md'),
    ('images/workspace/inspector_layers.png', '_documentation/topics/workspace-overview.md'),
    ('images/workspace/inspector_transform.png', '_documentation/topics/workspace-overview.md'),
    ('images/workspace/window-modes_multi.png', '_documentation/topics/workspace-overview.md'),
    ('images/workspace/window-modes_single.png', '_documentation/topics/workspace-overview.md'),
    ('images/workspace/preferences_extensions-startup-scripts.png', '_documentation/tutorials/setting-up-a-startup-script.md'),
    ('images/building-tools/interpolation/glyphmath_examples_01.gif', '_documentation/tutorials/using-glyphmath.md'),
    ('images/building-tools/interpolation/glyphmath_examples_02.gif', '_documentation/tutorials/using-glyphmath.md'),
    ('images/building-tools/interpolation/glyphmath_examples_03.gif', '_documentation/tutorials/using-glyphmath.md'),
    ('images/building-tools/interpolation/glyphmath_examples_04.gif', '_documentation/tutorials/using-glyphmath.md'),
    ('images/building-tools/interpolation/glyphmath_examples_05.gif', '_documentation/tutorials/using-glyphmath.md'),
    ('images/workspace/font-overview_load-all-glyphs.png', '_documentation/tutorials/working-with-large-fonts.md')
]


def moveImagesAndReplacePaths():

    fixedAssets = {}

    for imageNotFound, mdPath in imageNotFound_2_mdPath:

        imPath = '..' / Path(imageNotFound)
        mdPath = '..' / Path(mdPath)

        if imPath not in fixedAssets:
            midTreeSection = mdPath.parts[2:-1]
            newImPath = IMAGES_FOLDER / '/'.join(midTreeSection) / imPath.name
            fixedAssets[imPath] = newImPath

            if not newImPath.parent.exists():
                os.makedirs(newImPath.parent)
            shutil.move(imPath, newImPath)

        else:
            newImPath = fixedAssets[imPath]

        with open(mdPath, mode='r', encoding='utf-8') as mdFile:
            txt = mdFile.read()

        pathInMD = '/'.join(imPath.parts[2:])
        txt = txt.replace(pathInMD, '/'.join(newImPath.parts[2:]))
        with open(mdPath, mode='w', encoding='utf-8') as mdFile:
            mdFile.write(txt)


# -- Instructions -- #
if __name__ == '__main__':
    moveImagesAndReplacePaths()
