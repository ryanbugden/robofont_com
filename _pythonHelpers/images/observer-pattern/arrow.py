from math import sqrt, atan, degrees, cos, sin
from drawBot import *

def getVector(p1, p2):
    x1, y1 = p1
    x2, y2 = p2
    w = x2 - x1
    h = y2 - y1
    distance = sqrt(w ** 2 + h ** 2)
    if w != 0:
        angleRadians = atan(float(h) / w)
        angleDegrees = degrees(angleRadians)
    else:
        angleDegrees = 0
    return distance, angleDegrees

def vector(pos, distance, angle):
    x, y = pos
    x += cos(radians(angle)) * distance
    y += sin(radians(angle)) * distance
    return x, y

class Arrow:

    tipLength = 20
    tipAngle = 20
    tipStart = False
    tipEnd = True
    color = 1, 0, 0
    strokeWidth = 10
    lineDash = None # 10, 10
    lineCap = ['butt', 'square', 'round'][2]
    lineJoin = ['bevel', 'miter', 'round'][2]

    def draw(self, p1, p2):
        distance, angle = getVector(p1, p2)
        save()
        fill(*self.color)
        stroke(*self.color)
        strokeWidth(self.strokeWidth)
        lineCap(self.lineCap)
        lineJoin(self.lineJoin)
        if self.lineDash:
            lineDash(*self.lineDash)
        line(p1, p2)
        lineDash(None)
        if self.tipStart:
            a = angle if p1[0] < p2[0] else angle + 180
            p1A = vector(p1, self.tipLength, a + self.tipAngle)
            p1B = vector(p1, self.tipLength, a - self.tipAngle)
            polygon(p1, p1A, p1B, close=True)
        if self.tipEnd:
            a = angle + 180 if p1[0] < p2[0] else angle
            p2A = vector(p2, self.tipLength, a + self.tipAngle)
            p2B = vector(p2, self.tipLength, a - self.tipAngle)
            polygon(p2, p2A, p2B, close=True)
        restore()

if __name__ == '__main__':

    A = Arrow()
    A.tipLength = 30
    A.tipAngle = 30
    A.tipStart = False
    A.tipEnd = True
    A.color = 1, 0, 0
    A.strokeWidth = 10
    # A.lineDash = 13, 17
    A.lineCap = ['butt', 'square', 'round'][2]
    A.lineJoin = ['bevel', 'miter', 'round'][2]
    A.draw((172, 164), (860, 864))
