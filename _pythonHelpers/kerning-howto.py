'''Kerning pairs visualizer'''

import os

# ---------
# functions
# ---------

def kerningGroup(font, glyphName, pos):
    for group in font.groups.keys():
        if group.startswith('public.kern%s.' % pos):
            if glyphName in font.groups[group]:
                return group
    return glyphName

def kerningPair(font, pair):
    glyph1, glyph2 = pair
    pair = kerningGroup(font, glyph1, 1), kerningGroup(font, glyph2, 2)
    return font.kerning.get(pair, 0)

def drawGlyphOrGroup(f, glyphName, groups):
    if not groups or glyphName not in groups:
        fill(*c4)
        drawGlyph(f[glyphName])
    else:
        with savedState():
            alpha = 1.0 / len(groups[glyphName])
            fill(*(c0 + (alpha,)))
            # fill(0, 0.5)
            for glyphName in groups[glyphName]:
                drawGlyph(f[glyphName])

def drawGlyphOrGroupLines(f, glyphName, groups):
    if not groups or glyphName not in groups:
        w = f[glyphName].width
    else:
        w = []
        with savedState():
            for glyphName in groups[glyphName]:
                w.append(f[glyphName].width)
        w = max(w)
    with savedState():
        strokeWidth(sw)
        stroke(*c1)
        fill(None)
        rect(0, f.info.descender, w, 1000)
    return w

def drawPairLines(f, pair, pos, kern=False, groups={}):
    y1 = f.info.descender
    glyph1, glyph2 = pair
    value = kerningPair(f, pair)
    w = 0
    with savedState():
        translate(*pos)
        w += drawGlyphOrGroupLines(f, glyph1, groups)
        if kern:
            translate(f[glyph1].width + value, 0)
            with savedState():
                font('RoboType-Mono')
                fontSize(96)
                strokeWidth(sw)
                stroke(*c1)
                fill(*(c2+(0.3,)))
                rect(-value, y1, value, 1000)
                fill(*c2)
                stroke(None)
                text('%+d' % value, (-abs(value), y1-120))
                font('Menlo')
                fontSize(120)
                arrow = '→' if value > 0 else '←'
                d = 0 if value < 0 else -value 
                text(arrow, (d, y1+1000+60))
                w += value
        else:
            translate(f[glyph1].width, 0)
        w += drawGlyphOrGroupLines(f, glyph2, groups)
    return w

def drawPairGlyphs(f, pair, pos, kern=False, groups={}):
    y1 = f.info.descender
    glyph1, glyph2 = pair
    value = kerningPair(f, pair)    
    with savedState():
        translate(*pos)
        drawGlyphOrGroup(f, glyph1, groups)    
        if kern:
            translate(f[glyph1].width + value, 0)
        else:
            translate(f[glyph1].width, 0)
        drawGlyphOrGroup(f, glyph2, groups)

def drawPair(font, pair, pos, kern=False, groups={}):
    save()
    translate(0, abs(font.info.descender))
    w = drawPairLines(font, pair, pos, kern=kern, groups=groups)
    drawPairGlyphs(font, pair, pos, kern=kern, groups=groups)
    restore()
    return w

# ----------
# parameters
# ----------

groups = {
    'A' : 'A Aacute Acircumflex Agrave Atilde'.split(),
    'a' : 'a aacute acircumflex agrave atilde'.split(),
    'O' : ['Ocircumflexgrave', 'Schwa', 'Odotbelow', 'Ohorngrave', 'Omacron', 'Ccaron', 'Ohorntilde', 'Gcircumflex', 'Ohorn', 'Gcommaaccent', 'Gdotaccent', 'Gbreve', 'Ocircumflexdotbelow', 'Odieresis', 'Ohungarumlaut', 'Cacute', 'Ccircumflex', 'Oslash', 'Oslashacute', 'C', 'Ohook', 'G', 'Ocircumflextilde', 'O', 'Otilde', 'Q', 'Ohorndotbelow', 'Ocircumflexacute', 'Oacute', 'Cdotaccent', 'Ohornacute', 'Ograve', 'Ohornhook', 'OE', 'Ocircumflexhook', 'Obreve', 'Ccedilla', 'Ocircumflex'],
    'f' : ['fi', 'fl', 'germandbls', 'f'],
}

c0 = 0, 0.4, 1
c1 = 1, 0, 0.9
c2 = c1
c4 = c0 # 0, 0, 1
sw = 10

x, y = 40, 50
s = 0.16
d = 410

# ------
# images
# ------

def makeImage1(save=False):
    pairs = [
        ('V', 'A'),
        ('T', 'a'),
        ('f', 'parenright'),
    ]
    f = AllFonts().getFontsByFamilyName('IBM Plex Serif')[0] 
    newPage(800, 500)
    translate(x, y)
    scale(s)
    for pair in pairs:
        with savedState():
            w = []
            for i in range(2):
                kern = True if i == 0 else False    
                w += [drawPair(f, pair, (x, y), kern=kern, groups={})]
                translate(0, 1000 * 1.4)
        w = max(w)
        translate(w + d, 0)
    if save:
        imgPath = os.path.join(folder, 'adding-kern-pairs_on-off.png')
        saveImage(imgPath)

def makeImage2(save=False):
    pairs = [
        ('V', 'A'),
        ('T', 'a'),
        # ('A', 'O'),
        ('r', 'f'),
    ]
    f = AllFonts().getFontsByFamilyName('IBM Plex Serif')[0] 
    newPage(800, 280)
    translate(x, y)
    scale(s)
    for pair in pairs:
        w = []
        with savedState():
            w += [drawPair(f, pair, (x, y), kern=True, groups=groups)]
            translate(0, -1000 * 1.4)
        w = max(w)
        translate(w + d, 0)
    if save:
        imgPath = os.path.join(folder, 'adding-kern-pairs_groups.png')
        saveImage(imgPath)

folder = '/Users/gferreira/Desktop'
_save = False
makeImage1(save=_save)
makeImage2(save=_save)
