import inspect
import json
import os
import re
import tempfile

import AppKit
import defconAppKit
import mojo
import objc
import vanilla
from fontParts.base.base import dynamicProperty
from mojo.compile import executeCommand
from mojo.tools import walkDirectoryForFile


"""
Execute from RF Scripting Window

"""


# --------------
# rst converters
# --------------


def compilePandoc(txt):
    tempPath = tempfile.mkstemp(".rst")[1]
    destTempPath = tempfile.mkstemp(".md")[1]
    with open(tempPath, "w") as f:
        f.write(txt)
    executeCommand(["pandoc", "--from", "rst", "--to", "markdown_github", tempPath, "-o", destTempPath], shell=True)
    with open(destTempPath, "r") as f:
        txt = f.read()
    os.remove(tempPath)
    os.remove(destTempPath)
    return txt


def hasRST(txt):
    for search in ("::", ":class:"):
        if search in txt:
            return True
    return False


def convertRST(txt):
    txt = "\n\n%s\n\n" % txt
    classTagRE = re.compile(r":class:\`(.*?)\`")
    classTagReplacement = "`\\1`"

    refTagRE = re.compile(r":ref:\`(.*?)\`")
    refTagReplacement = "`\\1`"

    methTagRE = re.compile(r":meth:\`(.*?)\`")
    methTagReplacement = "`\\1`"

    attrTagRe = re.compile(r":attr:\`(.*?)\`")
    attrTagReplacement = "`\\1`"

    linkRe = re.compile(r"\`(.*?)[\r\n]?<(.*?)>\`_", re.MULTILINE)
    linkReplacement = "[\\1](\\2)"

    doubleTickmarksRE = re.compile(r"\`\`(.*?)\`\`")  # re ignore ```python
    doubleTickmarksReplacement = "`\\1`" # re ignore {::options

    doubleColonRE = re.compile(r"(?!\{)::(?!option)")


    replacements = [
        (classTagRE, classTagReplacement),
        (refTagRE, refTagReplacement),
        (methTagRE, methTagReplacement),
        (attrTagRe, attrTagReplacement),
        (linkRe, linkReplacement),
        (doubleTickmarksRE, doubleTickmarksReplacement),
        (re.compile(r"\.\. image\:\: .*"), ""),   #  .. image:: /_images/Window.png --> ignore
        ("\\s", ""),
        ("`type-int-float`", "[`integer/float`](../../value-types/#type-int-float)"),
        ("`type-string`", "[`string`](../../value-types/#type-string)"),
        ("`type-coordinate`", "[`coordinate`](../../value-types/#type-coordinate)"),
        ("`type-transformation`", "[`transformation`](../../value-types/#type-transformation)"),
        ("`type-angle`", "[`angle`](../../value-types/#type-angle)"),
        ("`type-identifier`", "[`identifier`](../../value-types/#type-identifier)"),
        ("`type-color`", "[`color`](../../value-types/#type-color)"),
        (doubleColonRE, ""),
        (re.compile(r"\`(.*?)\`\_"), "\\1"),  # ignore rest links
        (re.compile(r"\.\. \_.*\:.*"), ""),   #  .. _name:: link.html --> ignore

    ]

    for search, replacement in replacements:
        if isinstance(search, str):
            txt = txt.replace(search, replacement)
        else:
            txt = search.sub(replacement, txt)

    tableRE = re.compile(r"\n\n\+\-\-\-.*\-\-\-\+\n\n", re.MULTILINE | re.DOTALL)

    tableResult = ""
    i = 0
    for f in tableRE.finditer(txt):
        data = txt[f.start() : f.end()]
        data = compilePandoc(data)
        tableResult += txt[i : f.start()]
        tableResult += "\n\n"
        tableResult += data
        tableResult += "\n\n"
        i = f.end()
    tableResult += txt[i:]
    txt = tableResult

    return txt.strip()


formatter = """- type: %(type)s
  name: %(name)s
  arguments: %(args)s"""

attributeFormatter = """- type: %(type)s
  name: %(name)s"""

# -----------
# json writer
# -----------


class JSONWriter(object):

    ignore = [
        "faq",
        "milkShake",
        "absolute_import",
        "AppKit",
        "wrapClass" "frameAdjustments",
        # "preferencesChanged",
        "getNSView",
        "getNSVisualEffectView",
        "windowDeselectCallback",
        "additionContextualMenuItemsCallback_",
        "buildAdditionContectualMenuItems",
        "getNSScrollView",
        "respondsToSelector",
        "nsWindowLevel",
        "windowWillClose_",
        "nsWindowStyleMask",
        "contentView",
        "doodleWindowName",
        "closeCallback",
        "windowCloseCallback",
        "getWindowTitle",
        "raiseNotImplementedError",
    ]

    ignore.extend(dir(AppKit.NSObject))
    ignore.extend(dir(AppKit.NSView))

    ignoreSuperClasses = [
        AppKit.NSObject,
        AppKit.NSView,
        vanilla.vanillaBase.VanillaBaseObject,
        vanilla.vanillaWindows.Window,
        defconAppKit.windows.baseWindow.BaseWindowController,
    ]

    showClassButIgnoreMethods = [
        # "EditingTool",
        # "MeasurementTool",
        # "SliceTool",
        # "BezierDrawingTool",
        # "PointDataPen",
        # "DecomposePointPen",
    ]
    ignoreIndexes = []

    titleFormatter = ".. _%(title)s:\n\n%(lineAbove)s\n%(title)s\n%(underline)s\n"
    classFormatter = ".. class:: %(name)s%(args)s"
    functionFormatter = ".. function:: %(name)s%(args)s"
    attributeFormater = ".. attribute:: %(name)s"
    describeFormater = ".. describe:: %(name)s"

    def __init__(self, removeDuplicates=False, done=None, parentWriter=None, convertRST=None):
        self.clear()
        if done is None:
            done = []
        self.done = done
        self.removeDuplicates = removeDuplicates
        self.convertRST = convertRST
        if parentWriter:
            self.done = parentWriter.done
            self.convertRST = parentWriter.convertRST
            self.removeDuplicates = parentWriter.removeDuplicates

    def set(self, **kwargs):
        self.content.update(kwargs)

    def append(self, **data):
        if "objects" not in self.content:
            self.content["objects"] = []
        self.content["objects"].append(data)
        return data

    def extend(self, other, data=None):
        if not other.content:
            return
        if data is None:
            data = self.content
        data["objects"].extend(other.content["objects"])

    def __nonzero__(self):
        return len(self.content)

    def getData(self):
        return self.content

    def clear(self):
        self.content = dict()

    def write(
        self,
        root,
        name=None,
        moduleWriter=None,
        classWriter=None,
        functionWriter=None,
        methodWriter=None,
        attributeWriter=None,
        addTitle=False,
    ):
        if addTitle:
            name = self.getName(root)
            # mojoModuleDocumentation
            self.set(name=name, type="module", objects=[])
        if moduleWriter is None:
            moduleWriter = self
        if classWriter is None:
            classWriter = self
        if functionWriter is None:
            functionWriter = self
        if methodWriter is None:
            methodWriter = self
        if attributeWriter is None:
            attributeWriter = self

        if inspect.ismodule(root):
            moduleWriter.writeModule(root, name)
        elif inspect.isclass(root):
            classWriter.writeClass(root, name)
        elif inspect.isfunction(root):
            functionWriter.writeFunction(root, name)
        elif inspect.ismethod(root):
            methodWriter.writeMethod(root, name)
        elif isinstance(root, objc.selector):
            methodWriter.writeObjcMethod(root, name)
        else:
            attributeWriter.writeAttribute(root, name)
        self.done.append(name)

    def writeModule(self, module, name):
        contents = self.getModuleContent(module)
        sections, contents = self.getSections(name, contents)

        attributeWriter = self.__class__(parentWriter=self)
        functionWriter = self.__class__(parentWriter=self)
        methodWriter = self.__class__(parentWriter=self)
        classWriter = self.__class__(parentWriter=self)

        # add the content in sections
        for sectionData in sections:

            sectionAttributeWriter = self.__class__(parentWriter=self)
            sectionFunctionWriter = self.__class__(parentWriter=self)
            sectionMethodWriter = self.__class__(parentWriter=self)
            sectionClassWriter = self.__class__(parentWriter=self)

            sectionDataObject = self.append(
                name=sectionData["name"],
                type="section",
                objects=[],
                documentation=sectionData["documentation"]
            )

            for name, obj in sectionData["objects"]:
                self.write(
                    obj,
                    name,
                    classWriter=sectionClassWriter,
                    functionWriter=sectionFunctionWriter,
                    methodWriter=sectionMethodWriter,
                    attributeWriter=sectionAttributeWriter,
                )

            self.extend(sectionAttributeWriter, data=sectionDataObject)
            self.extend(sectionFunctionWriter, data=sectionDataObject)
            self.extend(sectionMethodWriter, data=sectionDataObject)
            self.extend(sectionClassWriter, data=sectionDataObject)

        # add the remaining contents
        for name, obj in contents:
            self.write(
                obj,
                name,
                classWriter=classWriter,
                functionWriter=functionWriter,
                methodWriter=methodWriter,
                attributeWriter=attributeWriter,
            )

        self.extend(attributeWriter)
        self.extend(functionWriter)
        self.extend(methodWriter)
        self.extend(classWriter)

    def writeClass(self, cls, name):
        if name is None:
            name = self.getName(cls)
        data = dict(name=name, type="class", arguments=self.getArgs(cls), objects=[])
        self.writeDoc(cls, data)
        self.append(**data)

        if name.endswith("Error"):
            return
        if name in self.showClassButIgnoreMethods:
            return

        self.removeDuplicates = True
        superClasses = inspect.getmro(cls)
        attributeWriter = self.__class__(parentWriter=self)
        functionWriter = self.__class__(parentWriter=self)
        methodWriter = self.__class__(parentWriter=self)
        classWriter = self.__class__(parentWriter=self)

        totalSuperClasses = len(superClasses)
        didAddSubclassTag = False
        for i, superClass in enumerate(superClasses):
            if superClass is object:
                continue
            if superClass in self.ignoreSuperClasses:
                continue
            if not didAddSubclassTag and not superClass.__module__.startswith("lib"):
                self.extend(attributeWriter, data)
                self.extend(functionWriter, data)
                self.extend(methodWriter, data)
                self.extend(classWriter, data)
                if attributeWriter or functionWriter or classWriter:
                    data["objects"].append(
                        dict(type="inherit", name="%s.%s" % (superClass.__module__, superClass.__name__))
                    )
                didAddSubclassTag = True
                attributeWriter = self.__class__(parentWriter=self)
                functionWriter = self.__class__(parentWriter=self)
                methodWriter = self.__class__(parentWriter=self)
                classWriter = self.__class__(parentWriter=self)

            for methodName in sorted(superClass.__dict__.keys()):
                if self.removeDuplicates and methodName in self.done:
                    continue
                if self.shouldIgnore(methodName):
                    continue
                try:
                    method = getattr(cls, methodName)
                except Exception:
                    continue
                self.write(
                    method,
                    methodName,
                    classWriter=classWriter,
                    methodWriter=methodWriter,
                    functionWriter=functionWriter,
                    attributeWriter=attributeWriter,
                )
        self.extend(attributeWriter, data)
        self.extend(functionWriter, data)
        self.extend(methodWriter, data)
        self.extend(classWriter, data)

        self.removeDuplicates = False

    def writeFunction(self, function, name):
        data = dict(type="function", name=self.getName(function), arguments=self.getArgs(function))
        self.writeDoc(function, data)
        self.append(**data)

    def writeMethod(self, method, name):
        self.writeFunction(method, name)

    def writeObjcMethod(self, method, name):
        self.writeFunction(method, name)

    def writeAttribute(self, attribute, name):
        data = dict(type="attribute", name=name)
        if isinstance(attribute, (property, dynamicProperty)):
            self.writeDoc(attribute, data)
        self.append(**data)

    def writeDescription(self, name, doc=None):
        data = dict(name=name, type="description")
        if doc:
            data["documentation"] = doc
        self.append(**data)

    def writeDoc(self, obj, data):
        doc = self.getDoc(obj)
        if doc:
            data["documentation"] = doc

    @classmethod
    def shouldIgnore(self, name):
        if name.startswith("_"):
            return True
        elif name.endswith("Class"):
            return True
        elif name in self.ignore:
            return True
        return False

    @classmethod
    def getModuleContent(self, module):
        if hasattr(module, "__all__"):
            return [(name, getattr(module, name)) for name in module.__all__ if not self.shouldIgnore(name)]
        else:
            return [(name, obj) for name, obj in inspect.getmembers(module) if not self.shouldIgnore(name)]

    def getSections(self, name, contents):
        contents = contents
        sectionData = mojoSection.get(name, [])
        sections = list()
        for sectionItem in sectionData:
            data = dict(
                name=sectionItem.get("name", ""),
                type="section",
                documentation=sectionItem.get("documentation", ""),
                objects=[]
            )
            for objectName in sectionItem.get("objects", []):
                for contentName, contentObj in list(contents):
                    if objectName == contentName or objectName == "*":
                        contents.remove((contentName, contentObj))
                        data["objects"].append((contentName, contentObj))
            sections.append(data)
        return sections, contents

    def getName(self, obj):
        if hasattr(obj, "__name__"):
            return obj.__name__
        else:
            return obj.__class__.__name__

    def getArgs(self, obj):
        if isinstance(obj, objc.selector):
            return "()"
        if inspect.isclass(obj):
            if inspect.ismethod(obj.__init__):
                value = inspect.signature(obj.__init__)
                # value = inspect.formatargspec(*inspect.getargspec(obj.__init__))
                value = str(value).replace("self, ", "").replace("self", "")
                return value
            else:
                return "()"
        value = inspect.signature(obj)
        # value = inspect.formatargspec(*inspect.getfullargspec(obj))
        value = str(value).replace("self, ", "").replace("self", "")
        return value

    def getDoc(self, obj):
        doc = inspect.getdoc(obj)
        if doc:
            if self.convertRST:
                doc = convertRST(doc)
            elif hasRST(doc):
                doc = convertRST(doc)

            identifier = self.getObjectIdentifier(obj)
            if identifier in mojoDocAdditionsPrefix:
                doc = mojoDocAdditionsPrefix[identifier] + "\n\n" + doc
            if identifier in mojoDocAdditionsSuffix:
                doc = doc + "\n\n" + mojoDocAdditionsSuffix[identifier]

        return doc

    def getObjectIdentifier(self, obj):
        if hasattr(obj, "__name__"):
            identifier = []
            if hasattr(obj, "__module__"):
                identifier.append(obj.__module__)
            identifier.append(obj.__name__)
            return ".".join(identifier)
        return None

# =================
# = doc additions =
# =================

mojoDocAdditionsPrefix = {
    # method identifier: module...methodName, string to insert
    "mojo.UI.AskString":       '<img alt="AskString"       src="/images/API/mojo/UI/AskString.png">',
    "mojo.UI.AskYesNoCancel":  '<img alt="AskYesNoCancel"  src="/images/API/mojo/UI/AskYesNoCancel.png">',
    "mojo.UI.FindGlyph":       '<img alt="FindGlyph"       src="/images/API/mojo/UI/FindGlyph.png">',
    "mojo.UI.GetFile":         '<img alt="GetFile"         src="/images/API/mojo/UI/GetFile.png">',
    "mojo.UI.GetFileOrFolder": '<img alt="GetFileOrFolder" src="/images/API/mojo/UI/GetFileOrFolder.png">',
    "mojo.UI.GetFolder":       '<img alt="GetFolder"       src="/images/API/mojo/UI/GetFolder.png">',
    "mojo.UI.Message":         '<img alt="Message"         src="/images/API/mojo/UI/Message.png">',
    "mojo.UI.SearchList":      '<img alt="SearchList"      src="/images/API/mojo/UI/SearchList.png">',
    "mojo.UI.SelectFont":      '<img alt="SelectFont"      src="/images/API/mojo/UI/SelectFont.png">',
    "mojo.UI.SelectGlyph":     '<img alt="SelectGlyph"     src="/images/API/mojo/UI/SelectGlyph.png">',
}

mojoDocAdditionsSuffix = {
}

# ============
# = sections =
# ============

mojoModuleDocumentation = {
    "mojo.Subscriber": "Subscriber is an event manager for scripters, making the bridge of subscribing and unsubsribing all sort of events super easy",
    "mojo.UI": "This page documents a large collection of function and classes able to interact with the RoboFont user interface"
}

mojoSection = {
    "mojo.UI": [

        dict(
            name="Retrieve windows",
            documentation="If you need to access a RoboFont window from a script, here you'll find the right function",
            objects=[
                "CurrentGlyphWindow",
                "CurrentFontWindow",
                "CurrentSpaceCenter",
                "CurrentSpaceCenterWindow",
                "CurrentWindow",
                "AllGlyphWindows",
                "AllFontWindows",
                "AllSpaceCenters",
                "AllSpaceCenterWindows",
                "AllWindows",
                "OutputWindow",
            ]
        ),

        dict(
            name="Open Windows",
            documentation="Handy helpers for opening RoboFont windows from a Python script",
            objects=[
                "OpenGlyphWindow",
                "OpenSpaceCenter",
                "OpenFontInfoSheet",
                "OpenScriptWindow"
            ]
        ),

        dict(
            name="Display Settings",
            documentation="These function allow the user to interact with the display settings of the glyph window",
            objects=[
                "getGlyphViewDisplaySettings",
                "setGlyphViewDisplaySettings"
            ]
        ),

        dict(
            name="Set by name",
            documentation="It is possible to set a glyph or a layer by name in the current Glyph Window",
            objects=[
                "SetCurrentGlyphByName",
                "SetCurrentLayerByName"
            ]
        ),

        dict(
            name="Draw View to PDF",
            documentation="Export to PDF the Glyph Window or the Space Center",
            objects=[
                "GlyphWindowToPDF",
                "SpaceCenterToPDF"
            ]
        ),

        dict(
            name="Test Installed Fonts",
            documentation="Functions to work with test installed fonts",
            objects=[
                "getTestInstalledFonts",
                "testDeinstallFont"
            ]
        ),

        dict(
            name="Character Sets",
            documentation="Function to manipulate and use the character sets from the RoboFont application",
            objects=[
                "setDefaultCharacterSet",
                "getDefaultCharacterSet",
                "addCharacterSet",
                "removeCharacterSet",
                "getCharacterSets",
                "setCharacterSets",
            ]
        ),

        dict(
            name="Tools",
            documentation="Miscellanea of powerful tools!",
            objects=[
                "splitText",
                "MenuBuilder"
            ]
        ),

        dict(
            name="Keyboard Tools",
            documentation="These functions allow you to interact with the keyboard and the application shortcuts",
            objects=[
                "createModifier",
                "shortKeyExists",
                "getMenuShortCuts",
                "setMenuShortCuts",
                "removeMenuShortCut"
            ]
        ),

        dict(
            name="Vanilla components",
            documentation="A collection of RoboFont specific vanilla components",
            objects=[
                "dontShowAgainMessage",
                "dontShowAgainYesOrNo",
                "AskString",
                "AskYesNoCancel",
                "FindGlyph",
                "GetFile",
                "GetFileOrFolder",
                "GetFolder",
                "Message",
                "PutFile",
                "SearchList",
                "SelectFont",
                "SelectGlyph",
                "PostBannerNotification",
            ]
        ),

        dict(
            name="RoboFont Windows",
            documentation="A collection of your favourite RoboFont windows to be subclassed or initiated",
            objects=[
                "HelpWindow",
                "DoodleWindow",
                "StatusInteractivePopUpWindow",
                "ScrollToFirstResponderWindow",
                "ModalWindow",
                "ShowHideWindow",
                "ShowHideFloatingWindow",
                "ScrollToFirstResponderSheet",
            ]
        ),

        dict(
            name="RoboFont Views",
            documentation="A collection of RoboFont Views to be composed into a vanilla window",
            objects=[
                "SpaceCenter",
                "AccordionView",
                "MultiLineView",
                "HTMLView",
                "SpaceMatrix",
                "CodeEditor",
                "KernCenter",
                "GroupCenter",
            ]
        ),

        dict(
            name="RoboFont Controls",
            documentation="A collection of RoboFont Controls to be composed into a vanilla window",
            objects=[
                "GlyphSequenceEditText",
                "EditStepper",
                "EditIntStepper",
                "SliderEditStepper",
                "NumberEditText",
                "StatusBar",
                "LightStatusBar",
                "SimpleStatus",
                "ProgressBar",
            ]
        ),

        dict(
            name="Undo",
            documentation="These functions are the best way to keep track of undo/redo -ing processes",
            objects=[
                "isUndoing",
                "isRedoing"
            ]
        ),

        dict(
            name="Preferences",
            documentation="Did you know that you can access and manipulate application preferences with Python code?",
            objects=[
                "preferencesChanged",
                "exportPreferences",
                "importPreferences",
                "getDefault",
                "setDefault",
                "removeDefault",
                "setMaxAmountOfVisibleTools",
                "getMaxAmountOfVisibleTools",
            ],
        ),

        dict(
            name="Passwords",
            documentation="A collection of function to deal with passwords",
            objects=[
                "setPassword",
                "getPassword",
                "deletePassword"
            ]
        ),

        dict(
            name="Deprecated",
            documentation="A collection of still usable but deprecated functions/objects",
            objects=[
                "UpdateCurrentGlyphView",
                "SliderEditIntStepper",
                "getScriptingMenuNamingShortKey",
                "setScriptingMenuNamingShortKey",
                "setScriptingMenuNamingShortKeyForPath",
                "removeScriptingMenuNamingShortKeyForPath",
            ],
        )
    ]
}

# -------
# globals
# -------

### Frederik's local settings
appRoot = "/Users/frederik/Documents/apps/roboFont/robofont"
_comRoot = "/Users/frederik/Documents/apps/roboFont/site/robofont_com"

if not os.path.exists(appRoot):
    ### Roberto's local settings
    appRoot = "/Users/robertoarista/Documents/Repositories/Robofont/RF_virtualApp/roboFont.test/roboFont.source/robofont"
    _comRoot = "/Users/robertoarista/Documents/Repositories/Robofont/robofont_com"

apiDest = os.path.join(_comRoot, "_data", "API")


fontPartsStuff = [
    "RFont",
    "RLayer",
    "RInfo",
    "RKerning",
    "RGroups",
    "RFeatures",
    "RLib",
    "RGlyph",
    "RContour",
    "RSegment",
    "RAnchor",
    "RComponent",
    "RGuideline",
    "RImage",
    "RPoint",
    "RBPoint",
]

# ----
# mojo
# ----


def extractMojoDocs(fontPartsStuff):

    mojoData = []
    writer = JSONWriter()

    # ignore FontParts objects
    writer.ignore.extend(fontPartsStuff)

    for name, module in writer.getModuleContent(mojo):
        writer.clear()
        writer.write(module, addTitle=True)
        mojoData.append(writer.content)

    return mojoData


def writeMojoDocs(mojoData, apiDest):
    mojoDest = os.path.join(apiDest, "mojo.json")
    with open(mojoDest, "w") as f:
        json.dump(mojoData, f, indent=2)


# ----------
# font parts
# ----------


def extractFontPartsDocs(fontPartsStuff):

    import lib.fontObjects.fontPartsWrappers as fontPartsWrappers

    fontPartsData = []

    for name in fontPartsStuff:
        obj = getattr(fontPartsWrappers, name)
        writer = JSONWriter(convertRST=True)
        writer.write(obj, addTitle=True)
        fontPartsData.append(writer.content)

    return fontPartsData


def writeFontPartsDocs(fontPartsData, apiDest):
    fontPartsDest = os.path.join(apiDest, "fontParts.json")
    with open(fontPartsDest, "w") as f:
        json.dump(fontPartsData, f, indent=2)


# ---------
# observers
# ---------


def extractObserversDocs(appRoot):

    # search for all publish tags...
    publishEventRE = re.compile(r"(?:publishEvent|postEvent)\([\"\'](.+?)[\"\'](.*?)\)")
    observers = dict(
        fontWillOpen=set(["font"]),
        newFontWillOpen=set(["font"]),
        fontDidOpen=set(["font"]),
        newFontDidOpen=set(["font"]),
        fontResignCurrent=set(["font"]),
        fontBecameCurrent=set(["font"]),
    )

    # get all publishEvent callbacks
    for path in walkDirectoryForFile(appRoot):
        with open(path, "r", encoding="utf-8") as f:
            code = f.read()

        for m in publishEventRE.findall(code):
            event, args = m
            args = args.replace(",", "").replace("**_", "").replace("**", "").strip().split()
            args = [a.split("=")[0] for a in args]
            if event not in observers:
                observers[event] = set()
            observers[event] |= set(args)

    return observers


def writeObserversDocs(observers, apiDest):

    cls = mojo.events.BaseEventTool

    writer = JSONWriter()
    writer.set(name="observers")
    for key in sorted(observers):
        doc = []
        args = observers.get(key)
        if hasattr(cls, key):
            externalDoc = writer.getDoc(getattr(cls, key))
            if externalDoc:
                doc.append(externalDoc.split("\n\n")[0])
        if args:
            if doc:
                doc.append("")
            doc.append("Extra notification keys: `%s`" % ("`, `".join(sorted(args))))

        writer.writeDescription(key, "\n".join(doc))

    observersAPIPath = os.path.join(apiDest, "observersAPI.json")

    with open(observersAPIPath, "w") as f:
        json.dump(writer.content["objects"], f, indent=2)


# =====================
# = preference editor =
# =====================

def writePreferenceEditor():
    from lib.UI.preferences.defaultPreferences import _defaults
    from lib.UI.preferences.preferenceEditor import HumanPreferencesWriter

    writer = HumanPreferencesWriter("Edit defaults with json")
    writer.write(_defaults)
    defaultText = writer.get()

    preferenceEditorPage = os.path.join(_comRoot, "_documentation", "reference", "workspace", "preferences-editor.md")

    with open(preferenceEditorPage, "r", encoding="utf-8") as f:
        text = f.read()

    text = text.split("```python")[0]
    text += f"```python\n{defaultText}\n```"

    text = text.replace("/Users/frederik/", "~/")
    text = text.replace("/Users/robertoarista/", "~/")

    with open(preferenceEditorPage, "w", encoding="utf-8") as f:
        f.write(text)


# ------------
# extract docs
# ------------

print("extracting API docs...")

print("\textracting mojo...")
mojoData = extractMojoDocs(fontPartsStuff)
writeMojoDocs(mojoData, apiDest)

print("\textracting fontParts...")
fontPartsData = extractFontPartsDocs(fontPartsStuff)
writeFontPartsDocs(fontPartsData, apiDest)

print("\textracting observers...")
observers = extractObserversDocs(appRoot)
writeObserversDocs(observers, apiDest)

print("\twrite preference editor...")
writePreferenceEditor()

print("...done.\n")
