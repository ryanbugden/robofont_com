from Quartz import CoreGraphics
import AppKit


def imagecapture(rect=None, window=None, path=None, excludeDesktop=True, windowShadow=True):
    """
    rect: top, left, w, h
    """
    if rect is None:
        rect = CoreGraphics.CGRectInfinite
    else:
        rect = AppKit.NSMakeRect(*rect)
    imageOptions = CoreGraphics.kCGWindowImageDefault
    if window is None:
        windowID = CoreGraphics.kCGNullWindowID
        options = CoreGraphics.kCGWindowListOptionAll
    else:
        rect = CoreGraphics.CGRectNull
        options = CoreGraphics.kCGWindowListOptionIncludingWindow
        windowID = window.windowNumber()
        if not windowShadow:
            imageOptions |= CoreGraphics.kCGWindowListOptionOnScreenOnly
            # imageOptions |= CoreGraphics.kCGWindowImageBoundsIgnoreFraming

    if excludeDesktop:
        options |= CoreGraphics.kCGWindowListExcludeDesktopElements

    screenshot = CoreGraphics.CGWindowListCreateImage(rect,
                                                      options,
                                                      windowID,
                                                      imageOptions)
    image = AppKit.CIImage.imageWithCGImage_(screenshot)
    bitmapRep = AppKit.NSBitmapImageRep.alloc().initWithCIImage_(image)
    pngData = bitmapRep.representationUsingType_properties_(AppKit.NSPNGFileType, None)
    if path:
        pngData.writeToFile_atomically_(path, False)
    nsImage = AppKit.NSImage.alloc().initWithSize_(bitmapRep.size())
    nsImage.addRepresentation_(bitmapRep)
    return nsImage


if __name__ == '__main__':
    import drawBot as dB
    # ---- EXAMPLES ---- #

    # entire screen
    im = imagecapture()
    w, h = im.size()
    dB.newPage(w, h)
    dB.image(im, (0, 0))

    # a portion of the screen
    im = imagecapture(rect=(0, 0, 632, 408))
    w, h = im.size()
    dB.newPage(w, h)
    dB.image(im, (0, 0))

    # a specific window
    f = RFont()
    import time
    # the window animation... :)
    time.sleep(.5)
    from mojo.UI import CurrentFontWindow

    window = CurrentFontWindow().window().getNSWindow()
    im = imagecapture(window=window, windowShadow=True)
    w, h = im.size()
    dB.newPage(w, h)
    dB.image(im, (0, 0))
    f.close()
