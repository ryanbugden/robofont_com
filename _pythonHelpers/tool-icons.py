pdfs = [
    '../images/workspace/tools/toolbarToolMeasurement.pdf',
    '../images/workspace/tools/toolbarToolsArrow.pdf',
    '../images/workspace/tools/toolbarToolsPen.pdf',
    '../images/workspace/tools/toolbarToolsSlice.pdf'
]

imgSize = 120

for pdf in pdfs:
    newPage(imgSize, imgSize)
    w, h = imageSize(pdf)
    sx = width() / w
    sy = height() / h
    scale(sx, sy)
    image(pdf, (0, 0))
    saveImage(pdf.replace('.pdf', '.png'))
