'''accentsDict2GlyphConstruction'''

#: A dictionary with data for building accented glyphs, as typically used with RoboFab's RFont.compileGlyph()
accentsDict = {
    # accented   : [ base, ( (accent1, anchor1), (accent2, anchor2), ... ) ],
    'aogonek'    : [ 'a',  ( ('ogonek', 'bottom'), ) ],
    'agrave'     : [ 'a',  ( ('grave', 'top'), ) ],
    'ccedilla'   : [ 'c',  ( ('cedilla', 'bottom'), ) ],
    'aringacute' : [ 'a',  ( ('ring', 'top'), ('acute', 'top'), ) ],
}

def accentsDict2GlyphConstruction(accentsDict, useSpaces=True):
    '''Converts glyph building recipes from dictionary to Glyph Construction syntax.'''

    # create an empty string to collect all GlyphConstruction rules
    glyphConstruction = ''

    # iterate over the accented glyphs dict
    for accentedGlyph in accentsDict:

        # get base glyph and accents for this accented glyph
        baseGlyph, accents = accentsDict[accentedGlyph]

        # start a glyph construction rule for this glyph:
        # accentedGlyph = baseGlyph + (some accents)
        glyphConstruction += '%s = %s + ' % (accentedGlyph, baseGlyph)

        # iterate over all accents in accented glyph, using a counter
        for i, (accentName, anchor) in enumerate(accents):
            # if accent is not the first, add a '+' before it
            if not i == 0:
                glyphConstruction += ' + '
            # add accent name and anchor name to glyph construction rule
            glyphConstruction += '%s@%s' % (accentName, anchor)

        # done with this accented glyph; go to the next line
        glyphConstruction += '\n'

    # remove all spaces
    if not useSpaces:
        glyphConstruction = glyphConstruction.replace(' ', '')

    # done!
    return glyphConstruction

# convert accents dict to glyph construction
glyphConstruction = accentsDict2GlyphConstruction(accentsDict, useSpaces=False)
print(glyphConstruction)