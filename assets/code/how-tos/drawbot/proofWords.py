# define the text we want to set
txt = "hãmbürgêfóns"

# get the current font
f = CurrentFont()

# get the font’s character map
charMap = f.getCharacterMapping()

# define the document size
size('A4Landscape')

# move to origin
translate(50, 250)

# set scale
scale(0.1)

# iterate over the characters in text
for char in txt:

    # get unicode value for character
    uni = ord(char)

    # get glyph names for unicode value
    glyphNames = charMap.get(uni)

    # skip glyphs which are not in the font
    if not glyphNames:
        continue

    # draw glyph
    glyph = f[glyphNames[0]]
    drawGlyph(glyph)

    # move position to next glyph
    translate(glyph.width, 0)