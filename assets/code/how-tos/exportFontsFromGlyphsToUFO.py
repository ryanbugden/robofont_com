# MenuTitle: batch ufo export
# -*- coding: utf-8 -*-

__doc__ = """Export all open fonts to ufo"""

import os.path

Exporter = NSClassFromString('GlyphsFileFormatUFO').alloc().init()

for f in Glyphs.fonts:
    Master = f.fontMasters()[0]
    Exporter.setFontMaster_(Master)
    filePath = f.parent.fileURL().path()
    # TODO: make a folder called 'ufo' and save the ufos there
    fileName, folderName = os.path.basename(filePath), os.path.dirname(filePath)
    ufoFolder = os.path.join(folderName, 'ufo')
    ufoFileName = fileName.replace('.glyphs', '.ufo')

    if not os.path.exists(ufoFolder):
        os.mkdir(ufoFolder)

    ufoFilePath = os.path.join(ufoFolder, ufoFileName)
    Result = Exporter.writeUfo_toURL_error_(f, NSURL.fileURLWithPath_(ufoFilePath), None)

# open folder where the ufo files were saved
import os
os.system("open %s" % ufoFolder)