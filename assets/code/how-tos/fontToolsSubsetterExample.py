from fontTools import subset

font = OpenFont()
fullPath = font.path.replace('.ufo', '_Full.otf')
trialPath = font.path.replace('.ufo', '_Trial.otf')

# generate full otf
font.info.familyName += ' Trial'
font.generate(path=fullPath, format='otf', decompose=True, checkOutlines=True, releaseMode=True)
font.close()

# subset full otf
options = subset.Options()
# print(dir(options))
font = subset.load_font(fullPath, options)
subsetter = subset.Subsetter(options)
subsetter.populate(text='ABCDXYZ abcdxyz')
subsetter.subset(font)
subset.save_font(font, trialPath, options)

# delete full otf
import os
os.remove(fullPath)

# open generated trial font in UI to check
OpenFont(trialPath)