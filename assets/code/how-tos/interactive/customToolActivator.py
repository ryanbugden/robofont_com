# change this import with the tool you'd like to activate/deactivate
from simpleExampleTool import MyTool

from vanilla import FloatingWindow, CheckBox
from mojo.subscriber import WindowController
from mojo.events import installTool, uninstallTool, getToolOrder

class MyToolActivator(WindowController):

    def __init__(self, tool):
        self.tool = tool
        super().__init__()

    def build(self):
        # install the tool
        installTool(self.tool)

        self.w = FloatingWindow((123, 44), 'MyTool')
        self.w.button = CheckBox((10, 10, -10, 24), 'activate',
                                 value=True, callback=self.activateToolCallback)
        self.w.open()

    def activateToolCallback(self, sender):
        # if the tool is not installed, install it
        if sender.get():
            installTool(self.tool)
        # if the tool is installed, uninstall it
        else:
            uninstallTool(self.tool)

    def windowWillClose(self, sender):
        # if the tool is active, remove it
        tools = getToolOrder()
        if self.tool.__class__.__name__ in tools:
            uninstallTool(self.tool)


if __name__ == '__main__':
    someTool = MyTool()
    MyToolActivator(someTool)
