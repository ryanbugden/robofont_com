from mojo.roboFont import OpenWindow
from mojo.subscriber import WindowController
from merz import MerzView
from vanilla import Window

class BrushWindow(WindowController):

    debug = True
    fillColor = (1, 0, 0, .25)
    brushRadius = 40

    def build(self):
        self.w = Window((600, 600), "Brush Window")
        self.w.view = MerzView(
            (0, 0, 0, 0),
            backgroundColor=(1, 1, 1, 1),
            delegate=self
        )
        container = self.w.view.getMerzContainer()
        self.pointerLayer = container.appendOvalSublayer(
            position=(0, 0),
            anchor=(.5, .5),
            size=(self.brushRadius*3, self.brushRadius*3),
            strokeColor=(0, 1, 1, .5),
            strokeWidth=5,
            fillColor=None
        )
        self.brushLayer = container.appendBaseSublayer()
        self.w.open()

    def acceptsFirstResponder(self, sender):
        # necessary for accepting mouse events
        return True

    def acceptsMouseMoved(self, sender):
        # necessary for tracking mouse movement
        return True

    def destroy(self):
        container = self.w.view.getMerzContainer()
        container.clearSublayers()

    def mouseMoved(self, view, event):
        x, y = event.locationInWindow()
        self.pointerLayer.setPosition((x, y))

    def mouseDragged(self, view, event):
        x, y = event.locationInWindow()
        self.pointerLayer.setPosition((x, y))
        self.brushLayer.appendOvalSublayer(
            position=(x, y),
            anchor=(.5, .5),
            size=(self.brushRadius*2, self.brushRadius*2),
            fillColor=self.fillColor
        )

    def mouseUp(self, view, event):
        self.brushLayer.clearSublayers()


if __name__ == '__main__':
    OpenWindow(BrushWindow)
