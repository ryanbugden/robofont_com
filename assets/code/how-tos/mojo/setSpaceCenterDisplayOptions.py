from mojo.UI import CurrentSpaceCenter

sp = CurrentSpaceCenter()

# set sample contents
sp.setRaw('abcdefghijklmnopqrstuvwxyz')
sp.setPre('/o/n')
sp.setAfter('/n')

# get display states
states = sp.glyphLineView.getDisplayStates()

# modify display states
states["Show Space Matrix"] = True
states["Right to Left"] = False
states["Upside Down"] = True
states["Inverse"] = True

# set display states
sp.glyphLineView.setDisplayStates(states)