from vanilla import FloatingWindow, SquareButton
from mojo.events import addObserver, removeObserver
from mojo.drawingTools import *

class CustomGlyphCellLabel:

    key = "com.myDomain.customGlyphCellLabel"

    def __init__(self):
        self.w = FloatingWindow((180, 52), "custom labels")
        self.w.button = SquareButton((10, 10, -10, -10), "add/remove label", callback=self.actionCallback)
        self.w.bind("close", self.closeCallback)
        self.w.open()
        addObserver(self, "drawCell", "glyphCellDrawBackground")

    def drawCell(self, info):
        glyph = info["glyph"]
        value = glyph.lib.get(self.key)
        if value:
            fill(1, 0, 0)
            rect(0, 0, 10, 10)

    def actionCallback(self, sender):
        font = CurrentFont()
        if font is None:
            return

        for glyphName in font.selectedGlyphNames:
            # get current value
            value = font[glyphName].lib.get(self.key, False)
            # toggle value
            font[glyphName].lib[self.key] = not value
            font[glyphName].changed()

    def closeCallback(self, sender):
        font = CurrentFont()
        if font is None:
            return

        for glyph in font:
            if not self.key in glyph.lib:
                continue
            del glyph.lib[self.key]
            glyph.changed()

        removeObserver(self, "glyphCellDrawBackground")

CustomGlyphCellLabel()