"""
FontParts provides the same functionality
out-of-the-box but it is not cached

"""

from defcon import Glyph, registerRepresentationFactory
from fontTools.pens.areaPen import AreaPen
from mojo.subscriber import Subscriber, registerGlyphEditorSubscriber


def glyphAreaFactory(glyph):
    pen = AreaPen(glyphset=glyph.layer)
    glyph.draw(pen)
    return pen.value


class AreaSubscriber(Subscriber):

    debug = True

    def build(self):
        self.glyphEditor = self.getGlyphEditor()
        self.container = self.glyphEditor.extensionContainer(
            identifier="com.roboFont.areaSubscriber",
            location="foreground",
            clear=True,
        )
        self.textLayer = self.container.appendTextLineSublayer(
            position=(0, 0),
            size=(200, 50),
            pointSize=24,
            fillColor=(0, 0, 0, 1),
            text="",
        )

    def started(self):
        self.updateArea()

    def destroy(self):
        self.container.clearSublayers()

    def updateArea(self):
        glyph = self.glyphEditor.getGlyph().asFontParts()
        area = glyph.getRepresentation("areaRepresentation")
        self.textLayer.setText(f"{area:.2f}")

    def glyphEditorDidSetGlyph(self, info):
        self.updateArea()

    def glyphEditorGlyphDidChangeOutline(self, info):
        self.updateArea()


if __name__ == "__main__":
    registerRepresentationFactory(Glyph, "areaRepresentation", glyphAreaFactory)
    registerGlyphEditorSubscriber(AreaSubscriber)
