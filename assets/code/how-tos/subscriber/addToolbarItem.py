from AppKit import NSImageNameAdvanced
from mojo.subscriber import Subscriber, registerRoboFontSubscriber


class AddToolbarItem(Subscriber):
    debug = True

    def fontDocumentWantsToolbarItems(self, info):
        # create the button
        newItem = {'itemIdentifier': 'MyButton',
                   'label':          'My Button',
                   'toolTip':        'My Button',
                   'imageNamed':      NSImageNameAdvanced,
                   'callback':        self.customButton}

        # add it to the toolbar
        info['itemDescriptions'].insert(2, newItem)

    def customButton(self, sender):
        print('click!')


if __name__ == '__main__':
    registerRoboFontSubscriber(AddToolbarItem)
