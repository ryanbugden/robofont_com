from vanilla import FloatingWindow
from mojo.glyphPreview import GlyphPreview
from mojo.subscriber import Subscriber, WindowController, registerCurrentGlyphSubscriber
from mojo.roboFont import CurrentGlyph

class StencilPreview(Subscriber, WindowController):

    debug = True

    def build(self):
        self.w = FloatingWindow((400, 400), "Stencil Preview", minSize=(200, 200))
        self.w.preview = GlyphPreview((0, 0, -0, -0))

    def started(self):
        self.updateGlyph(CurrentGlyph())
        self.w.open()

    def destroy(self):
        self.w.close()

    def glyphEditorDidSetGlyph(self, info):
        self.updateGlyph(CurrentGlyph())

    def glyphEditorGlyphDidChange(self, info):
        glyph = info["glyph"]
        if glyph is None:
            return
        self.updateGlyph(glyph)

    def updateGlyph(self, glyph):
        # if there is no glyph, just set `None` in the preview
        if glyph is None:
            self.w.preview.setGlyph(None)
            return
        # get the foreground and background layers
        foreground = glyph.getLayer("foreground")
        background = glyph.getLayer("background")
        # get the result by subtracting background from foreground layer
        result = foreground % background
        # set the result in the preview view
        self.w.preview.setGlyph(result)


if __name__ == '__main__':
    registerCurrentGlyphSubscriber(StencilPreview)
