class MyInterface:

    def __init__(self, glyph):
        self.glyph = glyph
        self.glyph.addObserver(self, "glyphWidthChangedCallback", "Glyph.WidthChanged")

    def glyphWidthChangedCallback(self, notification):
        glyph = notification.object
        oldWidth = notification.data['oldValue']
        newWidth = notification.data['newValue']
        print(f"width of '{glyph.name}' changed! {oldWidth} → {newWidth}")

MyInterface(CurrentGlyph())