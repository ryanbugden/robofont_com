import vanilla
import merz
from mojo.subscriber import Subscriber, registerRoboFontSubscriber
from mojo.roboFont import CurrentGlyph

class GlyphVisualizer(Subscriber):

    debug = True

    def build(self):
        self.glyph = CurrentGlyph()

        self.w = vanilla.FloatingWindow((400, 800), "Glyph Visualizer", minSize=(200, 300))
        self.merzView = merz.MerzView("auto")

        self.w.stack = vanilla.VerticalStackView(
            (0, 0, 0, 0),
            views=[dict(view=self.merzView)],
            edgeInsets=(15, 15, 15, 15)
        )
        container = self.merzView.getMerzContainer()

        # a layer for the glyph and the baseline
        self.backgroundLayer = container.appendBaseSublayer(
            size=(self.glyph.width, self.glyph.font.info.unitsPerEm),
            backgroundColor=(1, 1, 1, 1)
        )

        self.glyphLayer = self.backgroundLayer.appendPathSublayer(
            position=(0, -self.glyph.font.info.descender)
        )
        glyphPath = self.glyph.getRepresentation("merz.CGPath")
        self.glyphLayer.setPath(glyphPath)

        self.lineLayer = self.backgroundLayer.appendLineSublayer(
            startPoint=(0, -self.glyph.font.info.descender),
            endPoint=(self.glyph.width, -self.glyph.font.info.descender),
            strokeWidth=1,
            strokeColor=(1, 0, 0, 1)
        )

    def started(self):
        self.w.open()

    def glyphEditorDidSetGlyph(self, info):
        self.glyph = info['glyph']
        glyphPath = self.glyph.getRepresentation("merz.CGPath")
        self.glyphLayer.setPath(glyphPath)
        self.backgroundLayer.setSize((self.glyph.width, self.glyph.font.info.unitsPerEm))
        self.lineLayer.setEndPoint((self.glyph.width, -self.glyph.font.info.descender))


if __name__ == '__main__':
    registerRoboFontSubscriber(GlyphVisualizer)
