from vanilla import Window, Button

class ButtonDemo:

    def __init__(self):
        self.w = Window((100, 40))
        self.w.button = Button((10, 10, -10, 20), "A Button",
                               callback=self.buttonCallback)
        self.w.open()

    def buttonCallback(self, sender):
        print("button hit!")


ButtonDemo()