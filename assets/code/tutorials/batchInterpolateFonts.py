import os
from vanilla.dialogs import getFolder

# get masters and destination folder
font1 = OpenFont('path/to/font1.ufo')
font2 = OpenFont('path/to/font2.ufo')
folder = getFolder("Select a folder to save the interpolations")[0]

# instance names and interpolation factors
instances = [
    ("Light", 0.25),
    ("Regular", 0.5),
    ("Bold", 0.75),
]

# generate instances
print('generating instances...\n')
for instance in instances:
    styleName, factor = instance
    print(f"\tgenerating {styleName} ({factor})...")

    # make a new font
    destFont = NewFont()

    # interpolate the glyphs
    destFont.interpolate(factor, font1, font2)

    # set font name
    destFont.info.familyName = "MyFamily"
    destFont.info.styleName = styleName
    destFont.changed()

    # save instance
    fileName = f'{destFont.info.familyName}_{destFont.info.styleName}.ufo'
    ufoPath = os.path.join(folder, fileName)
    print(f'\tsaving instances at {ufoPath}...')
    destFont.save(ufoPath)

    # done with instance
    destFont.close()
    print()   # empty line in console

print('...done.\n')
