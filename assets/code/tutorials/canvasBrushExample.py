from vanilla import Window
from mojo.canvas import Canvas
from mojo.drawingTools import *

class CanvasBrushExample:

    radius = 7
    color  = 0, 1, 0
    points = []
    shape  = oval

    def __init__(self):
        self.w = Window((300, 300), title='CanvasBrush', minSize=(200, 200))
        self.w.canvas = Canvas((10, 10, -10, -10), delegate=self)
        self.w.open()

    def acceptsMouseMoved(self):
        return True

    def draw(self):
        if not len(self.points):
            return
        view = self.w.canvas.getNSView()
        fill(*self.color)
        for pt in self.points:
            pos = view.convertPoint_fromView_(pt, None)
            save()
            translate(pos.x, pos.y)
            self.shape(-self.radius, -self.radius, self.radius * 2, self.radius * 2)
            restore()

    def mouseMoved(self, event):
        self.points = [event.locationInWindow()]
        self.w.canvas.update()

    def mouseDragged(self, event):
        self.points.append(event.locationInWindow())
        self.w.canvas.update()

    def mouseDown(self, event):
        self.radius *= 2
        self.color = 1, 0, 0, 0.2
        self.w.canvas.update()

    def mouseUp(self, event):
        self.radius *= 0.5
        self.color = 0, 1, 0
        self.points = []
        self.w.canvas.update()

    def keyDown(self, event):
        self.shape = rect

    def keyUp(self, event):
        self.shape = oval

CanvasBrushExample()