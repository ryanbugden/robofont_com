import os
from fontTools.ufoLib import UFOReader

def getGlifPath(glyph):
    reader = UFOReader(glyph.font.path)
    # get glyphset for currrent layer
    glyphSet = reader.getGlyphSet(glyph.layer.name)
    # get filename for glyph name
    glifName = glyphSet.glyphNameToFileName(glyph.name, None)
    # glif path = ufo path + layer folder + glif file
    glifPath = os.path.join(glyph.font.path, glyphSet.dirName, glifName)
    return glifPath

print(getGlifPath(CurrentGlyph()))