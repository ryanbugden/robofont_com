# get the current font
font1 = CurrentFont()

# open a second font (without the UI)
font2 = OpenFont(showInterface=False)

# create target layer
layerName = f"{font2.info.familyName} {font2.info.styleName}"
dstLayer = font1.newLayer(layerName)

# loop over all glyphs in the second font
for glyph in font2:

    # add the glyph to the layer
    dstLayer[glyph.name] = glyph

# done!