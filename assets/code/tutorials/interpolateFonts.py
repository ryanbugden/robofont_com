# get fonts
font1 = OpenFont('path/to/font1.ufo')
font2 = OpenFont('path/to/font2.ufo')

# define interpolation factor
factor = 0.5

# make destination font
font3 = NewFont()

# this interpolates the glyphs
font3.interpolate(factor, font1, font2)

# this interpolates the kerning
# comment this line out of you're just testing
font3.kerning.interpolate(factor, font1.kerning, font2.kerning)

# done!
font3.save('path/to/font3.ufo')
