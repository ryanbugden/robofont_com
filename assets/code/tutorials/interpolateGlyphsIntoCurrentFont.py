# the font where the interpolated glyphs will be stored
f = CurrentFont()

# the two master fonts
f1 = AllFonts().getFontsByStyleName('Regular')[0]
f2 = AllFonts().getFontsByStyleName('Bold')[0]

# the interpolation factor
factor = 0.4

# a list of glyph names to be interpolated
glyphNames = ['A', 'B', 'C', 'a', 'b', 'c']

# iterate over the glyph names
for glyphName in glyphNames:

    # if this glyph is not available in one of the masters, skip it
    if glyphName not in f1:
        print(f'{glyphName} not in {f1}, skipping…')
        continue
    if glyphName not in f2:
        print(f'{glyphName} not in {f2}, skipping…')
        continue

    # if the glyph does not exist in the destination font, create it
    if glyphName not in f:
        f.newGlyph(glyphName)

    # interpolate glyph
    print(f'interpolating {glyphName}…')
    f[glyphName].interpolate(factor, f1[glyphName], f2[glyphName])
