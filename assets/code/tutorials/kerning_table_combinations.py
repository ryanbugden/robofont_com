# glyph groups
from string import ascii_lowercase as lowercase
from string import ascii_uppercase as uppercase
figures = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']
punctuation = ['underscore', 'hyphen', 'endash', 'emdash', 'parenleft',
               'parenright', 'period', 'comma', 'colon', 'semicolon', 'exclam', 'question']

# functions
def printPairList(first, second):
    for eachF in first:
        for eachS in second:
            print(f'{eachF} {eachS}')


# variables
maybe = True

# this table mimics the one above in the article
combinationsTable = [
    # lower2   # upper2   # fig2   # punct2
    [True,       maybe,     False,   True],    # lowercase1
    [True,       True,      False,   False],   # uppercase1
    [False,      False,     True,    True],    # figures1
    [False,      False,     False,   True],    # punctuation1
]
groupSequence = [lowercase, uppercase, figures, punctuation]

for jj, eachRow in enumerate(combinationsTable):
    for ii, shouldKern in enumerate(eachRow):

        if shouldKern:
            first = groupSequence[jj]
            second = groupSequence[ii]
            printPairList(first, second)
