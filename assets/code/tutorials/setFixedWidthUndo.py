# define a width value
value = 400

# get the current font
font = CurrentFont()

# iterate over all selected glyphs in the font
for glyph in font.selectedGlyphs:

    # wraps the glyph width change in an undo context
    with glyph.undo():

        # set glyph width
        glyph.width = value
