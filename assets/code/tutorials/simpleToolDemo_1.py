from vanilla import FloatingWindow, Button

class ToolDemo:

    def __init__(self):

        self.w = FloatingWindow((123, 70), "myTool")

        x, y = 10, 10
        padding = 10
        buttonHeight = 20

        self.w.printButton = Button(
                (x, y, -padding, buttonHeight),
                "print",
                # call this function when the button is clicked
                callback=self.printGlyphsCallback)

        y += buttonHeight + padding
        self.w.paintButton = Button(
                (x, y, -padding, buttonHeight),
                "paint",
                # call this function when the button is clicked
                callback=self.paintGlyphsCallback)

        self.w.open()

    # callbacks

    def printGlyphsCallback(self, sender):
        print('printing...')

    def paintGlyphsCallback(self, sender):
        print('painting...')

ToolDemo()