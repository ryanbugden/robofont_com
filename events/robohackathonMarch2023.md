---
layout: page
title: RoboHackathon Ezui, March 2023
excludeNavigation: true
---

{% image events/RoboHackathonMarch2023.mp4 %}

The first RoboHackathon of the year will be on the 8th of March when Tal Leming will present [ezui](https://typesupply.github.io/ezui/index.html): a fresh new python wrapper around vanilla. ezui makes creating user interfaces SO-MUCH-EAZIER!

> Be aware that the Keynote session will be recorded and shared publicly after the event!
{: .warning}

## Program
* March, 8th
    * 20 → 21: Keynotes by Tal Leming
    * 20 → 22: ezui lab with Frederik and Tal

## Video

{% include embed vimeo=808771374 %}

## Demos

- [Talk Demos](https://gist.github.com/typesupply/15e81a043856c1808e28d61814707653) by Tal
- [Lab Demos](https://github.com/typemytype/hackathon-ezui) by Frederik

> - [ezui](https://typesupply.github.io/ezui/index.html)
{: .seealso}