---
layout: page
title: Oeps RoboFont Launch Issues
excludeNavigation: true
---

Oeps... RoboFont can not be launched. Some steps to find out what goes wrong:

- go to the RoboFont.app in the Applications folder
- right-click and select "Show Package Contents"
- double-click on `Contents/MacOS/RoboFont` 


> - {% internallink 'submitting-bug-reports' %}
{: .seealso }