Layers
------

The *Layers* section offers an interface for creating and managing layers, and for setting the display attributes of each layer.

{% image reference/workspace/glyph-editor/inspector_layers.png %}

The **current layer** is indicated by a circle with a black dot.

The **default layer** is indicated by a star.