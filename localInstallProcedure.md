22/07/2022

Follow these steps to install jekyll locally on your machine:

- `$ brew install chruby ruby-install`
- remove old command line tools from `/Library/Developer/Command Line Tools`
- `$ xcode-select --install`
- `$ ruby-install ruby`
- `$ echo "source $(brew --prefix)/opt/chruby/share/chruby/chruby.sh" >> ~/.zshrc`
- `$ echo "source $(brew --prefix)/opt/chruby/share/chruby/auto.sh" >> ~/.zshrc`
- `$ echo "chruby ruby-3.1.2" >> ~/.zshrc # run 'chruby' to see actual version`
- go to the robofont_com folder
- `bundle install`