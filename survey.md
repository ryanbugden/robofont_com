---
layout: page
title: Mechanic Extensions Survey
excludeNavigation: true
---

* Table of Contents
{:toc}

## Results

We are finally ready to share with you the results of the survey. Sixty users gave us their time and answered the call. Thanks to you all!

Each user provided a list of [Mechanic](https://robofontmechanic.com) extensions installed on their machine. For each extension, users were asked to indicate an average use value from zero (never used it) to six (every day).

{% image survey/tool.png %}

In the following graph (made with DrawBot), you can read the results. We decided to filter out the zero values as outliers. Extensions are sorted for decreasing order of votes (how many users declared to have installed it) along with an average usage value.

{% image survey/graph.png %}

Here you can download the graph in <a href="../images/survey/graph.pdf" target='_blank'>PDF</a> format.

There is an upper block of 21 extensions with 20+ votes, which we can consider the most urgent group of extensions to be ported on RoboFont 4.0. The good news is: most of them do not need to be updated! They have been already updated or they do not use `drawingTools`.

> - {% internallink "topics/upgrading-code-to-RoboFont4" %}
> - {% internallink "how-tos/updating-scripts-from-RF3-to-RF4" %}
> - {% internallink "topics/merz" %}
> - {% internallink "topics/subscriber" %}
{: .seealso }

## Call to Action

{% image survey/tool.png %}

Dear RoboFont users,
We need your help!

As you might know, the [latest RoboFont release](https://forum.robofont.com/topic/804/robofont-four) introduced consistent changes to the application drawing back-end. These changes remarkably improved the overall application user experience. In other words, RoboFont 4 is way faster than RoboFont 3. However, this improvement has a cost: a high percentage (roughly 70%, 79 of 113) of open-sourced Mechanic extensions is broken on RoboFont 4 or so slow to be unusable. To get back to normal, they need to switch from the old way of drawing ({% internallink "reference/api/mojo/mojo-drawingtools" %}) to the new way of drawing ([Merz](https://typesupply.github.io/merz/)). Porting 79 extensions to the new APIs (Merz and Subscriber) is a huge amount of work, so we need some data to understand what we (the RoboFont team and the plugin developers) should prioritize.

Mechanic downloads the latest version of an extension from its GitHub repository as a zip asset. Unfortunately, we cannot pull any metric from GitHub, therefore we have no way to infer the popularity of a tool compared to another. Moreover, many downloads do not necessarily mean that a tool is critical in your workflow or that it gets used very often. To move from here, we need your input. We are asking the RoboFont community to fill out a survey.

If you click on <a href="robofont://robofont.com/assets/code/extensionSurvey.py">THIS</a> link a python script (here you can [read](https://gist.github.com/benkiel/ea57706d324405108eecdcaf22653396) the code) will be executed on your RoboFont application. What does the script do? It creates a window where you will find a list of all the Mechanic extensions installed on your computer. Near each extension, there is a slider to express your frequency of use. Once you have filled the survey, the send button will paste a string with the answers in an email message. You'll find the address already filled out, you just have to send the message. That's it.

This survey is very important for us at RoboFont and for the plugin developers. It will help everyone to get a better picture of the actual situation of the public plugin ecosystem. Also, we will be able to invest energy and resources where it will have a larger positive impact on the users' community.

The survey will be active for two weeks, starting from October, 6th, first day of the RoboHackathon 2021. So, you have time until November, 3rd to answer. Please, don't forget!
We will publish the results of the survey during the month of November. Data will be grouped and anonymized.

Your Community Manager, <br> Roberto Arista
