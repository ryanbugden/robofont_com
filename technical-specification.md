---
layout: page
title: Technical Specifications
treeTitle: Technical Specs
menuOrder: 2
---

* Table of Contents
{:toc}


Operating system
----------------

RoboFont runs on Apple Silicon and Intel Macs with OS X {{ site.data.versions.minimumSystem }}+.

> The [Downloads] page offers some RoboFont versions able to run on older macOS versions
> - RoboFont 1. MacOS 10.6 — 10.9
> - RoboFont 3. up to MacOS 10.10 (stable), MacOS 10.11 ([working but slow](https://forum.robofont.com/topic/908/robofont-on-macos-11-big-sur))
> - RoboFont 4.1. MacOS 10.11+
> - RoboFont 4.2. MacOS 10.12+
{: .note }

[Downloads]: ../downloads


Font generation
---------------

RoboFont uses the [Adobe FDK][AFDKO] internally to generate OpenType fonts. The latest release of RoboFont ({{ site.data.versions.roboFont }}) embeds version {{ site.data.versions.afdko }} of the FDK. The latest beta release embeds version {{ site.data.versions.afdko }}.

[AFDKO]: http://adobe.com/devnet/opentype/afdko.html


Supported formats
-----------------

RoboFont 3 uses [UFO3] as its native font format, and read/writes [UFO2].

RoboFont can open OpenType fonts, with a few limitations (for example, OpenType features cannot be imported in editable format). WOFF fonts are also supported.

RoboFont can open FontLab’s `.vfb` files, provided the [vfb2ufo] library is installed.

> - {% internallink "documentation/how-tos/converting-from-opentype-to-ufo" %}
> - {% internallink "documentation/how-tos/converting-from-vfb-to-ufo" %}
{: .seealso }

[UFO2]: http://unifiedfontobject.org/versions/ufo2/
[UFO3]: http://unifiedfontobject.org/versions/ufo3/
[vfb2ufo]: http://blog.fontlab.com/font-utility/vfb2ufo/


Scripting language
------------------

RoboFont 3 is written entirely in Python {{ site.data.versions.python }}, and uses it as its scripting language.

RoboFont comes with its own embedded Python interpreter, so you don’t need to install anything else. All modules from the [Python Standard Library] are also included.

[Python Standard Library]: http://docs.python.org/3/library/


Scripting API
-------------

RoboFont 3 uses the [FontParts] API (with a few extensions) to communicate to font objects programmatically.

> - {% internallink "reference/fontparts" %}
> - {% internallink "topics/robofab-fontparts" %}
{: .seealso }

[FontParts]: http://fontparts.readthedocs.io/


Embedded libraries
------------------

**RoboFont comes with batteries included.**

The following libraries are embedded in RoboFont and available out of the box:

<table>
<tr>
  <th width='40%'>embedded library</th>
  <th width='30%'>version</th>
</tr>
  <tr>
    <td><a href='http://github.com/typemytype/booleanOperations'>booleanOperations</a></td>
    <td>{{ site.data.versions.booleanOperations }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/robotools/compositor'>compositor</a></td>
    <td>{{ site.data.versions.compositor }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/googlei18n/cu2qu/'>cu2qu</a></td>
    <td>{{ site.data.versions.cu2qu }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/robotools/defcon'>defcon</a></td>
    <td>{{ site.data.versions.defcon }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/robotools/defconappkit'>defconAppKit</a></td>
    <td>{{ site.data.versions.defconAppKit }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/typesupply/dialogkit'>dialogKit</a></td>
    <td>{{ site.data.versions.dialogKit }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/robotools/extractor'>extractor</a></td>
    <td>{{ site.data.versions.extractor }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/typesupply/feapyfofum'>feaPyFoFum</a></td>
    <td>{{ site.data.versions.feaPyFoFum }}</td>
  </tr>
  <!--
  <tr>
    <td>fontCompiler</td>
    <td>{{ site.data.versions.fontCompiler }}</td>
  </tr>
  --->
  <tr>
    <td><a href='https://github.com/googlefonts/fontmake'>fontmake</a></td>
    <td>{{ site.data.versions.fontmake }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/robotools/fontmath'>fontMath</a></td>
    <td>{{ site.data.versions.fontMath }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/robotools/fontParts'>fontParts</a></td>
    <td>{{ site.data.versions.fontParts }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/robotools/fontpens'>fontPens</a></td>
    <td>{{ site.data.versions.fontPens }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/fonttools/fonttools'>fontTools</a></td>
    <td>{{ site.data.versions.fontTools }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/typemytype/glyphConstruction'>glyphConstruction</a></td>
    <td>{{ site.data.versions.glyphConstruction }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/LettError/glyphNameFormatter'>glyphNameFormatter</a></td>
    <td>{{ site.data.versions.glyphNameFormatter }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/Python-Markdown/markdown'>markdown</a></td>
    <td>{{ site.data.versions.markdown }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/LettError/MutatorMath'>mutatorMath</a></td>
    <td>{{ site.data.versions.mutatorMath }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/unified-font-object/ufoNormalizer'>ufoNormalizer</a></td>
    <td>{{ site.data.versions.ufonormalizer }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/robotools/ufo2fdk'>ufo2fdk</a></td>
    <td>{{ site.data.versions.ufo2fdk }}</td>
  </tr>
  <!--
  <tr>
    <td><a href='http://github.com/typesupply/ufo2svg'>ufo2svg</a></td>
    <td>{{ site.data.versions.ufo2svg }}</td>
  </tr>
  --->
  <tr>
    <td><a href='http://github.com/unified-font-object/ufoNormalizer'>ufoNormalizer</a></td>
    <td>{{ site.data.versions.ufonormalizer }}</td>
  </tr>
  <tr>
    <td><a href='http://github.com/robotools/vanilla'>vanilla</a></td>
    <td>{{ site.data.versions.vanilla }}</td>
  </tr>

</table>

> - {% internallink "reference/embedded-libraries#embedded-libraries" %}
> - {% internallink 'how-tos/overriding-embedded-libraries' %}
{: .seealso }

RoadMap
-------

Considering the following events:

- Python3.9 end of life set in October 2025
- First Apple M1 MacBook sold at the end of 2020
- MacOS 10.12 released in 2016

Starting from the end of 2025, RoboFont will only actively develop for Apple Silicon. Consequently, support for MacOS 12 (Monterey) and older will be dropped.

