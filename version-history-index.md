---
excludeSearch: true
excludeNavigation: true
---

<style type="text/css">
* {
    font-family:Menlo, monospace;
    margin:0px;
    padding:0px;
    font-size:12px;
}
body {
    margin-left:10px;
    margin-bottom:50px;
    line-height: 1.3em;
}
ul {
    list-style-type:none;
    margin-bottom:10px;
    padding-left: 1em;
    text-indent: -1em;
}
li ul {
    padding-left:20px;
    padding-top:5px;
    margin-bottom:-5px;
}
li {
    padding-bottom:5px;
    display: block;
}
li:before {
    content: "- ";
}
ul ul li {
    margin-left:0px;
}
ul ul {
    margin-bottom:0px;
}
ul ul li:before {
    content: "+ ";
}
h1, h2, h3 {
    background-color:#404040;
    margin:0px -10px 10px;
    padding-top:7px;
    color:white;
    font-size:12px;
    font-weight:normal;
    padding:10px;
}
code {
    background-color:#FFFFDA;
    padding: 3px 5px 3px 5px;
}
.warning {
    color: red;
    padding-bottom: 10px;
}
.note {
    color: green;
    padding-bottom: 10px;
}
</style>

{% include_relative version-history-raw.md %}